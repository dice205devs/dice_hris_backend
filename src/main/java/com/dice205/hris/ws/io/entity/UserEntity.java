package com.dice205.hris.ws.io.entity;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name = "users")
public class UserEntity implements Serializable {

	private static final long serialVersionUID = -1510132202068561206L;

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private String userId;

	@Column(nullable = false, length = 120)
	private String email;

	@Column(nullable = false)
	private String supervisorId;

	@Column(nullable = false)
	private int maxEarlyTimeout;

	@Column(nullable = false)
	private int earlyTimeoutCount;

	@Column(nullable = false)
	private String employmentStatus;
	
	@Column(nullable = false)
	private String workingStatus;

	@Column(nullable = false)
	private Time shift;

	@Column(nullable = false)
	private String encryptedPassword;

	@Column(nullable = true)
	private String emailVerificationToken;

	@Column(nullable = true, columnDefinition = "boolean default false")
	private Boolean emailVerificationStatus;

	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = true)
	private String updatedBy;

	@Column(nullable = true)
	private Timestamp deletedAt;

	@Column(nullable = true)
	private Timestamp updatedAt;

	@Column(nullable = false)
	private Timestamp createdAt;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "profile")
	private ProfileEntity profile;

	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "users_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "roles_id", referencedColumnName = "id"))
	private Collection<RoleEntity> roles;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="user")
	private Collection<TimesheetEntity> timesheet;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="user")
	private List<EmployeeRecordEntity> employeeRecord;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="user")
	private List<PayrollRecordEntity> payrollRecord;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="applicant")
	private List<RequestFormEntity> requestForm;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="user")
	private List<SubscriptionEntity> subscription;
	
	@ManyToMany(mappedBy = "participants")
	private List<CalendarEventEntity> calendarEvents;
	
	public Collection<TimesheetEntity> getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(Collection<TimesheetEntity> timesheet) {
		this.timesheet = timesheet;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	public int getMaxEarlyTimeout() {
		return maxEarlyTimeout;
	}

	public void setMaxEarlyTimeout(int maxEarlyTimeout) {
		this.maxEarlyTimeout = maxEarlyTimeout;
	}

	public int getEarlyTimeoutCount() {
		return earlyTimeoutCount;
	}

	public void setEarlyTimeoutCount(int earlyTimeoutCount) {
		this.earlyTimeoutCount = earlyTimeoutCount;
	}

	public Time getShift() {
		return shift;
	}

	public void setShift(Time shift) {
		this.shift = shift;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public String getEmailVerificationToken() {
		return emailVerificationToken;
	}

	public void setEmailVerificationToken(String emailVerificationToken) {
		this.emailVerificationToken = emailVerificationToken;
	}

	public Boolean getEmailVerificationStatus() {
		return emailVerificationStatus;
	}

	public void setEmailVerificationStatus(Boolean emailVerificationStatus) {
		this.emailVerificationStatus = emailVerificationStatus;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public ProfileEntity getProfile() {
		return profile;
	}

	public void setProfile(ProfileEntity profile) {
		this.profile = profile;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Collection<RoleEntity> getRoles() {
		return roles;
	}

	public void setRoles(Collection<RoleEntity> roles) {
		this.roles = roles;
	}

	public List<EmployeeRecordEntity> getEmployeeRecord() {
		return employeeRecord;
	}

	public void setEmployeeRecord(List<EmployeeRecordEntity> employeeRecord) {
		this.employeeRecord = employeeRecord;
	}

	public List<PayrollRecordEntity> getPayrollRecord() {
		return payrollRecord;
	}

	public void setPayrollRecord(List<PayrollRecordEntity> payrollRecord) {
		this.payrollRecord = payrollRecord;
	}

	public List<RequestFormEntity> getRequestForm() {
		return requestForm;
	}

	public void setRequestForm(List<RequestFormEntity> requestForm) {
		this.requestForm = requestForm;
	}

	public String getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public String getWorkingStatus() {
		return workingStatus;
	}

	public void setWorkingStatus(String workingStatus) {
		this.workingStatus = workingStatus;
	}

	public List<SubscriptionEntity> getSubscription() {
		return subscription;
	}

	public void setSubscription(List<SubscriptionEntity> subscription) {
		this.subscription = subscription;
	}

	public List<CalendarEventEntity> getCalendarEvents() {
		return calendarEvents;
	}

	public void setCalendarEvents(List<CalendarEventEntity> calendarEvents) {
		this.calendarEvents = calendarEvents;
	}

}
