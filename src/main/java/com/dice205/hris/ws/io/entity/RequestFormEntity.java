package com.dice205.hris.ws.io.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "requestForm")
public class RequestFormEntity implements Serializable {

	private static final long serialVersionUID = -8788964448608155743L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = true)
	private Boolean requestStatus;
	
	@Column(nullable = false)
	private String reason;
	
	@Column(nullable = false, columnDefinition="DATE DEFAULT '1970-01-01'")
	private Date dateFiled;
	
	@Column(columnDefinition = "DOUBLE PRECISION default '0.0'")
	private double overtimeDuration;
	
	@Column(nullable = true, columnDefinition="DATE DEFAULT NULL")
	private Date overtimeDate;
	
	@Column(nullable = true, columnDefinition="DATE DEFAULT NULL")
	private Date leaveDate;
	
	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = true)
	private String updatedBy;

	@Column(nullable = true)
	private Timestamp deletedAt;

	@Column(nullable = true)
	private Timestamp updatedAt;

	@Column(nullable = false)
	private Timestamp createdAt;
	
	@ManyToOne
	@JoinColumn(name = "applicant_id")
	private UserEntity applicant;
	
	@ManyToOne
	@JoinColumn(name = "request_type_id")
	private RequestTypeEntity requestType;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Boolean getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(Boolean requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getDateFiled() {
		return dateFiled;
	}

	public void setDateFiled(Date dateFiled) {
		this.dateFiled = dateFiled;
	}

	public Date getLeaveDate() {
		return leaveDate;
	}

	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public UserEntity getApplicant() {
		return applicant;
	}

	public void setApplicant(UserEntity applicant) {
		this.applicant = applicant;
	}

	public double getOvertimeDuration() {
		return overtimeDuration;
	}

	public void setOvertimeDuration(double overtimeDuration) {
		this.overtimeDuration = overtimeDuration;
	}

	public Date getOvertimeDate() {
		return overtimeDate;
	}

	public void setOvertimeDate(Date overtimeDate) {
		this.overtimeDate = overtimeDate;
	}

	public RequestTypeEntity getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestTypeEntity requestType) {
		this.requestType = requestType;
	}
}
