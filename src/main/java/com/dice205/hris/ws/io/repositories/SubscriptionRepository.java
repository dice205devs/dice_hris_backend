package com.dice205.hris.ws.io.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dice205.hris.ws.io.entity.SubscriptionEntity;
import com.dice205.hris.ws.io.entity.UserEntity;

@Repository
public interface SubscriptionRepository extends CrudRepository<SubscriptionEntity, Long> {
	SubscriptionEntity findBySubscriptionId(String subscriptionId);
	SubscriptionEntity findByEndpoint(String endpoint);
	List<SubscriptionEntity> findByUser(UserEntity user);
	List<SubscriptionEntity> findAllByUserIn(List<UserEntity> userEntityList);
}
