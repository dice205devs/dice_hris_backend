package com.dice205.hris.ws.io.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "employee_record")
public class EmployeeRecordEntity implements Serializable {

	private static final long serialVersionUID = 290176702386592626L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = true)
	private String employeeName;

	@Column(nullable = true)
	private String position;

	@Column(nullable = true)
	private String shift;

	@Column(nullable = false)
	private boolean isLate;
	
	@Column(nullable = false)
	private double requiredHours;

	@Column(columnDefinition = "DOUBLE PRECISION default '0.0'", nullable = false)
	private double breaksDuration;

	@Column(columnDefinition = "DOUBLE PRECISION default '0.0'", nullable = false)
	private double breaksOffset;

	@Column(columnDefinition = "DOUBLE PRECISION default '0.0'", nullable = false)
	private double breakOBDuration;

	@Column(columnDefinition = "DOUBLE PRECISION default '0.0'", nullable = false)
	private double overtime;

	@Column(columnDefinition = "DOUBLE PRECISION default '0.0'", nullable = false)
	private double hoursRendered;
	
	@Column(nullable = false)
	private int cutOffPeriod;
	
	@Column(nullable = false, columnDefinition="DATE DEFAULT '1970-01-01'")
	private Date employeeRecordDate;

	@Column(nullable = true)
	private String leaveType;

	@Column(nullable = true)
	private String holidayType;

	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = true)
	private String updatedBy;

	@Column(nullable = true)
	private Timestamp deletedAt;

	@Column(nullable = true)
	private Timestamp updatedAt;

	@Column(nullable = false)
	private Timestamp createdAt;

	@ManyToOne
	@JoinColumn(name = "user")
	private UserEntity user;

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getShift() {
		return shift;
	}

	public void setShift(String shift) {
		this.shift = shift;
	}

	public double getRequiredHours() {
		return requiredHours;
	}

	public void setRequiredHours(double requiredHours) {
		this.requiredHours = requiredHours;
	}

	public double getBreaksDuration() {
		return breaksDuration;
	}

	public void setBreaksDuration(double breaksDuration) {
		this.breaksDuration = breaksDuration;
	}

	public double getBreaksOffset() {
		return breaksOffset;
	}

	public void setBreaksOffset(double breaksOffset) {
		this.breaksOffset = breaksOffset;
	}

	public double getBreakOBDuration() {
		return breakOBDuration;
	}

	public void setBreakOBDuration(double breakOBDuration) {
		this.breakOBDuration = breakOBDuration;
	}

	public double getOvertime() {
		return overtime;
	}

	public void setOvertime(double overtime) {
		this.overtime = overtime;
	}

	public double getHoursRendered() {
		return hoursRendered;
	}

	public void setHoursRendered(double hoursRendered) {
		this.hoursRendered = hoursRendered;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Date getEmployeeRecordDate() {
		return employeeRecordDate;
	}

	public void setEmployeeRecordDate(Date employeeRecordDate) {
		this.employeeRecordDate = employeeRecordDate;
	}

	public int getCutOffPeriod() {
		return cutOffPeriod;
	}

	public void setCutOffPeriod(int cutOffPeriod) {
		this.cutOffPeriod = cutOffPeriod;
	}

	public boolean isLate() {
		return isLate;
	}

	public void setLate(boolean isLate) {
		this.isLate = isLate;
	}
	
	public String getHolidayType() {
		return holidayType;
	}

	public void setHolidayType(String holidayType) {
		this.holidayType = holidayType;
	}
}
