package com.dice205.hris.ws.io.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.dice205.hris.ws.shared.CustomJSONObject;
import com.dice205.hris.ws.shared.CustomJSONObjectConverter;

@Entity(name = "profile")
public class ProfileEntity implements Serializable {

    private static final long serialVersionUID = 3445198258331256054L;

    @Id
	@GeneratedValue
    private long id;
    
    @Column(nullable = false, length=30)
    private String employeeId;
    
    @Column(nullable = false, columnDefinition = "TEXT")
    @Convert(converter= CustomJSONObjectConverter.class)
    private CustomJSONObject personalInformation;
    
    @Column(nullable = false, columnDefinition = "TEXT")
    @Convert(converter= CustomJSONObjectConverter.class)
    private CustomJSONObject educationalBackground;

    @Column(nullable = false, columnDefinition = "TEXT")
    @Convert(converter= CustomJSONObjectConverter.class)
    private CustomJSONObject contactInformation;
    
    @Column(nullable = false, columnDefinition = "TEXT")
    @Convert(converter= CustomJSONObjectConverter.class)
    private CustomJSONObject governmentInformation;
    
    @Column(nullable = false, columnDefinition = "TEXT")
    @Convert(converter= CustomJSONObjectConverter.class)
    private CustomJSONObject employmentBackground;
    
    @Column(nullable = false, columnDefinition = "TEXT")
    @Convert(converter= CustomJSONObjectConverter.class)
    private CustomJSONObject workDetails;
    
    @Column(nullable = false)
	private String createdBy;
	
	@Column(nullable = true)
	private String updatedBy;
	
	@Column(nullable = true)
	private Timestamp deletedAt;
	
    @Column(nullable = true)
	private Timestamp updatedAt;

	@Column(nullable = false)
	private Timestamp createdAt;
    
    @OneToOne(mappedBy = "profile")
    private UserEntity user;
    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

	public CustomJSONObject getEducationalBackground() {
		return educationalBackground;
	}

	public void setEducationalBackground(CustomJSONObject educationalBackground) {
		this.educationalBackground = educationalBackground;
	}

	public CustomJSONObject getContactInformation() {
		return contactInformation;
	}

	public void setContactInformation(CustomJSONObject contactInformation) {
		this.contactInformation = contactInformation;
	}

	public CustomJSONObject getGovernmentInformation() {
		return governmentInformation;
	}

	public void setGovernmentInformation(CustomJSONObject governmentInformation) {
		this.governmentInformation = governmentInformation;
	}

	public CustomJSONObject getEmploymentBackground() {
		return employmentBackground;
	}

	public void setEmploymentBackground(CustomJSONObject employmentBackground) {
		this.employmentBackground = employmentBackground;
	}

	public CustomJSONObject getWorkDetails() {
		return workDetails;
	}

	public void setWorkDetails(CustomJSONObject workDetails) {
		this.workDetails = workDetails;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public CustomJSONObject getPersonalInformation() {
		return personalInformation;
	}

	public void setPersonalInformation(CustomJSONObject personalInformation) {
		this.personalInformation = personalInformation;
	}
}