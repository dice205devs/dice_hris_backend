package com.dice205.hris.ws.io.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dice205.hris.ws.io.entity.ProfileEntity;

@Repository
public interface ProfileRepository extends CrudRepository<ProfileEntity, Long> {
    ProfileEntity findByEmployeeId(String id);
}
