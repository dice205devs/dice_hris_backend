package com.dice205.hris.ws.io.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "audit_logs")
public class AuditLogsEntity implements Serializable {
	private static final long serialVersionUID = -8517679878844432451L;

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = true, length = 50)
	private String userId;

	@Column(nullable = false)
	private String api;

	@Column(nullable = false, length = 20)
	private String method;

	@Column(nullable = false)
	private Timestamp dateTime;

	@Column(nullable = false)
	private int status;
	
	@Column(nullable = true, columnDefinition="TEXT")
	private String requestBody;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Timestamp getDateTime() {
		return dateTime;
	}

	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}

}
