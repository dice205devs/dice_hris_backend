package com.dice205.hris.ws.io.repositories;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.dice205.hris.ws.io.entity.TimesheetEntity;
import com.dice205.hris.ws.io.entity.UserEntity;

@Repository
public interface TimesheetRepository extends PagingAndSortingRepository<TimesheetEntity, Long> {
	Page<TimesheetEntity> findAllByUserOrderByTimesheetDateAsc(UserEntity user, Pageable pageableRequest);
	Page<TimesheetEntity> findAllByUserAndTimesheetDateOrderByTimeStartAsc(UserEntity user, java.util.Date timesheetDate, Pageable pageableRequest);
	List<TimesheetEntity> findAllByUserAndTimesheetDateAndBreakTypeIsNull(UserEntity user, Date timesheetDate);
	List<TimesheetEntity> findAllByUserAndTimesheetDateAndBreakTypeIsNotNull(UserEntity user, Date timesheetDate);
	TimesheetEntity findByUserAndTimesheetDateAndTimeEndIsNullAndTimeStartIsNotNull(UserEntity user, Date timesheetDate);
	TimesheetEntity findByUserAndTimesheetDateAndTimeEndIsNullAndTimeStartIsNotNullAndBreakTypeIsNotNull(UserEntity user, Date timesheetDate);
	TimesheetEntity findByUserAndTimesheetDateAndTimeEndIsNullAndTimeStartIsNotNullAndBreakTypeIsNull(UserEntity user, Date timesheetDate);
	TimesheetEntity findByUserAndTimesheetDateLessThanAndTimeEndIsNullAndTimeStartIsNotNullAndBreakTypeIsNull(UserEntity user, Date timesheetDate);
	List<TimesheetEntity> findByUserAndTimesheetDate(UserEntity userEntity, Date dateToday);
}
