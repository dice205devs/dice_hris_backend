package com.dice205.hris.ws.io.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dice205.hris.ws.io.entity.AuditLogsEntity;

@Repository
public interface AuditLogsRepository extends CrudRepository<AuditLogsEntity, Long> {
	List<AuditLogsEntity> findAllByUserId(String userId);
}
