package com.dice205.hris.ws.io.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dice205.hris.ws.io.entity.SubscriptionKeysEntity;

@Repository
public interface SubscriptionKeysRepository extends CrudRepository<SubscriptionKeysEntity, Long> {

}
