package com.dice205.hris.ws.io.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity(name = "calendarEvents")
public class CalendarEventEntity implements Serializable {

	private static final long serialVersionUID = 7098959507507447514L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false, length = 100)
	private String eventName;
	
	@Column(nullable = false, columnDefinition="DATE DEFAULT '1970-01-01'")
	private Date eventDate;
	
	@Column(nullable = true, length = 30)
	private String holidayType;
	
	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinTable(name = "events_participants", joinColumns = @JoinColumn(name = "calendar_events_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "participants_id", referencedColumnName = "id"))
	@Column(nullable = true)
	private List<UserEntity> participants;
	
	@Column(nullable = true)
	private Time eventTime;
	
	@Column(nullable = false, length = 30)
	private String createdBy;

	@Column(nullable = true, length = 30)
	private String updatedBy;

	@Column(nullable = true)
	private Timestamp deletedAt;

	@Column(nullable = true)
	private Timestamp updatedAt;

	@Column(nullable = false)
	private Timestamp createdAt;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getHolidayType() {
		return holidayType;
	}

	public void setHolidayType(String holidayType) {
		this.holidayType = holidayType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public List<UserEntity> getParticipants() {
		return participants;
	}

	public void setParticipants(List<UserEntity> participants) {
		this.participants = participants;
	}

	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	} 
}
