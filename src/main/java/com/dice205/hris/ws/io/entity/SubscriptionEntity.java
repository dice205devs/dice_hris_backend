package com.dice205.hris.ws.io.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity(name = "subscription")
public class SubscriptionEntity implements Serializable {

	private static final long serialVersionUID = -5189480205409983299L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(length = 30)
	private String subscriptionId;
	
	@Column(nullable = false)
	private String endpoint;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "keysId")
	private SubscriptionKeysEntity keysId;
	
	@ManyToOne
	@JoinColumn(name = "user", nullable = false)
	private UserEntity user;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public SubscriptionKeysEntity getKeysId() {
		return keysId;
	}

	public void setKeysId(SubscriptionKeysEntity keysId) {
		this.keysId = keysId;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}
}
