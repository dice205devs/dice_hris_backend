package com.dice205.hris.ws.io.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.dice205.hris.ws.io.entity.JwtBlacklistEntity;

public interface JwtBlacklistRepository extends CrudRepository<JwtBlacklistEntity, Long> {
	Optional<JwtBlacklistEntity> findByJwt(String jwt); 
}
