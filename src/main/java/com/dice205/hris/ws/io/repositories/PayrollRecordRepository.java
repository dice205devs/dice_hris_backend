package com.dice205.hris.ws.io.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.dice205.hris.ws.io.entity.PayrollRecordEntity;
import com.dice205.hris.ws.io.entity.UserEntity;

public interface PayrollRecordRepository extends CrudRepository<PayrollRecordEntity, Long> {
	List<PayrollRecordEntity> findAllByUser(UserEntity user);
	
	@Query(value = "SELECT * FROM PAYROLL_RECORD WHERE USER = :user AND CUT_OFF_PERIOD LIKE :cutOffPeriod AND PAYROLL_RECORD_DATE LIKE :date", nativeQuery = true)
	Page<PayrollRecordEntity> getByUserAndCutOffPeriodAndPayrollRecordDate(@Param("user") UserEntity user, @Param("cutOffPeriod") String cutOffPeriod, @Param("date") String date, Pageable pageableRequest);
}
