package com.dice205.hris.ws.io.repositories;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dice205.hris.ws.io.entity.CalendarEventEntity;

@Repository
public interface CalendarEventRepository extends PagingAndSortingRepository<CalendarEventEntity, Long> {
	@Query(value = "SELECT * FROM CALENDAR_EVENTS WHERE EVENT_DATE LIKE :date AND DELETED_AT IS NULL", nativeQuery = true)
	List<CalendarEventEntity> findByEventDate(@Param("date") String date);
	
	List<CalendarEventEntity> findByEventDateAndHolidayTypeIsNotNullAndDeletedAtIsNull(Date date);
}
