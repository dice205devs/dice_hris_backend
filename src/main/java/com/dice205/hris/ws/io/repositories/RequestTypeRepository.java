package com.dice205.hris.ws.io.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dice205.hris.ws.io.entity.RequestTypeEntity;

@Repository
public interface RequestTypeRepository extends CrudRepository<RequestTypeEntity, Long> {
	RequestTypeEntity findByName(String name);
}
