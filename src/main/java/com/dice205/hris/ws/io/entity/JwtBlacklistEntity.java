package com.dice205.hris.ws.io.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "jwt_blacklist")
public class JwtBlacklistEntity implements Serializable {

	private static final long serialVersionUID = -9184499347430062553L;
	
	@Id
	@GeneratedValue
    private long id;
	
	@Column(nullable= false)
	private String jwt;
	
	@Column(nullable = false)
	private Timestamp dateBlacklisted;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

	public Timestamp getDateBlacklisted() {
		return dateBlacklisted;
	}

	public void setDateBlacklisted(Timestamp dateBlacklisted) {
		this.dateBlacklisted = dateBlacklisted;
	}
}
