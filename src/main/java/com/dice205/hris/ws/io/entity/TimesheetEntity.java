package com.dice205.hris.ws.io.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.dice205.hris.ws.shared.CustomJSONObject;
import com.dice205.hris.ws.shared.CustomJSONObjectConverter;

@Entity(name = "timesheet")
public class TimesheetEntity implements Serializable {

	private static final long serialVersionUID = 3205972122644888520L;

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false, length = 100)
	private String employeeName;

	@Column(nullable = false, columnDefinition = "DATE DEFAULT '1970-01-01'")
	private Date timesheetDate;

	@Column(nullable = false)
	private Time timeStart;

	@Column(nullable = true)
	private Time timeEnd;

	@Column(nullable = true, columnDefinition = "TEXT")
	@Convert(converter = CustomJSONObjectConverter.class)
	private CustomJSONObject officialBusiness;

	@Column(nullable = true, length = 20)
	private String breakType;

	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = true)
	private String updatedBy;

	@Column(nullable = true)
	private Timestamp deletedAt;

	@Column(nullable = true)
	private Timestamp updatedAt;

	@Column(nullable = false)
	private Timestamp createdAt;

	@ManyToOne
	@JoinColumn(name = "user")
	private UserEntity user;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBreakType() {
		return breakType;
	}

	public void setBreakType(String breakType) {
		this.breakType = breakType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Date getTimesheetDate() {
		return timesheetDate;
	}

	public void setTimesheetDate(Date timesheetDate) {
		this.timesheetDate = timesheetDate;
	}

	public Time getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(Time timeStart) {
		this.timeStart = timeStart;
	}

	public Time getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(Time timeEnd) {
		this.timeEnd = timeEnd;
	}

	public CustomJSONObject getOfficialBusiness() {
		return officialBusiness;
	}

	public void setOfficialBusiness(CustomJSONObject officialBusiness) {
		this.officialBusiness = officialBusiness;
	}
}
