
// Contains sql functions from CrudReposity

package com.dice205.hris.ws.io.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.dice205.hris.ws.io.entity.UserEntity;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {
	UserEntity findByEmail(String email);
	UserEntity findByEmailAndDeletedAtIsNull(String email);
	UserEntity findByUserId(String id);
	UserEntity findByUserIdAndDeletedAtIsNull(String associateId);
	List<UserEntity> findByDeletedAtIsNull();
	List<UserEntity> findByDeletedAtIsNotNull();
	UserEntity findUserByEmailVerificationToken(String token);
	Page<UserEntity> findAllByDeletedAtIsNull(Pageable pageableRequest);
	Page<UserEntity> findAllByDeletedAtIsNotNull(Pageable pageableRequest);
	List<UserEntity> findAllByUserIdIn(String[] userId);
}
