package com.dice205.hris.ws.io.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dice205.hris.ws.io.entity.EmployeeRecordEntity;
import com.dice205.hris.ws.io.entity.UserEntity;

@Repository
public interface EmployeeRecordRepository extends CrudRepository<EmployeeRecordEntity, Long> {
	List<EmployeeRecordEntity> findAllByUser(UserEntity userEntity);
	Page<EmployeeRecordEntity> findAllByUserAndEmployeeRecordDateLessThanEqualAndEmployeeRecordDateGreaterThanEqual(UserEntity userEntity, Date to, Date from, Pageable pageableRequest);
	EmployeeRecordEntity findByUserAndEmployeeRecordDate(UserEntity userEntity, java.sql.Date employeeRecordDate);
	
	@Query(value = "SELECT * FROM EMPLOYEE_RECORD WHERE USER = :user AND CUT_OFF_PERIOD LIKE :cutOffPeriod AND EMPLOYEE_RECORD_DATE LIKE :date", nativeQuery = true)
	List<EmployeeRecordEntity> getByUserAndCutOffAndDate(@Param("user") UserEntity user, @Param("cutOffPeriod") String cutOffPeriod, @Param("date") String date);
}
