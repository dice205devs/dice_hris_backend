package com.dice205.hris.ws.io.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "payroll_record")
public class PayrollRecordEntity implements Serializable {

	private static final long serialVersionUID = -7587473835517171778L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String employeeName;
	
	@Column(nullable = false, columnDefinition="DATE DEFAULT '1970-01-01'")
	private Date payrollRecordDate;

	@Column(columnDefinition = "DOUBLE PRECISION", nullable = false)
	private double cutOffRequiredHours;
	
	@Column(columnDefinition = "DOUBLE PRECISION", nullable = false)
	private double cutOffHoursRendered;
	
	@Column(columnDefinition = "DOUBLE PRECISION", nullable = false)
	private double cutOffBreaksRendered;
	
	@Column(columnDefinition = "DOUBLE PRECISION", nullable = false)
	private double cutOffOBRendered;
	
	@Column(columnDefinition = "DOUBLE PRECISION", nullable = false)
	private double cutOffOvertimeRendered;
	
	@Column(columnDefinition = "DOUBLE PRECISION", nullable = false)
	private double cutOffBreaksOffset;
	
	@Column(nullable = false)
	private int cutOffPeriod;
	
	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = true)
	private String updatedBy;

	@Column(nullable = true)
	private Timestamp deletedAt;

	@Column(nullable = true)
	private Timestamp updatedAt;

	@Column(nullable = false)
	private Timestamp createdAt;

	@ManyToOne
	@JoinColumn(name = "user", nullable = false)
	private UserEntity user;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public double getCutOffHoursRendered() {
		return cutOffHoursRendered;
	}

	public void setCutOffHoursRendered(double cutOffHoursRendered) {
		this.cutOffHoursRendered = cutOffHoursRendered;
	}

	public int getCutOffPeriod() {
		return cutOffPeriod;
	}

	public void setCutOffPeriod(int cutOffPeriod) {
		this.cutOffPeriod = cutOffPeriod;
	}

	public Date getPayrollRecordDate() {
		return payrollRecordDate;
	}

	public void setPayrollRecordDate(Date payrollRecordDate) {
		this.payrollRecordDate = payrollRecordDate;
	}

	public double getCutOffRequiredHours() {
		return cutOffRequiredHours;
	}

	public void setCutOffRequiredHours(double cutOffRequiredHours) {
		this.cutOffRequiredHours = cutOffRequiredHours;
	}

	public double getCutOffBreaksRendered() {
		return cutOffBreaksRendered;
	}

	public void setCutOffBreaksRendered(double cutOffBreaksRendered) {
		this.cutOffBreaksRendered = cutOffBreaksRendered;
	}

	public double getCutOffOBRendered() {
		return cutOffOBRendered;
	}

	public void setCutOffOBRendered(double cutOffOBRendered) {
		this.cutOffOBRendered = cutOffOBRendered;
	}

	public double getCutOffOvertimeRendered() {
		return cutOffOvertimeRendered;
	}

	public void setCutOffOvertimeRendered(double cutOffOvertimeRendered) {
		this.cutOffOvertimeRendered = cutOffOvertimeRendered;
	}

	public double getCutOffBreaksOffset() {
		return cutOffBreaksOffset;
	}

	public void setCutOffBreaksOffset(double cutOffBreaksOffset) {
		this.cutOffBreaksOffset = cutOffBreaksOffset;
	}

}
