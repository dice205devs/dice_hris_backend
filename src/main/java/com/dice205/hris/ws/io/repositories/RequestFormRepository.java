package com.dice205.hris.ws.io.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.dice205.hris.ws.io.entity.RequestFormEntity;
import com.dice205.hris.ws.io.entity.UserEntity;

@Repository
public interface RequestFormRepository extends PagingAndSortingRepository<RequestFormEntity, Long> {
	Page<RequestFormEntity> findAllByApplicant(UserEntity userEntity, Pageable pageableRequest);
}
