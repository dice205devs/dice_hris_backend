package com.dice205.hris.ws.io.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name = "request_type")
public class RequestTypeEntity implements Serializable {

	private static final long serialVersionUID = 7088982170750812863L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false, length = 20)
	private String name;
	
	@Column(nullable = false)
	private long fileByDate;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="requestType")
	private List<RequestFormEntity> requestForms;

	public RequestTypeEntity() {}
	
	public RequestTypeEntity(String name, long fileByDate) {
		this.name = name;
		this.fileByDate = fileByDate;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getFileByDate() {
		return fileByDate;
	}

	public void setFileByDate(long fileByDate) {
		this.fileByDate = fileByDate;
	}

	public List<RequestFormEntity> getRequestForms() {
		return requestForms;
	}

	public void setRequestForms(List<RequestFormEntity> requestForms) {
		this.requestForms = requestForms;
	}
}
