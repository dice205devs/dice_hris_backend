package com.dice205.hris.ws.exceptions;

public class UserServiceException extends RuntimeException {

	private static final long serialVersionUID = 6961166661317621532L;

	public UserServiceException(String message) {
		super(message);
	}
}
