package com.dice205.hris.ws.shared;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class CustomJSONObject extends JSONObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4401697456142693877L;
	
	public CustomJSONObject() {
		super();
	}
	
	public CustomJSONObject(JSONObject jo, String ... names) {
		super(jo, names);
	}
	
	public CustomJSONObject(JSONTokener x) {
		super(x);
	}
	
	public CustomJSONObject(Map<?, ?> m) {
		super(m);
	}
	
	public CustomJSONObject(Object bean) {
		super(bean);
	}
	
	public CustomJSONObject(Object object, String ... names) {
		super(object, names);
	}
	
	public CustomJSONObject(String source) {
		super(source);
	}
	
	public CustomJSONObject(String baseName, Locale locale) {
		super(baseName, locale);
	}
	
	public CustomJSONObject(int initialCapacity) {
		super(initialCapacity);
	}
	
	public JSONObject getJSONObject(String key) {
		JSONObject returnValue = new JSONObject();
		
		Object object;
		
		try {
			object = this.get(key);
			
	        if (object instanceof JSONObject) {
	            returnValue = (JSONObject) object;
	        }
			
		} catch (JSONException e) {
			returnValue = null;
		}
		
		return returnValue;
	}
}
