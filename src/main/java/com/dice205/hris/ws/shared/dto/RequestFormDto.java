package com.dice205.hris.ws.shared.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import com.dice205.hris.ws.io.entity.UserEntity;

public class RequestFormDto implements Serializable {

	private static final long serialVersionUID = 8325684194585665900L;
	
	private long id;
	private RequestTypes requestType;
	private Boolean requestStatus;
	private String reason;
	private Date dateFiled;
	private double overtimeDuration = 0.0;
	private Date overtimeDate = null;
	private Time overtimeStart = null;
	private Time overtimeEnd = null;
	private Date leaveDate = null;
	private String createdBy;
	private String updatedBy;
	private Timestamp deletedAt;
	private Timestamp updatedAt;
	private Timestamp createdAt;
	private UserEntity applicant;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Boolean getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(Boolean requestStatus) {
		this.requestStatus = requestStatus;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getDateFiled() {
		return dateFiled;
	}
	public void setDateFiled(Date dateFiled) {
		this.dateFiled = dateFiled;
	}
	public double getOvertimeDuration() {
		return overtimeDuration;
	}
	public void setOvertimeDuration(double overtimeDuration) {
		this.overtimeDuration = overtimeDuration;
	}
	public Date getOvertimeDate() {
		return overtimeDate;
	}
	public void setOvertimeDate(Date overtimeDate) {
		this.overtimeDate = overtimeDate;
	}
	public Time getOvertimeStart() {
		return overtimeStart;
	}
	public void setOvertimeStart(Time overtimeStart) {
		this.overtimeStart = overtimeStart;
	}
	public Time getOvertimeEnd() {
		return overtimeEnd;
	}
	public void setOvertimeEnd(Time overtimeEnd) {
		this.overtimeEnd = overtimeEnd;
	}
	public Date getLeaveDate() {
		return leaveDate;
	}
	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getDeletedAt() {
		return deletedAt;
	}
	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public UserEntity getApplicant() {
		return applicant;
	}
	public void setApplicant(UserEntity applicant) {
		this.applicant = applicant;
	}
	public RequestTypes getRequestType() {
		return requestType;
	}
	public void setRequestType(RequestTypes requestType) {
		this.requestType = requestType;
	}
}
