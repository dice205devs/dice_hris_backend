package com.dice205.hris.ws.shared;

import org.springframework.stereotype.Service;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.dice205.hris.ws.security.SecurityConstants;
import com.dice205.hris.ws.shared.dto.UserDto;

@Service
public class AmazonSES {
	final String URL = SecurityConstants.SERVER_URL;
	
	// This address must be verified with Amazon SES.
	final String FROM = "francis.borlas@dice205.com";

	// The subject line for the email.
	final String SUBJECT = "One last step to complete your registration with DICE205 HRIS";
	final String PASSWORD_RESET_SUBJECT = "Password reset request";
	
	// The HTML body for the email.
	final String HTMLBODY = "<h1>Please verify your email address</h1>"
			+ "<p>Welcome from DICE205. To complete registration process and be able to log in,"
			+ " click on the link: "
			+ "<a href='"+URL+"/dice-hris/users/email-verification?token=$tokenValue'>"
			+ "Direct Link" + "</a><br/><br/>"
			+ "Thank you! You can use your Dice email to login to HRIS!";

	// The email body for recipients with non-HTML email clients.
	final String TEXTBODY = "Please verify your email address. "
			+ "Welcome from DICE205. To complete registration process and be able to log in,"
			+ " open then the following URL in your browser window: "
			+URL+"/dice-hris/users/email-verification?token=$tokenValue&resetToken=$resetTokenValue"
			+ " Thank you! You can use your Dice email to login to HRIS!";

	final String PASSWORD_RESET_HTMLBODY = "<h1>A request to reset your password</h1>" + "<p>Hello!</p> "
			+ "<p>Someone has requested to reset your password on DICE205 HRIS. If it were not you, please ignore it."
			+ " otherwise please click on the link below to set a new password: "
			+ "<a href='"+URL+"/verification-service/password-reset.html?token=$tokenValue'>"
			+ " Click this link to Reset Password" + "</a><br/><br/>" + "Thank you!";

	// The email body for recipients with non-HTML email clients.
	final String PASSWORD_RESET_TEXTBODY = "A request to reset your password " + "Hello! "
			+ "Someone has requested to reset your password on DICE205 HRIS. If it were not you, please ignore it."
			+ " otherwise please open the link below in your browser window to set a new password: "
			+URL+"/verification-service/password-reset.html?token=$tokenValue" + " Thank you!";

	public void verifyEmail(UserDto userDto, String resetToken) {

		// You can also set your keys this way. And it will work!
		System.setProperty("aws.accessKeyId", "AKIA37TFOHYR3JKDU2GN");
		System.setProperty("aws.secretKey", "DKqzlNtZkf3NcevMISpb+9qSpGtgQcytLgpQmvlg");

		AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
				.withRegion(Regions.AP_SOUTHEAST_2).build();
				
		String htmlBodyWithToken = HTMLBODY.replace("$tokenValue", userDto.getEmailVerificationToken()).replace("$resetTokenValue", resetToken);
		String textBodyWithToken = TEXTBODY.replace("$tokenValue", userDto.getEmailVerificationToken()).replace("$resetTokenValue", resetToken);;

		SendEmailRequest request = new SendEmailRequest()
				.withDestination(new Destination().withToAddresses(userDto.getEmail()))
				.withMessage(new Message()
						.withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(htmlBodyWithToken))
								.withText(new Content().withCharset("UTF-8").withData(textBodyWithToken)))
						.withSubject(new Content().withCharset("UTF-8").withData(SUBJECT)))
				.withSource(FROM);

		client.sendEmail(request);

		System.out.println("Email sent!");

	}

	public boolean sendPasswordResetRequest(String email, String token) {
		System.setProperty("aws.accessKeyId", "AKIA37TFOHYR3JKDU2GN");
		System.setProperty("aws.secretKey", "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");
		
		boolean returnValue = false;
		
		AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard().withRegion(Regions.AP_SOUTHEAST_2)
				.build();

		String htmlBodyWithToken = PASSWORD_RESET_HTMLBODY.replace("$tokenValue", token);

		String textBodyWithToken = PASSWORD_RESET_TEXTBODY.replace("$tokenValue", token);

		SendEmailRequest request = new SendEmailRequest().withDestination(new Destination().withToAddresses(email))
				.withMessage(new Message()
						.withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(htmlBodyWithToken))
								.withText(new Content().withCharset("UTF-8").withData(textBodyWithToken)))
						.withSubject(new Content().withCharset("UTF-8").withData(PASSWORD_RESET_SUBJECT)))
				.withSource(FROM);

		SendEmailResult result = client.sendEmail(request);
		if (result != null && (result.getMessageId() != null && !result.getMessageId().isEmpty())) {
			returnValue = true;
		}

		return returnValue;
	}
}
