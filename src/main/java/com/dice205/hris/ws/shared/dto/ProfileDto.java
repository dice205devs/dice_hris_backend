package com.dice205.hris.ws.shared.dto;

import java.io.Serializable;
import java.sql.Date;

import com.dice205.hris.ws.shared.CustomJSONObject;

public class ProfileDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5991799706519780774L;
	private long id;  
    private String employeeId;
    private CustomJSONObject personalInformation;
    private CustomJSONObject educationalBackground;
    private CustomJSONObject contactInformation;
	private CustomJSONObject governmentInformation;
	private CustomJSONObject employmentBackground;
	private CustomJSONObject workDetails;
    private String createdBy;
    private String updatedBy;
    private Date deletedAt;
    private Date updatedAt;
    private Date createdAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

	public CustomJSONObject getEducationalBackground() {
		return educationalBackground;
	}

	public void setEducationalBackground(CustomJSONObject educationalBackground) {
		this.educationalBackground = educationalBackground;
	}

	public CustomJSONObject getContactInformation() {
		return contactInformation;
	}

	public void setContactInformation(CustomJSONObject contactInformation) {
		this.contactInformation = contactInformation;
	}

	public CustomJSONObject getGovernmentInformation() {
		return governmentInformation;
	}

	public void setGovernmentInformation(CustomJSONObject governmentInformation) {
		this.governmentInformation = governmentInformation;
	}

	public CustomJSONObject getEmploymentBackground() {
		return employmentBackground;
	}

	public void setEmploymentBackground(CustomJSONObject employmentBackground) {
		this.employmentBackground = employmentBackground;
	}

	public CustomJSONObject getWorkDetails() {
		return workDetails;
	}

	public void setWorkDetails(CustomJSONObject workDetails) {
		this.workDetails = workDetails;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public CustomJSONObject getPersonalInformation() {
		return personalInformation;
	}

	public void setPersonalInformation(CustomJSONObject personalInformation) {
		this.personalInformation = personalInformation;
	}
}