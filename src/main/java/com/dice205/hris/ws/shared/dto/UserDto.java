package com.dice205.hris.ws.shared.dto;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import com.dice205.hris.ws.io.entity.CalendarEventEntity;
import com.dice205.hris.ws.io.entity.EmployeeRecordEntity;
import com.dice205.hris.ws.io.entity.PayrollRecordEntity;
import com.dice205.hris.ws.io.entity.RequestFormEntity;
import com.dice205.hris.ws.io.entity.TimesheetEntity;

public class UserDto implements Serializable {

	private static final long serialVersionUID = 6894010501030692751L;
	private long id;
	private String userId;
	private String email;
	private String supervisorId;
	private int maxEarlyTimeout = 3;
	private int earlyTimeoutCount = 0 ;
	private EmploymentStatus employmentStatus = EmploymentStatus.PROBATIONARY;
	private WorkingStatus workingStatus = WorkingStatus.OUT_OF_OFFICE;	
	private Time shift;
	private Collection<String> roles;
	private String password;
	private String encryptedPassword;
	private String emailVerificationToken;
	private Boolean emailVerificationStatus = false;
	private String createdBy;
	private String updatedBy;
	private Timestamp deletedAt;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Collection<TimesheetEntity> timesheet;
	private List<EmployeeRecordEntity> employeeRecord;
	private List<PayrollRecordEntity> payrollRecord;
	private List<RequestFormEntity> requestForm;
	private List<CalendarEventEntity> calendarEvents;

	public Boolean getEmailVerificationStatus() {
		return emailVerificationStatus;
	}

	public void setEmailVerificationStatus(Boolean emailVerificationStatus) {
		this.emailVerificationStatus = emailVerificationStatus;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public String getEmailVerificationToken() {
		return emailVerificationToken;
	}

	public void setEmailVerificationToken(String emailVerificationToken) {
		this.emailVerificationToken = emailVerificationToken;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	public int getMaxEarlyTimeout() {
		return maxEarlyTimeout;
	}

	public void setMaxEarlyTimeout(int maxEarlyTimeout) {
		this.maxEarlyTimeout = maxEarlyTimeout;
	}

	public int getEarlyTimeoutCount() {
		return earlyTimeoutCount;
	}

	public void setEarlyTimeoutCount(int earlyTimeoutCount) {
		this.earlyTimeoutCount = earlyTimeoutCount;
	}

	public Time getShift() {
		return shift;
	}

	public void setShift(Time shift) {
		this.shift = shift;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Collection<TimesheetEntity> getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(Collection<TimesheetEntity> timesheet) {
		this.timesheet = timesheet;
	}

	public List<EmployeeRecordEntity> getEmployeeRecord() {
		return employeeRecord;
	}

	public void setEmployeeRecord(List<EmployeeRecordEntity> employeeRecord) {
		this.employeeRecord = employeeRecord;
	}

	public List<PayrollRecordEntity> getPayrollRecord() {
		return payrollRecord;
	}

	public void setPayrollRecord(List<PayrollRecordEntity> payrollRecord) {
		this.payrollRecord = payrollRecord;
	}

	public List<RequestFormEntity> getRequestForm() {
		return requestForm;
	}

	public void setRequestForm(List<RequestFormEntity> requestForm) {
		this.requestForm = requestForm;
	}

	public Collection<String> getRoles() {
		return roles;
	}

	public void setRoles(Collection<String> roles) {
		this.roles = roles;
	}

	public List<CalendarEventEntity> getCalendarEvents() {
		return calendarEvents;
	}

	public void setCalendarEvents(List<CalendarEventEntity> calendarEvents) {
		this.calendarEvents = calendarEvents;
	}

	public WorkingStatus getWorkingStatus() {
		return workingStatus;
	}

	public void setWorkingStatus(WorkingStatus workingStatus) {
		this.workingStatus = workingStatus;
	}

	public EmploymentStatus getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(EmploymentStatus employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
}
