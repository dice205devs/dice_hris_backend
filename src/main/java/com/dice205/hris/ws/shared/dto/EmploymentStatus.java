package com.dice205.hris.ws.shared.dto;

public enum EmploymentStatus {
	PROBATIONARY, RESIGNED, REGULAR, INTERN, PART_TIME
}
