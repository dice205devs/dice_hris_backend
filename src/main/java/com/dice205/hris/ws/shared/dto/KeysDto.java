package com.dice205.hris.ws.shared.dto;

import java.io.Serializable;

import com.dice205.hris.ws.io.entity.SubscriptionKeysEntity;

public class KeysDto implements Serializable {

	private static final long serialVersionUID = -1792900204557844864L;
	private long id;
	private String auth;
	private String p256dh;

	public KeysDto() {}
	
	public KeysDto(SubscriptionKeysEntity keysEntity) {
		this.id = keysEntity.getId();
		this.auth = keysEntity.getAuth();
		this.p256dh = keysEntity.getP256dh();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getP256dh() {
		return p256dh;
	}

	public void setP256dh(String p256dh) {
		this.p256dh = p256dh;
	}
}
