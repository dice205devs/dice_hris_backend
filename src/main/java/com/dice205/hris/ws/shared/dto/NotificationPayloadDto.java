package com.dice205.hris.ws.shared.dto;

public class NotificationPayloadDto {
	private String title;
	private String content;

	public NotificationPayloadDto() {
	}

	public NotificationPayloadDto(String title, String content) {
		this.title = title;
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
