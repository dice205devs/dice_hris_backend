package com.dice205.hris.ws.shared;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.json.JSONException;
import org.json.JSONObject;

@Converter
public class JSONObjectConverter implements AttributeConverter<JSONObject, String> {

	@Override
	public String convertToDatabaseColumn(JSONObject attribute) {
		String json;
        try{
            json = attribute.toString();
        }
        catch (NullPointerException ex)
        {
//            ex.printStackTrace();
            json = "";
        }
        return json;
	}

	@Override
	public JSONObject convertToEntityAttribute(String dbData) {		
		JSONObject jsonData;
        try {
            jsonData = new JSONObject(dbData);
        } catch (JSONException ex) {
//        	 ex.printStackTrace();
            jsonData = null;
        }
        return jsonData;
	}

}
