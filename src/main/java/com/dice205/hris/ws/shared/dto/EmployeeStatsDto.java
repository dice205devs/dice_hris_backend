package com.dice205.hris.ws.shared.dto;

import java.io.Serializable;

public class EmployeeStatsDto implements Serializable {

	private static final long serialVersionUID = -8406393697647748205L;

	private int workingDays = 262;
	private int daysWorked;
	private int absences = 0;
	private int lates = 0;
	private double punctualityRate;
	private int leavesAvailable = 15;
	private int leavesUsed;
	private int sickLeavesAvailable = 15;
	private int sickLeavesUsed;

	public int getWorkingDays() {
		return workingDays;
	}

	public void setWorkingDays(int workingDays) {
		this.workingDays = workingDays;
	}

	public int getAbsences() {
		return absences;
	}

	public void setAbsences(int absences) {
		this.absences = absences;
	}

	public int getLates() {
		return lates;
	}

	public void setLates(int lates) {
		this.lates = lates;
	}

	public double getPunctualityRate() {
		return punctualityRate;
	}

	public void setPunctualityRate(double punctualityRate) {
		this.punctualityRate = punctualityRate;
	}

	public int getLeavesAvailable() {
		return leavesAvailable;
	}

	public void setLeavesAvailable(int leavesAvailable) {
		this.leavesAvailable = leavesAvailable;
	}

	public int getLeavesUsed() {
		return leavesUsed;
	}

	public void setLeavesUsed(int leavesUsed) {
		this.leavesUsed = leavesUsed;
	}

	public int getSickLeavesAvailable() {
		return sickLeavesAvailable;
	}

	public void setSickLeavesAvailable(int sickLeavesAvailable) {
		this.sickLeavesAvailable = sickLeavesAvailable;
	}

	public int getSickLeavesUsed() {
		return sickLeavesUsed;
	}

	public void setSickLeavesUsed(int sickLeavesUsed) {
		this.sickLeavesUsed = sickLeavesUsed;
	}

	public int getDaysWorked() {
		return daysWorked;
	}

	public void setDaysWorked(int daysWorked) {
		this.daysWorked = daysWorked;
	}
}
