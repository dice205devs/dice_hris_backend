package com.dice205.hris.ws.shared.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import com.dice205.hris.ws.io.entity.UserEntity;

public class PayrollRecordDto implements Serializable {

	private static final long serialVersionUID = 292404047660107973L;
	private long id;
	private String employeeName;
	private Date payrollRecordDate;
	private double cutOffRequiredHours = 0.0;
	private double cutOffHoursRendered = 0.0;
	private double cutOffBreaksRendered = 0.0;
	private double cutOffOBRendered = 0.0;
	private double cutOffOvertimeRendered = 0.0;
	private double cutOffBreaksOffset = 0.0;
	private int cutOffPeriod;
	private String createdBy;
	private String updatedBy;
	private Timestamp deletedAt;
	private Timestamp updatedAt;
	private Timestamp createdAt;
	private UserEntity user;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public double getCutOffHoursRendered() {
		return cutOffHoursRendered;
	}

	public void setCutOffHoursRendered(double cutOffHoursRendered) {
		this.cutOffHoursRendered = cutOffHoursRendered;
	}

	public int getCutOffPeriod() {
		return cutOffPeriod;
	}

	public void setCutOffPeriod(int cutOffPeriod) {
		this.cutOffPeriod = cutOffPeriod;
	}

	public Date getPayrollRecordDate() {
		return payrollRecordDate;
	}

	public void setPayrollRecordDate(Date payrollRecordDate) {
		this.payrollRecordDate = payrollRecordDate;
	}

	public double getCutOffRequiredHours() {
		return cutOffRequiredHours;
	}

	public void setCutOffRequiredHours(double cutOffRequiredHours) {
		this.cutOffRequiredHours = cutOffRequiredHours;
	}

	public double getCutOffBreaksRendered() {
		return cutOffBreaksRendered;
	}

	public void setCutOffBreaksRendered(double cutOffBreaksRendered) {
		this.cutOffBreaksRendered = cutOffBreaksRendered;
	}

	public double getCutOffOBRendered() {
		return cutOffOBRendered;
	}

	public void setCutOffOBRendered(double cutOffOBRendered) {
		this.cutOffOBRendered = cutOffOBRendered;
	}

	public double getCutOffOvertimeRendered() {
		return cutOffOvertimeRendered;
	}

	public void setCutOffOvertimeRendered(double cutOffOvertimeRendered) {
		this.cutOffOvertimeRendered = cutOffOvertimeRendered;
	}

	public double getCutOffBreaksOffset() {
		return cutOffBreaksOffset;
	}

	public void setCutOffBreaksOffset(double cutOffBreaksOffset) {
		this.cutOffBreaksOffset = cutOffBreaksOffset;
	}

}
