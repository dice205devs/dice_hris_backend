package com.dice205.hris.ws.shared.dto;

import java.io.Serializable;

import com.dice205.hris.ws.io.entity.UserEntity;

public class SubscriptionDto implements Serializable {

	private static final long serialVersionUID = -7123714187315864920L;

	private long id;
	private String subscriptionId;
	private String endpoint;
	private String userId;
	private UserEntity user;
	private KeysDto keys;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public KeysDto getKeys() {
		return keys;
	}

	public void setKeys(KeysDto keys) {
		this.keys = keys;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
