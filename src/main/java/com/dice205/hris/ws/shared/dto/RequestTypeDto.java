package com.dice205.hris.ws.shared.dto;

import java.io.Serializable;
import java.util.List;

import com.dice205.hris.ws.io.entity.RequestFormEntity;

public class RequestTypeDto implements Serializable {

	private static final long serialVersionUID = -264906007478545325L;

	private long id;

	private String name;
	
	private long fileByDate;
	
	private List<RequestFormEntity> requestForms;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getFileByDate() {
		return fileByDate;
	}

	public void setFileByDate(long fileByDate) {
		this.fileByDate = fileByDate;
	}

	public List<RequestFormEntity> getRequestForms() {
		return requestForms;
	}

	public void setRequestForms(List<RequestFormEntity> requestForms) {
		this.requestForms = requestForms;
	}
}
