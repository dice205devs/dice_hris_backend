package com.dice205.hris.ws.shared.dto;

public enum RequestTypes {
	OVERTIME(0), 
	VACATION_LEAVE(1209600000), 
	SICK_LEAVE(172800000), 
	EMERGENCY_LEAVE(172800000), 
	BIRTHDAY_LEAVE(604800000), 
	BEREAVEMENT_LEAVE(172800000);
	
	private long fileByDate;
	
	RequestTypes(long fileByDate) {
		this.fileByDate = fileByDate;
	}

	public long getFileByDate() {
		return fileByDate;
	}

	public void setFileByDate(long fileByDate) {
		this.fileByDate = fileByDate;
	}
}
