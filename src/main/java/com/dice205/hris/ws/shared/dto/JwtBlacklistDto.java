package com.dice205.hris.ws.shared.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class JwtBlacklistDto implements Serializable {

	private static final long serialVersionUID = -8885919443540883981L;
	private long id;
	private String jwt;
	private Timestamp dateBlacklisted;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

	public Timestamp getDateBlacklisted() {
		return dateBlacklisted;
	}

	public void setDateBlacklisted(Timestamp dateBlacklisted) {
		this.dateBlacklisted = dateBlacklisted;
	}
}
