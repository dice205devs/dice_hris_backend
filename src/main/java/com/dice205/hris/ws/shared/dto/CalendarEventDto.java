package com.dice205.hris.ws.shared.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

import com.dice205.hris.ws.io.entity.UserEntity;

public class CalendarEventDto implements Serializable {

	private static final long serialVersionUID = -3390796180656742258L;

	private long id;
	private String eventName;
	private Date eventDate;
	private HolidayTypes holidayType;
	private Time eventTime;
	private List<String> participantIds;
	private List<UserEntity> participants;
	private String createdBy;
	private String updatedBy;
	private Timestamp deletedAt;
	private Timestamp updatedAt;
	private Timestamp createdAt;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public List<UserEntity> getParticipants() {
		return participants;
	}

	public void setParticipants(List<UserEntity> participants) {
		this.participants = participants;
	}

	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	public List<String> getParticipantIds() {
		return participantIds;
	}

	public void setParticipantIds(List<String> participantIds) {
		this.participantIds = participantIds;
	}

	public HolidayTypes getHolidayType() {
		return holidayType;
	}

	public void setHolidayType(HolidayTypes holidayType) {
		this.holidayType = holidayType;
	}
}
