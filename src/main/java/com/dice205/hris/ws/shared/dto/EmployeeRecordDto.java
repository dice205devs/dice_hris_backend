package com.dice205.hris.ws.shared.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import com.dice205.hris.ws.io.entity.UserEntity;

public class EmployeeRecordDto implements Serializable {

	private static final long serialVersionUID = -5865095302062426197L;
	private long id;
	private String employeeName;
	private String position;
	private String shift;
	private boolean isLate;
	private double requiredHours = 7.5;
	private double breaksDuration = 0;
	private double breaksOffset = 0;
	private double breakOBDuration = 0;
	private double overtime = 0;
	private double hoursRendered = 0;
	private int cutOffPeriod;
	private Date employeeRecordDate;
	private String leaveType;
	private HolidayTypes holidayType;
	private String createdBy;
	private String updatedBy;
	private Timestamp deletedAt;
	private Timestamp updatedAt;
	private Timestamp createdAt;
	private UserEntity user;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public double getRequiredHours() {
		return requiredHours;
	}

	public void setRequiredHours(double requiredHours) {
		this.requiredHours = requiredHours;
	}

	public double getBreaksDuration() {
		return breaksDuration;
	}

	public void setBreaksDuration(double breaksDuration) {
		this.breaksDuration = breaksDuration;
	}

	public double getBreaksOffset() {
		return breaksOffset;
	}

	public void setBreaksOffset(double breaksOffset) {
		this.breaksOffset = breaksOffset;
	}

	public double getBreakOBDuration() {
		return breakOBDuration;
	}

	public void setBreakOBDuration(double breakOBDuration) {
		this.breakOBDuration = breakOBDuration;
	}

	public double getOvertime() {
		return overtime;
	}

	public void setOvertime(double overtime) {
		this.overtime = overtime;
	}

	public double getHoursRendered() {
		return hoursRendered;
	}

	public void setHoursRendered(double hoursRendered) {
		this.hoursRendered = hoursRendered;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getShift() {
		return shift;
	}

	public void setShift(String shift) {
		this.shift = shift;
	}

	public Date getEmployeeRecordDate() {
		return employeeRecordDate;
	}

	public void setEmployeeRecordDate(Date employeeRecordDate) {
		this.employeeRecordDate = employeeRecordDate;
	}

	public int getCutOffPeriod() {
		return cutOffPeriod;
	}

	public void setCutOffPeriod(int cutOffPeriod) {
		this.cutOffPeriod = cutOffPeriod;
	}

	public boolean isLate() {
		return isLate;
	}

	public void setLate(boolean isLate) {
		this.isLate = isLate;
	}

	public HolidayTypes getHolidayType() {
		return holidayType;
	}

	public void setHolidayType(HolidayTypes holidayType) {
		this.holidayType = holidayType;
	}

}
