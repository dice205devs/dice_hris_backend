package com.dice205.hris.ws.shared.dto;

public enum HolidayTypes {
	REGULAR, SPECIAL_NON_WORKING
}
