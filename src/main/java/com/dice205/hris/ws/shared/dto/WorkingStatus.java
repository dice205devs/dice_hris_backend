package com.dice205.hris.ws.shared.dto;

public enum WorkingStatus {
	OUT_OF_OFFICE, IN_OFFICE, BRB, LUNCH, OFFICIAL_BUSINESS, FLOATING_BREAK, ON_LEAVE, AWOL
}
