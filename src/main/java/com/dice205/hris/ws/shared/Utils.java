package com.dice205.hris.ws.shared;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.BeanUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.dice205.hris.ws.security.SecurityConstants;
import com.dice205.hris.ws.ui.model.json.JwtPayloadModel;
import com.dice205.hris.ws.ui.model.response.CurrentUserRest;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class Utils {
	private final Random RANDOM = new SecureRandom();
	private final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	public String generateUserId(int length) {
		return generateRandomString(length);
	}

	private String generateRandomString(int length) {
		StringBuilder returnValue = new StringBuilder(length);

		for (int i = 0; i < length; i++) {
			returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
		}

		return new String(returnValue);
	}

	public Timestamp generateTimestamp() {
		LocalDateTime dateTime = LocalDateTime.now();
		Instant instant = dateTime.atZone(ZoneId.of("Asia/Manila")).toInstant();
		long millis = instant.toEpochMilli();
		Timestamp timestamp = new Timestamp(millis);
		return timestamp;
	}

	public Time stringToTime(String time) {
		LocalTime localTime = LocalTime.parse(time, DateTimeFormatter.ISO_TIME);

		return Time.valueOf(localTime);
	}
	
	public static Date generateDate() {
		LocalDate localDate = LocalDate.now();
		Date date = Date.valueOf(localDate);
				
		return date;
	}

	public Date stringToDate(String date) {
		LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);

		return Date.valueOf(localDate);
	}

	public Time generateTimeNow() {
		LocalTime localTime = LocalTime.now();

		return Time.valueOf(localTime);
	}

	public BigDecimal getTimeDiff(Time endTime, Time startTime) {
		String time1 = endTime.toString();
		String time2 = startTime.toString();

		long timeInMilli1 = LocalTime.parse(time1).toNanoOfDay();
		long timeInMilli2 = LocalTime.parse(time2).toNanoOfDay();

		double diff = (TimeUnit.MILLISECONDS.toHours(timeInMilli1) - TimeUnit.MILLISECONDS.toHours(timeInMilli2));
		double diffHours = diff * 0.000001;

		BigDecimal bigDecimal = new BigDecimal(diffHours);

		return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	public static boolean hasTokenExpired(String token) {
		java.util.Date todayDate = new java.util.Date();
		Base64.Decoder decoder = Base64.getUrlDecoder();
		String src = token;
		
		String[] parts = src.split("\\."); // Splitting header, payload and signature
		
		ObjectMapper objMapper = new ObjectMapper();
				
		JwtPayloadModel jwtPayloadModel = new JwtPayloadModel();
		try {
			jwtPayloadModel = objMapper.readValue(decoder.decode(parts[1]), JwtPayloadModel.class);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		long expSecs = jwtPayloadModel.getExp(); 
		
		java.util.Date tokenExpirationDate = new Date(expSecs * 1000);
				
		return tokenExpirationDate.before(todayDate);
	}

	public String generateEmailVerificationToken(String userId) {
		String token = Jwts.builder().setSubject(userId)
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SecurityConstants.getTokenSecret()).compact();
		return token;
	}

	public String generatePasswordResetToken(String userId) {
		String token = Jwts.builder().setSubject(userId)
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.PASSWORD_RESET_EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SecurityConstants.getTokenSecret()).compact();
		return token;
	}

	public CurrentUserRest getAuth() {
		CurrentUserRest returnValue = new CurrentUserRest();
		List<String> roles = new ArrayList<>();

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

		List<GrantedAuthority> listAuthorities = new ArrayList<GrantedAuthority>();
		listAuthorities.addAll(authorities);

		for (int i = 0; i < listAuthorities.size(); i++) {
			String role = listAuthorities.get(i).getAuthority();
			roles.add(role);
		}

		BeanUtils.copyProperties(authentication.getPrincipal(), returnValue);
		returnValue.setRoles(roles);

		return returnValue;
	}

	public Object jsonToJavaObject(CustomJSONObject jsonObject, String model)
			throws ClassNotFoundException, JsonMappingException, JsonProcessingException {
		Class<?> classType = Class.forName("com.dice205.hris.ws.ui.model.json." + model);

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);

		DefaultPrettyPrinter pp = new DefaultPrettyPrinter();
		pp.indentArraysWith(new DefaultIndenter());
		objectMapper.setDefaultPrettyPrinter(pp);

		if (jsonObject == null) {
			return null;
		}
		
		CustomJSONObject json = new CustomJSONObject(jsonObject.toString());

		Object objectData = objectMapper.readValue(json.toString(), classType);

		return objectData;
	}
	
	public int getCutOffPeriod(Date date) {
		LocalDate localDate = date.toLocalDate();
		int cutOffPeriod = 1;

		if (localDate.getDayOfMonth() >= 16) {
			cutOffPeriod = 2;
		}
		
		return cutOffPeriod;
	}
	
	public String generateSubscriptionId(int length) {
		return generateRandomString(length);
	}
}
