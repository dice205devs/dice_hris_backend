package com.dice205.hris.ws.shared;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TimerTask;

import com.dice205.hris.ws.io.entity.CalendarEventEntity;
import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.repositories.CalendarEventRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.security.AppProperties;
import com.dice205.hris.ws.service.EmployeeRecordService;
import com.dice205.hris.ws.service.PushService;
import com.dice205.hris.ws.shared.dto.EmployeeRecordDto;
import com.dice205.hris.ws.shared.dto.HolidayTypes;
import com.dice205.hris.ws.shared.dto.NotificationPayloadDto;
import com.dice205.hris.ws.ui.model.request.ScheduledTasksConstants;

public class ScheduledTaskImpl extends TimerTask {
	private UserEntity userEntity;
	private PushService pushService;
	private CalendarEventRepository calendarEventRepository;
	private String method;
	private UserRepository userRepository;
	private EmployeeRecordService employeeRecordService;
	private Utils utils;
	private CalendarEventEntity calendarEventEntity;
	private AppProperties appProperties;

	public ScheduledTaskImpl() {

	}

	public ScheduledTaskImpl(UserEntity userEntity, PushService pushService) {
		this.userEntity = userEntity;
		this.pushService = pushService;
		this.method = ScheduledTasksConstants.OT;
	}

	public ScheduledTaskImpl(CalendarEventRepository calendarEventRepository, UserRepository userRepository,
			EmployeeRecordService employeeRecordService, Utils utils, AppProperties appProperties) {
		this.calendarEventRepository = calendarEventRepository;
		this.userRepository = userRepository;
		this.employeeRecordService = employeeRecordService;
		this.utils = utils;
		this.appProperties = appProperties;
		this.method = ScheduledTasksConstants.HOLIDAY;
	}

	public ScheduledTaskImpl(CalendarEventEntity calendarEventEntity, PushService pushService) {
		this.calendarEventEntity = calendarEventEntity;
		this.pushService = pushService;
		this.method = ScheduledTasksConstants.MEETING;
	}

	private void overtimeNotification() {
		NotificationPayloadDto notifPayload = new NotificationPayloadDto("Over time",
				"Talk with your immediate supervisor to resolve the exact time out");
		pushService.sendPushMessages(new String[] { userEntity.getUserId() }, notifPayload);
	}

	private void holiday() {
		Date date = Utils.generateDate();

		for (CalendarEventEntity calendarEventEntitySrc : calendarEventRepository
				.findByEventDateAndHolidayTypeIsNotNullAndDeletedAtIsNull(date)) {
			try {
				appProperties.setHolidayType(HolidayTypes.valueOf(calendarEventEntitySrc.getHolidayType()));
			} catch (NullPointerException e) {
				appProperties.setHolidayType(null);
			}
		}

		Iterable<UserEntity> userEntityIter = userRepository.findByDeletedAtIsNull();

		if (appProperties.getHolidayType() != null) {
			System.out.println("HOLIDAY TYPE: " + appProperties.getHolidayType());
			
			for (UserEntity userEntity : userEntityIter) {

				Collection<EmployeeRecordDto> employeeRecordDtoList = employeeRecordService.getEmployeeRecords(userEntity.getUserId(), date, date, 0, 999);
				
				if (employeeRecordDtoList.size() < 1) {
					EmployeeRecordDto employeeRecordDto = new EmployeeRecordDto();

					employeeRecordDto.setEmployeeRecordDate(date);
					employeeRecordDto.setHolidayType(appProperties.getHolidayType());
					employeeRecordDto.setCutOffPeriod(utils.getCutOffPeriod(date));
					employeeRecordDto.setCreatedBy("Server");

					EmployeeRecordDto createdEmployeeRecordDto = employeeRecordService
							.createEmployeeRecord(userEntity.getUserId(), employeeRecordDto);

					if (createdEmployeeRecordDto != null) {
						System.out.println(
								createdEmployeeRecordDto.getUser().getUserId() + " HOLIDAY EMPLOYEE RECORD UPDATED");
					}
				}
			}
		}

		this.earlyInHandler(userEntityIter, date);
	}

	private void earlyInHandler(Iterable<UserEntity> userEntityIter, Date date) {
		if (date.toLocalDate().getDayOfMonth() == 1 || date.toLocalDate().getDayOfMonth() == 01) {
			System.out.println("IS 1st OF MONTH EARLY SHIFT RESET");
			for (UserEntity userEntity : userEntityIter) {
				userEntity.setEarlyTimeoutCount(0);
				userEntity.setUpdatedBy("Server");
			}

			userRepository.saveAll(userEntityIter);
		}
	}

	private void meetingNotification() {
		NotificationPayloadDto notifPayload = new NotificationPayloadDto("Calendar Schedule",
				this.calendarEventEntity.getEventName() + " at " + this.calendarEventEntity.getEventTime());
		List<String> participantsList = new ArrayList<>();

		for (UserEntity userEntity : this.calendarEventEntity.getParticipants()) {
			participantsList.add(userEntity.getUserId());
			System.out.println(userEntity.getUserId());
		}

		System.out.println("MEETING ALERT");

		pushService.sendPushMessages(participantsList.toArray(new String[0]), notifPayload);
	}

	@Override
	public void run() {
		switch (method) {
		case ScheduledTasksConstants.OT: {
			overtimeNotification();
			break;
		}

		case ScheduledTasksConstants.HOLIDAY: {
			holiday();
			break;
		}

		case ScheduledTasksConstants.MEETING: {
			meetingNotification();
			break;
		}

		default:
			break;
		}

	}
}
