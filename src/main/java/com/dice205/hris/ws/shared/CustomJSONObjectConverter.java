package com.dice205.hris.ws.shared;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.json.JSONException;

@Converter
public class CustomJSONObjectConverter implements AttributeConverter<CustomJSONObject, String> {

	@Override
	public String convertToDatabaseColumn(CustomJSONObject attribute) {
		String json;
        try{
            json = attribute.toString();
        }
        catch (NullPointerException ex)
        {
//            ex.printStackTrace();
            json = "";
        }
        return json;
	}

	@Override
	public CustomJSONObject convertToEntityAttribute(String dbData) {
		CustomJSONObject jsonData;
        try {
            jsonData = new CustomJSONObject(dbData);
        } catch (JSONException ex) {
//        	 ex.printStackTrace();
            jsonData = null;
        }
        return jsonData;
	}

}
