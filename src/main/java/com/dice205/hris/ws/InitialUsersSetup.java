package com.dice205.hris.ws;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.dice205.hris.ws.io.entity.AuthorityEntity;
import com.dice205.hris.ws.io.entity.ProfileEntity;
import com.dice205.hris.ws.io.entity.RequestTypeEntity;
import com.dice205.hris.ws.io.entity.RoleEntity;
import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.repositories.AuthorityRepository;
import com.dice205.hris.ws.io.repositories.CalendarEventRepository;
import com.dice205.hris.ws.io.repositories.ProfileRepository;
import com.dice205.hris.ws.io.repositories.RequestTypeRepository;
import com.dice205.hris.ws.io.repositories.RoleRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.security.AppProperties;
import com.dice205.hris.ws.service.EmployeeRecordService;
import com.dice205.hris.ws.shared.CustomJSONObject;
import com.dice205.hris.ws.shared.ScheduledTaskImpl;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.EmploymentStatus;
import com.dice205.hris.ws.shared.dto.HolidayTypes;
import com.dice205.hris.ws.shared.dto.RequestTypes;
import com.dice205.hris.ws.shared.dto.WorkingStatus;
import com.dice205.hris.ws.ui.model.json.ContactInformationModel;
import com.dice205.hris.ws.ui.model.json.EducationalBackgroundModel;
import com.dice205.hris.ws.ui.model.json.EmploymentBackgroundModel;
import com.dice205.hris.ws.ui.model.json.GovernmentInformationModel;
import com.dice205.hris.ws.ui.model.json.PersonalInformationModel;
import com.dice205.hris.ws.ui.model.json.WorkDetailsModel;

@Component
@EnableScheduling
public class InitialUsersSetup {
	@Autowired
	AuthorityRepository authorityRepository;

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	AppProperties appProperties;
	
	@Autowired
	Utils utils;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ProfileRepository profileRepository;
	
	@Autowired
	RequestTypeRepository requestTypeRepository;
	
	@Autowired
	CalendarEventRepository calendarEventRepository;
	
	@Autowired
	EmployeeRecordService employeeRecordService;

	@EventListener
	@Transactional
	public void onApplicationEvent(ApplicationReadyEvent event) {
		AuthorityEntity readAuthority = createAuthority("READ_AUTHORITY");
		AuthorityEntity writeAuthority = createAuthority("WRITE_AUTHORITY");
		AuthorityEntity deleteAuthority = createAuthority("DELETE_AUTHORITY");

		RoleEntity roleUser = createRole("ROLE_USER", Arrays.asList(readAuthority, writeAuthority));
		RoleEntity roleApprover = createRole("ROLE_APPROVER", Arrays.asList(readAuthority, writeAuthority));
		RoleEntity roleAdmin = createRole("ROLE_ADMIN", Arrays.asList(readAuthority, writeAuthority, deleteAuthority));
		
		for (RequestTypes requestType: RequestTypes.values()) {
			createRequestType(requestType.name(), requestType.getFileByDate());
		}
		
		holidayHandler();
		
		if (userRepository.count() > 0) return;
		
		String publicId = utils.generateUserId(30);
		
		ProfileEntity profileEntity = new ProfileEntity();
		profileEntity.setEmployeeId(publicId);
		profileEntity.setCreatedBy(publicId);
		profileEntity.setCreatedAt(utils.generateTimestamp());
		profileEntity.setPersonalInformation(this.convertToJSON(new PersonalInformationModel()));
		profileEntity.setContactInformation(this.convertToJSON(new ContactInformationModel()));
	    profileEntity.setEducationalBackground(this.convertToJSON(new EducationalBackgroundModel()));
	    profileEntity.setEmploymentBackground(this.convertToJSON(new EmploymentBackgroundModel()));
        profileEntity.setWorkDetails(this.convertToJSON(new WorkDetailsModel()));
        profileEntity.setGovernmentInformation(this.convertToJSON(new GovernmentInformationModel()));
		
		UserEntity adminUser = new UserEntity();
		adminUser.setEmail("hris.admin@dice205.com");
		adminUser.setEmailVerificationStatus(true);
		adminUser.setShift(utils.stringToTime("08:00:00"));
		adminUser.setEmploymentStatus(EmploymentStatus.REGULAR.name());
		adminUser.setWorkingStatus(WorkingStatus.OUT_OF_OFFICE.name());
		adminUser.setSupervisorId(publicId);
		adminUser.setUserId(publicId);
		adminUser.setEncryptedPassword(bCryptPasswordEncoder.encode("p@ssw0rd"));
		adminUser.setRoles(Arrays.asList(roleAdmin));
		adminUser.setCreatedBy(publicId);
		adminUser.setCreatedAt(utils.generateTimestamp());
		adminUser.setProfile(profileEntity);
		
		userRepository.save(adminUser);		
	}

	@Transactional
	private AuthorityEntity createAuthority(String name) {
		AuthorityEntity authority = authorityRepository.findByName(name);

		if (authority == null) {
			authority = new AuthorityEntity(name);
			authorityRepository.save(authority);
		}

		return authority;
	}

	@Transactional
	private RoleEntity createRole(String name, Collection<AuthorityEntity> authorities) {
		RoleEntity role = roleRepository.findByName(name);

		if (role == null) {
			role = new RoleEntity(name);
			role.setAuthorities(authorities);
			roleRepository.save(role);
		}

		return role;
	}
	
	@Transactional
	private RequestTypeEntity createRequestType(String name, long fileByDate) {
		RequestTypeEntity requestTypeEntity = requestTypeRepository.findByName(name);

		if (requestTypeEntity == null) {
			requestTypeEntity = new RequestTypeEntity(name, fileByDate);
			requestTypeRepository.save(requestTypeEntity);
		}

		return requestTypeEntity;
	}
	
	private CustomJSONObject convertToJSON(Object object) {
		CustomJSONObject customJSONObject = new CustomJSONObject(object);
		
		return customJSONObject;
	}
	
	private void holidayHandler() {				
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 1);
		today.set(Calendar.SECOND, 0);

		// every day at 12:01am you run your task
		Timer timer = new Timer();
		timer.schedule(new ScheduledTaskImpl(calendarEventRepository, userRepository, employeeRecordService, utils, appProperties), today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
	}
}
