package com.dice205.hris.ws.service.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dice205.hris.ws.io.entity.PayrollRecordEntity;
import com.dice205.hris.ws.io.entity.ProfileEntity;
import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.repositories.PayrollRecordRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.service.EmployeeRecordService;
import com.dice205.hris.ws.service.PayrollRecordService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.PayrollRecordDto;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;

@Service
public class PayrollRecordServiceImpl implements PayrollRecordService {

	@Autowired
	PayrollRecordRepository payrollRecordRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	EmployeeRecordService employeeRecordService;
	
	@Autowired
	Utils utils;

	@Override
	public PayrollRecordDto timesheetCreatePayrollRecord(String userId, Date date) {
		PayrollRecordDto returnValue = new PayrollRecordDto();
		
		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);
		
		if (userEntity == null) throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());
		
		PayrollRecordEntity payrollRecordEntity = new PayrollRecordEntity();
		ProfileEntity profileEntity = userEntity.getProfile();
		
		String firstname = profileEntity.getPersonalInformation().getString("firstname");
		String lastname = profileEntity.getPersonalInformation().getString("lastname");
		LocalDate localDate = date.toLocalDate(); 
		
		if (localDate.getDayOfMonth() >= 16) {
			payrollRecordEntity.setCutOffPeriod(2);
		} else {
			payrollRecordEntity.setCutOffPeriod(1);
		}
		
		payrollRecordEntity.setEmployeeName(firstname + " " + lastname);
		payrollRecordEntity.setUser(userEntity);
		payrollRecordEntity.setCreatedBy(utils.getAuth().getUserId());
		payrollRecordEntity.setCreatedAt(utils.generateTimestamp());
		payrollRecordEntity.setPayrollRecordDate(Utils.generateDate());
		
		PayrollRecordEntity storedPayrollRecordEntity = payrollRecordRepository.save(payrollRecordEntity);
		
		if (storedPayrollRecordEntity == null) throw new RuntimeException("Payroll Record, " + ErrorMessages.COULD_NOT_CREATE_RECORD.getErrorMessage());
		
		BeanUtils.copyProperties(storedPayrollRecordEntity, returnValue);
		
		return returnValue;
	}

	@Override
	public PayrollRecordDto updatePayrollRecord(long id, PayrollRecordDto payrollRecordDto) {
		PayrollRecordDto returnValue = new PayrollRecordDto();
		
		Optional<PayrollRecordEntity> payrollRecordEntity = payrollRecordRepository.findById(id);
		
		if (!payrollRecordEntity.isPresent()) throw new RuntimeException("Payroll Record, " + ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		
		payrollRecordEntity.get().setUpdatedAt(utils.generateTimestamp());
		payrollRecordEntity.get().setUpdatedBy(utils.getAuth().getUserId());
		payrollRecordEntity.get().setCutOffHoursRendered(payrollRecordDto.getCutOffHoursRendered());
		payrollRecordEntity.get().setCutOffPeriod(payrollRecordDto.getCutOffPeriod());
		payrollRecordEntity.get().setPayrollRecordDate(payrollRecordDto.getPayrollRecordDate());
		payrollRecordEntity.get().setCutOffBreaksRendered(payrollRecordDto.getCutOffBreaksRendered());
		payrollRecordEntity.get().setCutOffBreaksOffset(payrollRecordDto.getCutOffBreaksOffset());
		payrollRecordEntity.get().setCutOffOBRendered(payrollRecordDto.getCutOffOBRendered());
		payrollRecordEntity.get().setCutOffOvertimeRendered(payrollRecordDto.getCutOffOvertimeRendered());
		payrollRecordEntity.get().setCutOffRequiredHours(payrollRecordDto.getCutOffRequiredHours());
		
		PayrollRecordEntity updatedPayrollRecordEntity = payrollRecordRepository.save(payrollRecordEntity.get());
		
		if (updatedPayrollRecordEntity == null)throw new RuntimeException("Payroll Record, " + ErrorMessages.COULD_NOT_UPDATE_RECORD.getErrorMessage());
		
		BeanUtils.copyProperties(updatedPayrollRecordEntity, returnValue);
		
		return returnValue;
	}

	@Override
	public List<PayrollRecordDto> getAllPayrollRecords(String userId, int year, int month, int cutOffPeriod, int page, int limit) {
		List<PayrollRecordDto> returnValue = new ArrayList<>();
		
		UserEntity userEntity = userRepository.findByUserId(userId);
		
		if (userEntity == null) throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());
		Pageable pageableRequest = PageRequest.of(page, limit);
		
		String monthToString = String.valueOf(month);
		String yearToString = String.valueOf(year);
		String cutOffPeriodToString = String.valueOf(cutOffPeriod);
		
		if (monthToString.length() == 1) {
			monthToString = "0" + monthToString;
		}
		
		String dateStringPattern = yearToString + "-" + monthToString + "%";
		
		if (month == 0 && year != 0) {
			dateStringPattern = yearToString + "%";
		}
		
		if (year == 0 && month !=0 ) {
			dateStringPattern = "_____" + monthToString + "%";
		}
		
		if (year == 0 && month == 0 ) {
			dateStringPattern = "%";
		}
		
		if (cutOffPeriod == 0) {
			cutOffPeriodToString = "%";
		}
		
		Page<PayrollRecordEntity> payrollRecordEntityList = payrollRecordRepository.getByUserAndCutOffPeriodAndPayrollRecordDate(userEntity, cutOffPeriodToString, dateStringPattern, pageableRequest);
		
		if (payrollRecordEntityList == null) throw new RuntimeException("Payroll Record, " + ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		
		for (PayrollRecordEntity payrollRecordEntity: payrollRecordEntityList) {
			PayrollRecordDto payrollRecordDto = new PayrollRecordDto();
			
			BeanUtils.copyProperties(payrollRecordEntity, payrollRecordDto);
			
			returnValue.add(payrollRecordDto);
		}
		
		return returnValue;
	}

	@Override
	public boolean deletePayrollRecord(long id) {
		boolean returnValue = false;
		
		Optional<PayrollRecordEntity> payrollRecordEntity = payrollRecordRepository.findById(id);
		
		if (payrollRecordEntity.get() != null) {
			payrollRecordEntity.get().setDeletedAt(utils.generateTimestamp());
			
			payrollRecordRepository.save(payrollRecordEntity.get());
			
			returnValue = true;
		}
		
		return returnValue;
	}

	@Override
	public PayrollRecordDto createPayrollRecord(String userId, PayrollRecordDto payrollRecordDto) {
		PayrollRecordDto returnValue = new PayrollRecordDto();
		UserEntity userEntity = userRepository.findByUserId(userId);
		
		if(userEntity == null) throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());
		PayrollRecordEntity payrollRecordEntity = new PayrollRecordEntity();
		
		BeanUtils.copyProperties(payrollRecordDto, payrollRecordEntity);
		
		String firstname = userEntity.getProfile().getPersonalInformation().getString("firstname");
		String lastname = userEntity.getProfile().getPersonalInformation().getString("lastname");
		
		payrollRecordEntity.setEmployeeName(firstname + " " + lastname);
		payrollRecordEntity.setCreatedAt(utils.generateTimestamp());
		payrollRecordEntity.setCreatedBy(utils.getAuth().getUserId());
		payrollRecordEntity.setUser(userEntity);
		
		PayrollRecordEntity storedPayrollRecordEntity = payrollRecordRepository.save(payrollRecordEntity);
		
		if (storedPayrollRecordEntity == null) throw new RuntimeException("Payroll Record, " + ErrorMessages.COULD_NOT_CREATE_RECORD.getErrorMessage());
		
		BeanUtils.copyProperties(storedPayrollRecordEntity, returnValue);
		
		return returnValue;
	}

}
