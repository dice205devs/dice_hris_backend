package com.dice205.hris.ws.service;

import java.util.Collection;
import java.util.Date;

import com.dice205.hris.ws.shared.dto.EmployeeRecordDto;

public interface EmployeeRecordService {
	Collection<EmployeeRecordDto> getEmployeeRecords(String userId, Date from, Date to, int page, int limit);
	EmployeeRecordDto createEmployeeRecord(String userId, EmployeeRecordDto employeeRecordDto);
	EmployeeRecordDto updateEmloyeeRecord(long id, EmployeeRecordDto employeeRecordDto);
	boolean softDeleteEmployeeRecord(long id);
	double getHoursPerCutOff(String userId, int year, int month, int cutOffPeriod);
	double getBreaksDurationPerCutOff(String userId, int year, int month, int cutOffPeriod);
	double getOfficialBusinessPerCutOff(String userId, int year, int month, int cutOffPeriod);
	double getOverBreakPerCutOff(String userId, int year, int month, int cutOffPeriod);
	double getOvertimePerCutOff(String userId, int year, int month, int cutOffPeriod);
}
