package com.dice205.hris.ws.service;

import java.util.List;

import com.dice205.hris.ws.shared.dto.RoleDto;

public interface RoleService {
	RoleDto getUserRole(long id);
	List<RoleDto> getAllUserRoles();
}
