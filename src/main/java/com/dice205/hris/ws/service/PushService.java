package com.dice205.hris.ws.service;

import java.util.List;

import com.dice205.hris.ws.shared.dto.NotificationPayloadDto;
import com.dice205.hris.ws.shared.dto.SubscriptionDto;

public interface PushService {
	SubscriptionDto subscribe(SubscriptionDto subscriptionDetails);
	SubscriptionDto getBySubscriptionId(String subscriptionId);
	List<SubscriptionDto> getByUsers(String[] userIds);
	List<SubscriptionDto> getAllSubscriptions();
	boolean unsubscribe(String endpoint, boolean isUnsubscribed);
	boolean sendPushMessages(String[] userIds, NotificationPayloadDto notificationPayload);
	NotificationPayloadDto newNotificationPayload(String title, String content);
}
