package com.dice205.hris.ws.service.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.dice205.hris.ws.io.entity.ProfileEntity;
import com.dice205.hris.ws.io.entity.TimesheetEntity;
import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.repositories.TimesheetRepository;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.TimesheetDto;

//@Service
public class TimesheetUtility {
	private Utils utils;
	private TimesheetRepository timesheetRepository;
	private UserEntity userEntity;
	private Date dateToday;
	private TimesheetDto timesheetDetails;

	public TimesheetUtility() {
	}

	public TimesheetUtility(UserEntity userEntity, TimesheetDto timesheetDetails,
			TimesheetRepository timesheetRepository, Utils utils) {
		this.userEntity = userEntity;
		this.dateToday = timesheetDetails.getTimesheetDate();
		this.timesheetDetails = timesheetDetails;
		this.timesheetRepository = timesheetRepository;
		this.utils = utils;
	}

	public double getRenderedHours() {
		Date dateToday = this.dateToday;

		List<TimesheetEntity> timesheetEntityList = timesheetRepository.findByUserAndTimesheetDate(userEntity,
				dateToday);

		List<TimesheetDto> timesheetDtoList = new ArrayList<>();

		double returnValue = 0.00;
//		double breakDuration = this.getBreakDuration(userEntity, timesheetDetails);
		double breakAllowance = 0.00;

//		if (breakDuration >= 1.0) {
//			breakAllowance = breakDuration - 1.0;
//			if (breakAllowance > 0.5) {
//				breakAllowance = 0.5;
//			}
//		}

		for (TimesheetEntity timesheetEntity : timesheetEntityList) {
				if (timesheetEntity.getOfficialBusiness().getJSONObject("reason") != null
						|| timesheetEntity.getBreakType() == null) {

					TimesheetDto timesheetDto = new TimesheetDto();
					BeanUtils.copyProperties(timesheetEntity, timesheetDto);
					timesheetDtoList.add(timesheetDto);
				}
		}

		for (int i = 0; i < timesheetDtoList.size(); i++) {
			try {
				if (timesheetDtoList.get(i).getTimeEnd() == null && timesheetDtoList.get(i).getTimeStart() != null) {
					timesheetDtoList.get(i).setTimeEnd(this.timesheetDetails.getTimeEnd());
				}

				double difference = utils
						.getTimeDiff(timesheetDtoList.get(i).getTimeEnd(), timesheetDtoList.get(i).getTimeStart())
						.doubleValue();
				returnValue += difference;

				System.out.println("Rendered In: " + timesheetDtoList.get(i).getTimeStart() + " Out: "
						+ timesheetDtoList.get(i).getTimeEnd());

			} catch (NullPointerException e) {

			}
		}

		BigDecimal bigDecimal = new BigDecimal(returnValue);

		return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() + breakAllowance;
	}

	public double getBreakDuration() {
		Date dateToday = this.dateToday;
		List<TimesheetEntity> timesheetEntityList = timesheetRepository.findByUserAndTimesheetDate(userEntity,
				dateToday);

		List<TimesheetDto> timesheetDtoList = new ArrayList<>();

		for (TimesheetEntity timesheetEntity : timesheetEntityList) {
			if (timesheetEntity.getOfficialBusiness().getJSONObject("reason")== null && timesheetEntity.getBreakType() != null) {

				TimesheetDto timesheetDto = new TimesheetDto();
				BeanUtils.copyProperties(timesheetEntity, timesheetDto);
				timesheetDtoList.add(timesheetDto);
			}
		}

		double returnValue = 0.00;

		for (int i = 0; i < timesheetDtoList.size(); i++) {
			try {

				if (timesheetDtoList.get(i).getTimeEnd() == null && timesheetDtoList.get(i).getTimeStart() != null) {
					timesheetDtoList.get(i).setTimeEnd(timesheetDetails.getTimeEnd());
				}

				double difference = utils
						.getTimeDiff(timesheetDtoList.get(i).getTimeEnd(), timesheetDtoList.get(i).getTimeStart())
						.doubleValue();
				returnValue += difference;

				System.out.println("Breaks In: " + timesheetDtoList.get(i).getTimeStart() + " Out: "
						+ timesheetDtoList.get(i).getTimeEnd());

			} catch (NullPointerException e) {

			}
		}

		BigDecimal bigDecimal = new BigDecimal(returnValue);

		return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	public double getOBDuration() {
		Date dateToday = this.dateToday;

		List<TimesheetEntity> timesheetEntityList = timesheetRepository
				.findByUserAndTimesheetDate(userEntity, dateToday);

		List<TimesheetDto> timesheetDtoList = new ArrayList<>();

		for (TimesheetEntity timesheetEntity : timesheetEntityList) {
			TimesheetDto timesheetDto = new TimesheetDto();
			BeanUtils.copyProperties(timesheetEntity, timesheetDto);

			if (!timesheetDto.getOfficialBusiness().isNull("reason")) {
				timesheetDtoList.add(timesheetDto);
			}
		}

		double returnValue = 0.00;

		for (int i = 0; i < timesheetDtoList.size(); i++) {
			try {
				if (timesheetDtoList.get(i).getTimeEnd() == null && timesheetDtoList.get(i).getTimeStart() != null) {
					timesheetDtoList.get(i).setTimeEnd(timesheetDetails.getTimeEnd());
				}

				double difference = utils
						.getTimeDiff(timesheetDtoList.get(i).getTimeEnd(), timesheetDtoList.get(i).getTimeStart())
						.doubleValue();
				returnValue += difference;

				System.out.println("OB In: " + timesheetDtoList.get(i).getTimeStart() + " Out: "
						+ timesheetDtoList.get(i).getTimeEnd());

			} catch (NullPointerException e) {

			}
		}

		BigDecimal bigDecimal = new BigDecimal(returnValue);

		return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	public String getEmployeeName() {
		ProfileEntity profileEntity = userEntity.getProfile();

		String firstname = profileEntity.getPersonalInformation().getString("firstname");
		String lastname = profileEntity.getPersonalInformation().getString("lastname");

		return firstname + " " + lastname;
	}

	public String getPosition() {
		ProfileEntity profileEntity = userEntity.getProfile();

		return profileEntity.getWorkDetails().getString("positionTitle");
	}

	public double getHoursToRender() {
		return 7.5 - this.getRenderedHours();
	}

	public boolean isLate() {
		boolean returnValue = false;

		BigDecimal timeDiff = utils.getTimeDiff(timesheetDetails.getTimeStart(), userEntity.getShift());

		if (timeDiff.doubleValue() > 0.5) {
			returnValue = true;
		}

		return returnValue;
	}

	public boolean isEarlyIn() {
		boolean returnValue = false;

		LocalTime localTime = userEntity.getShift().toLocalTime();

		Instant i = localTime.atDate(timesheetDetails.getTimesheetDate().toLocalDate())
				.toInstant(ZoneId.of("Asia/Manila").getRules().getOffset(Instant.now()));

		long earlyShiftInMilli = i.toEpochMilli() - 1800000;

		Time earlyShift = new Time(earlyShiftInMilli);

		if (timesheetDetails.getTimeStart().toLocalTime().isBefore(earlyShift.toLocalTime())) {
			returnValue = true;
		}

		return returnValue;
	}

	public boolean isEarlyOut() {
		boolean returnValue = false;

		LocalTime regShiftEnd = userEntity.getShift().toLocalTime().plus(32400000, ChronoUnit.MILLIS);

		Instant regShiftEndInstant = regShiftEnd.atDate(timesheetDetails.getTimesheetDate().toLocalDate())
				.toInstant(ZoneId.of("Asia/Manila").getRules().getOffset(Instant.now()));

		long earlyShiftInMilli = regShiftEndInstant.toEpochMilli() - 1800000;

		Time earlyShift = new Time(earlyShiftInMilli);

		if (timesheetDetails.getTimeEnd().toLocalTime().isBefore(earlyShift.toLocalTime())) {
			returnValue = true;
		}

		return returnValue;
	}
}
