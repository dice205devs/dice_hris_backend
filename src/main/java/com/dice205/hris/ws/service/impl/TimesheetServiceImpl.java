package com.dice205.hris.ws.service.impl;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Timer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dice205.hris.ws.exceptions.UserServiceException;
import com.dice205.hris.ws.io.entity.EmployeeRecordEntity;
import com.dice205.hris.ws.io.entity.TimesheetEntity;
import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.repositories.EmployeeRecordRepository;
import com.dice205.hris.ws.io.repositories.PayrollRecordRepository;
import com.dice205.hris.ws.io.repositories.TimesheetRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.security.AppProperties;
import com.dice205.hris.ws.service.EmployeeRecordService;
import com.dice205.hris.ws.service.PayrollRecordService;
import com.dice205.hris.ws.service.PushService;
import com.dice205.hris.ws.service.TimesheetService;
import com.dice205.hris.ws.shared.ScheduledTaskImpl;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.EmployeeRecordDto;
import com.dice205.hris.ws.shared.dto.PayrollRecordDto;
import com.dice205.hris.ws.shared.dto.TimesheetDto;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;

@Service
public class TimesheetServiceImpl implements TimesheetService {

	@Autowired
	TimesheetRepository timesheetRepository;

	@Autowired
	EmployeeRecordRepository employeeRecordRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	EmployeeRecordService employeeRecordService;

	@Autowired
	PayrollRecordService payrollRecordService;

	@Autowired
	PayrollRecordRepository payrollRecordRepository;

	@Autowired
	PushService pushService;

	@Autowired
	AppProperties appProperties;
	
	@Autowired
	Utils utils;

	private Timer timer;

	@Override
	public TimesheetDto timeIn(String userId, TimesheetDto timesheetDetails) {
		TimesheetDto returnValue = new TimesheetDto();
		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);

		if (userEntity == null)
			throw new UserServiceException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());

		TimesheetUtility timesheetUtility = new TimesheetUtility(userEntity, timesheetDetails, timesheetRepository,
				utils);

		Date dateToday = timesheetDetails.getTimesheetDate();

		TimesheetEntity isStillIn = timesheetRepository
				.findByUserAndTimesheetDateAndTimeEndIsNullAndTimeStartIsNotNullAndBreakTypeIsNull(userEntity,
						dateToday);

		if (!this.isTimedOut(userEntity, dateToday) || isStillIn != null)
			throw new RuntimeException("User has not timed out or is still in");

		this.isFromBreak(userEntity, timesheetDetails);

		this.createEmployeeRecord(userEntity, timesheetDetails, timesheetUtility);

		TimesheetEntity timesheetEntity = new TimesheetEntity();

		timesheetEntity.setEmployeeName(timesheetUtility.getEmployeeName());
		timesheetEntity.setCreatedBy(utils.getAuth().getUserId());
		timesheetEntity.setCreatedAt(utils.generateTimestamp());
		timesheetEntity.setUser(userEntity);
		timesheetEntity.setTimesheetDate(timesheetDetails.getTimesheetDate());
		timesheetEntity.setTimeStart(timesheetDetails.getTimeStart());
		timesheetEntity.setOfficialBusiness(timesheetDetails.getOfficialBusiness());

		TimesheetEntity storedTimesheetEntity = timesheetRepository.save(timesheetEntity);

		if (storedTimesheetEntity != null) {
			BeanUtils.copyProperties(storedTimesheetEntity, returnValue);
		}

		return returnValue;
	}

	@Override
	public TimesheetDto timeOut(String userId, TimesheetDto timesheetDetails) {
		TimesheetDto returnValue = new TimesheetDto();

		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);

		if (userEntity == null)
			throw new UserServiceException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());
		TimesheetUtility timesheetUtility = new TimesheetUtility(userEntity, timesheetDetails, timesheetRepository,
				utils);

		Date dateToday = timesheetDetails.getTimesheetDate();

		TimesheetEntity timesheetEntity = timesheetRepository
				.findByUserAndTimesheetDateAndTimeEndIsNullAndTimeStartIsNotNull(userEntity, dateToday);

		if (timesheetEntity == null)
			throw new RuntimeException("User is not back or not yet timed in");

		timesheetEntity.setTimeEnd(timesheetDetails.getTimeEnd());
		timesheetEntity.setUpdatedAt(utils.generateTimestamp());
		timesheetEntity.setUpdatedBy(utils.getAuth().getUserId());

		if (timesheetUtility.getRenderedHours() < 7.5 && timesheetDetails.getBreakType() == null)
			throw new RuntimeException("You need to render " + timesheetUtility.getHoursToRender() + " hrs");

		TimesheetEntity updatedTimesheetEntity = timesheetRepository.save(timesheetEntity);

		if (updatedTimesheetEntity != null) {

			// ========== EMPLOYEE RECORDS UPDATE ==========
			
			this.createEmployeeRecord(userEntity, timesheetDetails, timesheetUtility);

			this.updateEmployeeRecord(userEntity, timesheetDetails, timesheetUtility);

			// ==================== BRB/TIMEOUT ====================

			TimesheetEntity storedTimesheetEntity = this.handleBreakType(userEntity, timesheetDetails,
					timesheetUtility);
			
			// =============================================

			if (storedTimesheetEntity == null) {
				BeanUtils.copyProperties(updatedTimesheetEntity, returnValue);
			} else {
				BeanUtils.copyProperties(storedTimesheetEntity, returnValue);
			}

		}

		return returnValue;
	}

	@Override
	public List<TimesheetDto> getTimesheetRecord(String userId, java.util.Date date, int page, int limit) {
		List<TimesheetDto> returnValue = new ArrayList<>();

		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);

		if (userEntity == null)
			throw new UserServiceException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());

		Pageable pageableRequest = PageRequest.of(page, limit);

		Page<TimesheetEntity> timesheetPage = timesheetRepository
				.findAllByUserAndTimesheetDateOrderByTimeStartAsc(userEntity, date, pageableRequest);

		for (TimesheetEntity timesheetEntity : timesheetPage.getContent()) {
			TimesheetDto timesheetDto = new TimesheetDto();
			BeanUtils.copyProperties(timesheetEntity, timesheetDto);
			returnValue.add(timesheetDto);
		}

		return returnValue;
	}

	@Override
	public TimesheetDto createTimesheetRecord(String userId, TimesheetDto timesheetDto) {
		TimesheetDto returnValue = new TimesheetDto();

		UserEntity userEntity = userRepository.findByUserId(userId);

		if (userEntity == null)
			throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());
		TimesheetUtility timesheetUtility = new TimesheetUtility(userEntity, timesheetDto, timesheetRepository, utils);

		TimesheetEntity timesheetEntity = new TimesheetEntity();

		BeanUtils.copyProperties(timesheetDto, timesheetEntity);

		timesheetEntity.setEmployeeName(timesheetUtility.getEmployeeName());
		timesheetEntity.setCreatedAt(utils.generateTimestamp());
		timesheetEntity.setCreatedBy(utils.getAuth().getUserId());
		timesheetEntity.setOfficialBusiness(timesheetDto.getOfficialBusiness());
		timesheetEntity.setUser(userEntity);

		TimesheetEntity storedTimesheetEntity = timesheetRepository.save(timesheetEntity);

		if (storedTimesheetEntity == null)
			throw new RuntimeException(ErrorMessages.COULD_NOT_CREATE_RECORD.getErrorMessage());

		BeanUtils.copyProperties(storedTimesheetEntity, returnValue);

		return returnValue;
	}

	@Override
	public TimesheetDto updateTimesheetRecord(long id, TimesheetDto timesheetDto) {
		TimesheetDto returnValue = new TimesheetDto();

		Optional<TimesheetEntity> timesheetEntityOpt = timesheetRepository.findById(id);

		if (timesheetEntityOpt.get() == null)
			throw new RuntimeException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		TimesheetEntity timesheetEntity = timesheetEntityOpt.get();

		timesheetEntity.setTimesheetDate(timesheetDto.getTimesheetDate());
		timesheetEntity.setTimeStart(timesheetDto.getTimeStart());
		timesheetEntity.setTimeEnd(timesheetDto.getTimeEnd());
		timesheetEntity.setOfficialBusiness(timesheetDto.getOfficialBusiness());
		timesheetEntity.setBreakType(timesheetDto.getBreakType());
		timesheetEntity.setUpdatedAt(utils.generateTimestamp());
		timesheetEntity.setUpdatedBy(utils.getAuth().getUserId());

		TimesheetEntity updatedTimesheetEntity = timesheetRepository.save(timesheetEntity);

		if (updatedTimesheetEntity == null)
			throw new RuntimeException(ErrorMessages.COULD_NOT_UPDATE_RECORD.getErrorMessage());

		BeanUtils.copyProperties(updatedTimesheetEntity, returnValue);

		return returnValue;
	}

	private boolean hasDailyEmployeeRecord(String userId, Date date) {
		boolean returnValue = false;

		List<EmployeeRecordDto> employeeRecordList = new ArrayList<>();
		Instant instant = date.toLocalDate().atStartOfDay(ZoneOffset.UTC).toInstant();

		Collection<EmployeeRecordDto> initEmployeeRecordDto = employeeRecordService.getEmployeeRecords(userId,
				java.util.Date.from(instant), java.util.Date.from(instant), 0, 999);

		if (initEmployeeRecordDto.size() < 1) {
			return returnValue;
		}

		for (EmployeeRecordDto dto : initEmployeeRecordDto) {
			employeeRecordList.add(dto);
		}

		EmployeeRecordDto employeeRecordDto = employeeRecordList.get(0);

		if (employeeRecordDto != null) {
			returnValue = true;
		}

		return returnValue;
	}

	private void createEmployeeRecord(UserEntity userEntity, TimesheetDto timesheetDto,
			TimesheetUtility timesheetUtility) {
		Date dateToday = timesheetDto.getTimesheetDate();

		if (!this.hasDailyEmployeeRecord(userEntity.getUserId(), dateToday)) {
			EmployeeRecordDto employeeRecordDto = new EmployeeRecordDto();

			employeeRecordDto.setHolidayType(appProperties.getHolidayType());
			employeeRecordDto.setPosition(timesheetUtility.getPosition());
			employeeRecordDto.setEmployeeName(timesheetUtility.getEmployeeName());
			employeeRecordDto.setEmployeeRecordDate(dateToday);
			employeeRecordDto.setCutOffPeriod(utils.getCutOffPeriod(dateToday));
			employeeRecordDto.setLate(timesheetUtility.isLate());

			employeeRecordService.createEmployeeRecord(userEntity.getUserId(), employeeRecordDto);

			this.updateUserEarlyIn(userEntity, timesheetUtility);

			this.createPayrollRecord(userEntity, dateToday);
		}
	}

	private void createPayrollRecord(UserEntity userEntity, Date date) {
		if (!this.hasPayrollRecord(userEntity, date)) {
			payrollRecordService.timesheetCreatePayrollRecord(userEntity.getUserId(), date);
		}
	}

	private boolean hasPayrollRecord(UserEntity userEntity, Date date) {
		boolean returnValue = false;

		LocalDate localDate = date.toLocalDate();

		List<PayrollRecordDto> hasPayroll = payrollRecordService.getAllPayrollRecords(userEntity.getUserId(),
				localDate.getYear(), localDate.getMonthValue(), utils.getCutOffPeriod(date), 0, 999);

		if (!hasPayroll.isEmpty()) {
			returnValue = true;
		}

		return returnValue;
	}

	private void isFromBreak(UserEntity userEntity, TimesheetDto timesheetDetails) {
		TimesheetEntity fromBreak = timesheetRepository
				.findByUserAndTimesheetDateAndTimeEndIsNullAndTimeStartIsNotNullAndBreakTypeIsNotNull(userEntity,
						timesheetDetails.getTimesheetDate());

		if (fromBreak != null) {
			if (timesheetDetails.getTimeStart().before(fromBreak.getTimeStart()))
				throw new RuntimeException("Input time must not be before the previous start time");

			fromBreak.setTimeEnd(timesheetDetails.getTimeStart());
			fromBreak.setUpdatedAt(utils.generateTimestamp());
			fromBreak.setUpdatedBy(utils.getAuth().getUserId());

			timesheetRepository.save(fromBreak);
		}
	}

	private TimesheetEntity handleBreakType(UserEntity userEntity, TimesheetDto timesheetDto,
			TimesheetUtility timesheetUtility) {
		if (timesheetDto.getBreakType() != null) {
			TimesheetEntity timesheetBreak = new TimesheetEntity();

			timesheetBreak.setEmployeeName(timesheetUtility.getEmployeeName());
			timesheetBreak.setTimeStart(timesheetDto.getTimeEnd());
			timesheetBreak.setBreakType(timesheetDto.getBreakType());
			timesheetBreak.setUser(userEntity);
			timesheetBreak.setOfficialBusiness(timesheetDto.getOfficialBusiness());
			timesheetBreak.setTimesheetDate(timesheetDto.getTimesheetDate());
			timesheetBreak.setCreatedAt(utils.generateTimestamp());
			timesheetBreak.setCreatedBy(utils.getAuth().getUserId());

			return timesheetRepository.save(timesheetBreak);
		} else {
			int cutOffPeriod = 1;
			LocalDate localDate = timesheetDto.getTimesheetDate().toLocalDate();
			String userId = userEntity.getUserId();
			int year = localDate.getYear();
			int month = localDate.getMonthValue();

			if (localDate.getDayOfMonth() >= 16) {
				cutOffPeriod = 2;
			}

			List<PayrollRecordDto> payrollRecordDtoList = payrollRecordService.getAllPayrollRecords(
					userEntity.getUserId(), localDate.getYear(), localDate.getMonthValue(), cutOffPeriod, 0, 999);

			PayrollRecordDto payrollRecordDto;

			if (payrollRecordDtoList.isEmpty()) {
				PayrollRecordDto newPayrollRecordDto = new PayrollRecordDto();
				newPayrollRecordDto.setPayrollRecordDate(timesheetDto.getTimesheetDate());
				payrollRecordDto = payrollRecordService.createPayrollRecord(userId, newPayrollRecordDto);
			} else {
				payrollRecordDto = payrollRecordDtoList.get(0);
			}

			payrollRecordDto
					.setCutOffHoursRendered(employeeRecordService.getHoursPerCutOff(userId, year, month, cutOffPeriod));
			payrollRecordDto.setCutOffBreaksRendered(
					employeeRecordService.getBreaksDurationPerCutOff(userId, year, month, cutOffPeriod));
			payrollRecordDto.setCutOffBreaksOffset(
					employeeRecordService.getOverBreakPerCutOff(userId, year, month, cutOffPeriod));
			payrollRecordDto.setCutOffOvertimeRendered(
					employeeRecordService.getOvertimePerCutOff(userId, year, month, cutOffPeriod));
			payrollRecordDto.setCutOffOBRendered(
					employeeRecordService.getOfficialBusinessPerCutOff(userId, year, month, cutOffPeriod));

			payrollRecordService.updatePayrollRecord(payrollRecordDto.getId(), payrollRecordDto);

			try {
				timer.cancel();
				timer.purge();
				System.out.println("TIMESHEET: TIMER CANCELLED");
			} catch (NullPointerException e) {
				System.out.println("TIMESHEET: NO TIMER TO CANCEL");
			}

			if (timesheetUtility.isEarlyOut()) {
				pushService.sendPushMessages(new String[] { userEntity.getUserId() },
						pushService.newNotificationPayload("Time Out", "Early time out"));
			}

			return null;
		}
	}

	private void updateEmployeeRecord(UserEntity userEntity, TimesheetDto timesheetDetails,
			TimesheetUtility timesheetUtility) {
		List<EmployeeRecordDto> employeeRecordList = new ArrayList<>();
		Date dateToday = timesheetDetails.getTimesheetDate();
		Instant instant = dateToday.toLocalDate().atStartOfDay(ZoneOffset.UTC).toInstant();
		double breaksDuration = timesheetUtility.getBreakDuration();
		double renderedHours = timesheetUtility.getRenderedHours();
		double breaksOffset = breaksDuration - 1.5;

		Collection<EmployeeRecordDto> initEmployeeRecordDto = employeeRecordService.getEmployeeRecords(
				userEntity.getUserId(), java.util.Date.from(instant), java.util.Date.from(instant), 0, 999);

		for (EmployeeRecordDto dto : initEmployeeRecordDto) {
			employeeRecordList.add(dto);
		}

		EmployeeRecordDto employeeRecordDto = employeeRecordList.get(0);

		employeeRecordDto.setShift(this.getShift(userEntity, timesheetDetails));
		employeeRecordDto.setBreaksDuration(breaksDuration);
		employeeRecordDto.setHoursRendered(renderedHours);
		employeeRecordDto.setBreakOBDuration(timesheetUtility.getOBDuration());

		if (breaksDuration > 1.5) {
			employeeRecordDto.setBreaksOffset(breaksOffset);
		}

		EmployeeRecordEntity employeeRecordEntity = employeeRecordRepository.findByUserAndEmployeeRecordDate(userEntity,
				dateToday);

		employeeRecordService.updateEmloyeeRecord(employeeRecordEntity.getId(), employeeRecordDto);

		System.out.println("Rendered hours: " + renderedHours);
		System.out.println("Break Duration: " + breaksDuration);
	}

	private boolean isTimedOut(UserEntity userEntity, Date date) {
		boolean returnValue = false;
		TimesheetEntity timesheetEntity = timesheetRepository
				.findByUserAndTimesheetDateLessThanAndTimeEndIsNullAndTimeStartIsNotNullAndBreakTypeIsNull(userEntity,
						date);

		if (timesheetEntity == null) {
			returnValue = true;
		}

		return returnValue;
	}

	private String getShift(UserEntity userEntity, TimesheetDto timesheetDetails) {
		List<TimesheetDto> timesheetDtoList = this.getTimesheetRecord(userEntity.getUserId(),
				timesheetDetails.getTimesheetDate(), 0, 999);
		String shiftStart = timesheetDtoList.get(0).getTimeStart().toString();
		String shiftEnd = timesheetDetails.getTimeEnd().toString();

		return shiftStart + "-" + shiftEnd;
	}

	private void updateUserEarlyIn(UserEntity userEntity, TimesheetUtility timesheetUtility) {
		if (timesheetUtility.isEarlyIn()) {
			if (userEntity.getEarlyTimeoutCount() < 3) {
				userEntity.setEarlyTimeoutCount(userEntity.getEarlyTimeoutCount() + 1);

				userRepository.save(userEntity);

				System.out.println("TIMESHEET EARLY IN: UPDATED");
			} else {
				Date scheduledDate = new Date(System.currentTimeMillis() + 36000000);
				timer = new Timer();
				timer.schedule(new ScheduledTaskImpl(userEntity, pushService), scheduledDate);

				pushService.sendPushMessages(new String[] { userEntity.getUserId() },
						pushService.newNotificationPayload("Allowed Early Time In Exceeded",
								"You must time out at the end of your regular shift"));
			}
		}
	}
}
