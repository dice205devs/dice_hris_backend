package com.dice205.hris.ws.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dice205.hris.ws.io.entity.AuditLogsEntity;
import com.dice205.hris.ws.io.repositories.AuditLogsRepository;
import com.dice205.hris.ws.service.AuditLogService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.AuditLogDto;

@Service
public class AuditLogServiceImpl implements AuditLogService {

	@Autowired
	AuditLogsRepository auditLogRepository;
	
	@Autowired
	Utils utils;
	
	@Override
	public boolean createAuditLog(AuditLogDto auditDetails) {
		boolean returnValue = false;
		
		AuditLogsEntity auditLogEntity = new AuditLogsEntity();
		
		BeanUtils.copyProperties(auditDetails, auditLogEntity);
		
		auditLogEntity.setUserId(utils.getAuth().getUserId());
		auditLogEntity.setDateTime(utils.generateTimestamp());
		
		AuditLogsEntity storedAuditLog = auditLogRepository.save(auditLogEntity);
		
		if (storedAuditLog != null) {
			return true;
		}
		
		return returnValue;
	}
}
