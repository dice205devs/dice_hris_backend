package com.dice205.hris.ws.service;

import java.util.Date;
import java.util.List;

import com.dice205.hris.ws.shared.dto.TimesheetDto;

public interface TimesheetService {
	TimesheetDto timeIn(String userId, TimesheetDto timesheetDetails);
	TimesheetDto timeOut(String userId, TimesheetDto timesheetDetails);
	List<TimesheetDto> getTimesheetRecord(String userId, Date date, int page, int limit);
	TimesheetDto createTimesheetRecord(String userId, TimesheetDto timesheetDto);
	TimesheetDto updateTimesheetRecord(long id, TimesheetDto timesheetDto);
}
