package com.dice205.hris.ws.service.impl;

import java.time.LocalDate;
import java.time.LocalTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.Timer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dice205.hris.ws.io.entity.CalendarEventEntity;
import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.repositories.CalendarEventRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.service.CalendarEventService;
import com.dice205.hris.ws.service.PushService;
import com.dice205.hris.ws.shared.ScheduledTaskImpl;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.CalendarEventDto;
import com.dice205.hris.ws.shared.dto.HolidayTypes;
import com.dice205.hris.ws.shared.dto.NotificationPayloadDto;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;

@Service
public class CalendarEventServiceImpl implements CalendarEventService {

	@Autowired
	CalendarEventRepository calendarEventRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PushService pushService;

	@Autowired
	Utils utils;

	private Timer timer;

	@Override
	public CalendarEventDto createCalendarEvent(CalendarEventDto calendarEventDetails) {
		CalendarEventDto returnValue = new CalendarEventDto();

		CalendarEventEntity calendarEventEntity = new CalendarEventEntity();

		BeanUtils.copyProperties(calendarEventDetails, calendarEventEntity);

		calendarEventEntity.setHolidayType(calendarEventDetails.getHolidayType().name());
		calendarEventEntity.setCreatedBy(utils.getAuth().getUserId());
		calendarEventEntity.setCreatedAt(utils.generateTimestamp());

		CalendarEventEntity storedCalendarEventEntity = calendarEventRepository.save(calendarEventEntity);

		if (storedCalendarEventEntity == null)
			throw new RuntimeException("Calendar Event, " + ErrorMessages.COULD_NOT_CREATE_RECORD.getErrorMessage());

		BeanUtils.copyProperties(storedCalendarEventEntity, returnValue);

		returnValue.setHolidayType(HolidayTypes.valueOf(storedCalendarEventEntity.getHolidayType()));

		return returnValue;
	}

	@Override
	public CalendarEventDto updateCalendarEvent(long id, CalendarEventDto calendarEventDto) {
		CalendarEventDto returnValue = new CalendarEventDto();

		Optional<CalendarEventEntity> calendarEventEntityOpt = calendarEventRepository.findById(id);

		if (!calendarEventEntityOpt.isPresent())
			throw new RuntimeException("Calendar Event, " + ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		CalendarEventEntity calendarEventEntity = calendarEventEntityOpt.get();

		calendarEventEntity.setEventName(calendarEventDto.getEventName());
		calendarEventEntity.setEventDate(calendarEventDto.getEventDate());
		calendarEventEntity.setHolidayType(calendarEventDto.getHolidayType().name());
		calendarEventEntity.setUpdatedBy(utils.getAuth().getUserId());
		calendarEventEntity.setUpdatedAt(utils.generateTimestamp());

		CalendarEventEntity updatedCalendarEventEntity = calendarEventRepository.save(calendarEventEntity);

		if (updatedCalendarEventEntity == null)
			throw new RuntimeException("Calendar Event, " + ErrorMessages.COULD_NOT_UPDATE_RECORD.getErrorMessage());

		BeanUtils.copyProperties(updatedCalendarEventEntity, returnValue);

		returnValue.setHolidayType(HolidayTypes.valueOf(updatedCalendarEventEntity.getHolidayType()));
		
		return returnValue;
	}

	@Override
	public boolean deleteCalendarEvent(long id) {
		Optional<CalendarEventEntity> calendarEventEntityOpt = calendarEventRepository.findById(id);

		Boolean bool = false;

		if (calendarEventEntityOpt.isPresent()) {

			CalendarEventEntity calendarEventEntity = calendarEventEntityOpt.get();

			calendarEventEntity.setDeletedAt(utils.generateTimestamp());
			calendarEventRepository.save(calendarEventEntity);

			this.cancelScheduleAlert();

			bool = true;
		}

		return bool;
	}

	@Override
	public List<CalendarEventDto> getByEventDate(int year, int month, int day) {
		List<CalendarEventDto> returnValue = new ArrayList<>();

		List<CalendarEventEntity> calendarEventEntityList = calendarEventRepository
				.findByEventDate(this.toDateQueryString(year, month, day));

		for (CalendarEventEntity calendarEventEntity : calendarEventEntityList) {
			CalendarEventDto calendarEventDto = new CalendarEventDto();

			BeanUtils.copyProperties(calendarEventEntity, calendarEventDto);

			calendarEventDto.setHolidayType(HolidayTypes.valueOf(calendarEventEntity.getHolidayType()));
			
			returnValue.add(calendarEventDto);
		}

		return returnValue;
	}

	private void alertParticipants(CalendarEventEntity calendarEventEntity) {
		LocalDate localDate = calendarEventEntity.getEventDate().toLocalDate();
		LocalTime localTime = calendarEventEntity.getEventTime().toLocalTime();

		NotificationPayloadDto notifPayload = new NotificationPayloadDto("Invitation",
				calendarEventEntity.getEventName() + " on " + localDate + "at" + localTime);
		List<String> participantsList = new ArrayList<>();

		for (UserEntity userEntity : calendarEventEntity.getParticipants()) {
			participantsList.add(userEntity.getUserId());
			System.out.println(userEntity.getUserId());
		}

		pushService.sendPushMessages(participantsList.toArray(new String[0]), notifPayload);

		// ===============================================================================

		Calendar today = Calendar.getInstance();
		today.set(Calendar.YEAR, localDate.getYear());
		today.set(Calendar.DAY_OF_MONTH, localDate.getDayOfMonth());
		today.set(Calendar.MONTH, localDate.getMonth().ordinal());
		today.set(Calendar.HOUR_OF_DAY, localTime.getHour());
		today.set(Calendar.MINUTE, localTime.getMinute());
		today.set(Calendar.SECOND, localTime.getSecond());

		timer = new Timer();
//		timer.schedule(new ScheduledTaskImpl(calendarEventEntity, pushService), today.getTime(), TimeUnit.MILLISECONDS.convert(ChronoUnit.DAYS.between(Utils.generateDate().toLocalDate(), localDate), TimeUnit.DAYS));

		timer.schedule(new ScheduledTaskImpl(calendarEventEntity, pushService), today.getTime());
	}

	private void cancelScheduleAlert() {
		try {
			timer.cancel();
			timer.purge();
			System.out.println("CALENDAR EVENT SCHEDULE: TIMER CANCELLED");
		} catch (NullPointerException e) {
			System.out.println("CALENDAR EVENT SCHEDULE: NO TIMER TO CANCEL");
		}
	}

	private String toDateQueryString(int year, int month, int day) {
		String yearString = String.valueOf(year);
		String monthString = String.valueOf(month);
		String dayString = String.valueOf(day);

		if (year == 0) {
			yearString = "____";
		}

		if (month == 0) {
			monthString = "__";
		}

		if (day == 0) {
			dayString = "__";
		}

		if (monthString.length() == 1) {
			monthString = "0" + monthString;
		}

		if (dayString.length() == 1) {
			dayString = "0" + dayString;
		}

		return yearString + "_" + monthString + "_" + dayString;
	}

	@Override
	public CalendarEventDto createMeetingSchedule(CalendarEventDto calendarEventDetails) {
		CalendarEventDto returnValue = new CalendarEventDto();

		CalendarEventEntity calendarEventEntity = new CalendarEventEntity();

		BeanUtils.copyProperties(calendarEventDetails, calendarEventEntity);

		List<UserEntity> participantList = new ArrayList<>();

		try {
			for (String participantId : calendarEventDetails.getParticipantIds()) {

				UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(participantId);

				participantList.add(userEntity);
			}
			
		} catch (NullPointerException e) {

		}

		calendarEventEntity.setParticipants(participantList);
		calendarEventEntity.setCreatedBy(utils.getAuth().getUserId());
		calendarEventEntity.setCreatedAt(utils.generateTimestamp());

		CalendarEventEntity storedCalendarEventEntity = calendarEventRepository.save(calendarEventEntity);

		if (storedCalendarEventEntity == null)
			throw new RuntimeException("Meeting Schedule, " + ErrorMessages.COULD_NOT_CREATE_RECORD.getErrorMessage());

		BeanUtils.copyProperties(storedCalendarEventEntity, returnValue);
		
		this.alertParticipants(storedCalendarEventEntity);

		return returnValue;
	}

}
