package com.dice205.hris.ws.service;

import java.util.List;

import com.dice205.hris.ws.shared.dto.RequestFormDto;
import com.dice205.hris.ws.shared.dto.RequestTypeDto;

public interface RequestFormService {
	List<RequestFormDto> getRequestForms(String userId, int page, int limit);
	List<RequestFormDto> getAllBySupervisor(String supervisorId, int page, int limit);
	List<RequestFormDto> getAllRequestForms(int page, int limit);
	RequestFormDto createRequestForm(String userId, RequestFormDto requestFormDto);
	RequestFormDto updateRequestForm(long id, RequestFormDto requestFormDto);
	List<RequestTypeDto> getRequestTypes();
}
