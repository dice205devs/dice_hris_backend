package com.dice205.hris.ws.service;

import java.sql.Date;
import java.util.List;

import com.dice205.hris.ws.shared.dto.PayrollRecordDto;

public interface PayrollRecordService {
	List<PayrollRecordDto> getAllPayrollRecords(String userId, int year, int month, int cuttOffPeriod, int page, int limit);
	PayrollRecordDto timesheetCreatePayrollRecord(String userId, Date date);
	PayrollRecordDto updatePayrollRecord(long id, PayrollRecordDto payrollRecordDto);
	PayrollRecordDto createPayrollRecord(String userId, PayrollRecordDto payrollRecordDto);
	boolean deletePayrollRecord(long id);
}
