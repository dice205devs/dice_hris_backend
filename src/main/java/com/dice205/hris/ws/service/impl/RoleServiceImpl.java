package com.dice205.hris.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dice205.hris.ws.io.entity.RoleEntity;
import com.dice205.hris.ws.io.repositories.RoleRepository;
import com.dice205.hris.ws.service.RoleService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.RoleDto;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleRepository userRoleRepository;

	@Autowired
	Utils utils;

	@Override
	public RoleDto getUserRole(long roleId) {
		RoleDto returnValue = new RoleDto();
		Optional<RoleEntity> roleEntity = userRoleRepository.findById(roleId);

		BeanUtils.copyProperties(roleEntity.get(), returnValue);
		
		return returnValue;
	}

	@Override
	public List<RoleDto> getAllUserRoles() {
		List<RoleDto> returnValue = new ArrayList<>();
		List<RoleEntity> userRoleEntities = new ArrayList<>();

		Iterable<RoleEntity> source = userRoleRepository.findAll();
		Iterator<RoleEntity> iterator = source.iterator();
		
		iterator.forEachRemaining(userRoleEntities::add);
		
		for (int i = 0; i < userRoleEntities.size(); i++) {
			RoleDto userRoleDto = new RoleDto();
			
			BeanUtils.copyProperties(userRoleEntities.get(i), userRoleDto);
			
			returnValue.add(userRoleDto);
		}
		
		return returnValue;
	}

}
