package com.dice205.hris.ws.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.dice205.hris.ws.shared.dto.UserDto;

public interface UserService extends UserDetailsService {
	UserDto createUser(UserDto user);
	UserDto getUser(String email);
	UserDto getUserByUserId(String userId);
	List<UserDto> getAllUserByUserId(String[] userIds);
	UserDto updateUser(String userId, UserDto userDetails);
	Boolean softDeleteUser(String userId);
	List<UserDto> getAllUsers(int page, int limit);
	List<UserDto> getArchive(int page, int limit);
	String verifyEmailToken(String token);
	boolean resetPassword(String token, String password);
	boolean requestPasswordReset(String email);
	boolean logout(String jwt);
	List<UserDto> getAllUsersByRole(String roleName, int page, int limit);
	boolean changePassword(String password, String newPassword);
}
