package com.dice205.hris.ws.service.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.service.EmployeeRecordService;
import com.dice205.hris.ws.service.EmployeeStatsService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.EmployeeRecordDto;
import com.dice205.hris.ws.shared.dto.EmployeeStatsDto;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;

import java.math.BigDecimal;

@Service
public class EmployeeStatsServiceImpl implements EmployeeStatsService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	EmployeeRecordService employeeRecordService;

	@Autowired
	Utils utils;

	@Override
	public EmployeeStatsDto getEmployeeStats(String userId, Date date) {
		EmployeeStatsDto returnValue = new EmployeeStatsDto();

		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);

		if (userEntity == null)
			throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());

		LocalDate localDate = date.toLocalDate();
		int sickLeavesUsed = this.getSickLeaves(userEntity, localDate);
		int getLeavesUsed = this.getLeavesUsed(userEntity, localDate);
		int daysWorked = this.getDaysWorked(userEntity, localDate);
		int lates = this.getLates(userEntity, localDate);
		BigDecimal bd = new BigDecimal(((float) lates) / ((float) daysWorked) * ((float) 100));
		BigDecimal scaled = new BigDecimal(100 - bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		double punctualityRate = scaled.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();

		returnValue.setDaysWorked(daysWorked);
		returnValue.setAbsences(this.getAbsences(userEntity, localDate));
		returnValue.setLates(lates);
		returnValue.setSickLeavesUsed(this.getSickLeaves(userEntity, localDate));
		returnValue.setSickLeavesAvailable(returnValue.getSickLeavesAvailable() - sickLeavesUsed);
		returnValue.setPunctualityRate(punctualityRate);
		returnValue.setLeavesUsed(getLeavesUsed);
		returnValue.setLeavesAvailable(returnValue.getLeavesAvailable() - getLeavesUsed);

		return returnValue;
	}

	private int getDaysWorked(UserEntity userEntity, LocalDate localDate) {
		int returnValue = 0;

		Date firstOfYear = Date.valueOf(localDate.with(firstDayOfYear()));
		Date lastOfYear = Date.valueOf(localDate.with(lastDayOfYear()));

		Collection<EmployeeRecordDto> employeeRecordsColl = employeeRecordService
				.getEmployeeRecords(userEntity.getUserId(), firstOfYear, lastOfYear, 0, 300);

		for (EmployeeRecordDto employeeRecordDto : employeeRecordsColl) {
			try {
				if (!employeeRecordDto.getLeaveType().equalsIgnoreCase("AWOL")
						&& !employeeRecordDto.getLeaveType().contains("Unpaid")
						&& !employeeRecordDto.getLeaveType().contains("UNPAID")
						&& !employeeRecordDto.getLeaveType().equalsIgnoreCase("LEAVE WITHOUT PAY")) {
					returnValue += 1;
				}
			} catch (NullPointerException ex) {
				returnValue += 1;
			}

		}

		return returnValue;
	}

	private int getAbsences(UserEntity userEntity, LocalDate localDate) {
		int returnValue = 0;

		Date firstOfYear = Date.valueOf(localDate.with(firstDayOfYear()));
		Date lastOfYear = Date.valueOf(localDate.with(lastDayOfYear()));

		Collection<EmployeeRecordDto> employeeRecordsColl = employeeRecordService
				.getEmployeeRecords(userEntity.getUserId(), firstOfYear, lastOfYear, 0, 300);

		for (EmployeeRecordDto employeeRecordDto : employeeRecordsColl) {
			if (employeeRecordDto.getLeaveType() != null) {
				if (employeeRecordDto.getLeaveType().equalsIgnoreCase("AWOL")
						|| employeeRecordDto.getLeaveType().contains("Unpaid")
						|| employeeRecordDto.getLeaveType().contains("UNPAID")
						|| employeeRecordDto.getLeaveType().equalsIgnoreCase("LEAVE WITHOUT PAY")) {
					returnValue += 1;
				}
			}
		}

		return returnValue;
	}

	private int getLates(UserEntity userEntity, LocalDate localDate) {
		int returnValue = 0;

		Date firstOfYear = Date.valueOf(localDate.with(firstDayOfYear()));
		Date lastOfYear = Date.valueOf(localDate.with(lastDayOfYear()));

		Collection<EmployeeRecordDto> employeeRecordsColl = employeeRecordService
				.getEmployeeRecords(userEntity.getUserId(), firstOfYear, lastOfYear, 0, 300);

		for (EmployeeRecordDto employeeRecordDto : employeeRecordsColl) {
			if (employeeRecordDto.isLate()) {
				returnValue += 1;
			}
		}

		return returnValue;
	}

	private int getSickLeaves(UserEntity userEntity, LocalDate localDate) {
		int returnValue = 0;

		Date firstOfYear = Date.valueOf(localDate.with(firstDayOfYear()));
		Date lastOfYear = Date.valueOf(localDate.with(lastDayOfYear()));

		Collection<EmployeeRecordDto> employeeRecordsColl = employeeRecordService
				.getEmployeeRecords(userEntity.getUserId(), firstOfYear, lastOfYear, 0, 300);

		for (EmployeeRecordDto employeeRecordDto : employeeRecordsColl) {
			if (employeeRecordDto.getLeaveType() != null) {
				if (employeeRecordDto.getLeaveType().contains("Sick")
						&& (!employeeRecordDto.getLeaveType().contains("Unpaid")
								|| !employeeRecordDto.getLeaveType().contains("UNPAID"))) {
					returnValue += 1;
				}
			}
		}

		return returnValue;
	}

	private int getLeavesUsed(UserEntity userEntity, LocalDate localDate) {
		int returnValue = 0;

		Date firstOfYear = Date.valueOf(localDate.with(firstDayOfYear()));
		Date lastOfYear = Date.valueOf(localDate.with(lastDayOfYear()));

		Collection<EmployeeRecordDto> employeeRecordsColl = employeeRecordService
				.getEmployeeRecords(userEntity.getUserId(), firstOfYear, lastOfYear, 0, 300);

		for (EmployeeRecordDto employeeRecordDto : employeeRecordsColl) {
			if (employeeRecordDto.getLeaveType() != null) {
				if (employeeRecordDto.getLeaveType().contains("Leave")
						&& !employeeRecordDto.getLeaveType().contains("Sick")
						&& (!employeeRecordDto.getLeaveType().contains("Unpaid")
								&& !employeeRecordDto.getLeaveType().contains("UNPAID"))
						&& !employeeRecordDto.getLeaveType().isEmpty()
						&& !employeeRecordDto.getLeaveType().equalsIgnoreCase("LEAVE WITHOUT PAY")) {
					returnValue += 1;
				}
			}
		}

		return returnValue;
	}

}
