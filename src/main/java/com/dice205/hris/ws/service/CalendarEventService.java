package com.dice205.hris.ws.service;

import java.util.List;

import com.dice205.hris.ws.shared.dto.CalendarEventDto;

public interface CalendarEventService {
	CalendarEventDto createCalendarEvent(CalendarEventDto calendarEventDetails);
	CalendarEventDto updateCalendarEvent(long id, CalendarEventDto calendarEventDto);
	CalendarEventDto createMeetingSchedule(CalendarEventDto calendarEventDetails);
	boolean deleteCalendarEvent(long id);
	List<CalendarEventDto> getByEventDate(int year, int month, int day);
}
