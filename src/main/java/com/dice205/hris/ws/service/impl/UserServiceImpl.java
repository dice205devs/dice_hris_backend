package com.dice205.hris.ws.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.exceptions.UserServiceException;
import com.dice205.hris.ws.io.entity.JwtBlacklistEntity;
import com.dice205.hris.ws.io.entity.PasswordResetTokenEntity;
import com.dice205.hris.ws.io.entity.RoleEntity;
import com.dice205.hris.ws.io.repositories.JwtBlacklistRepository;
import com.dice205.hris.ws.io.repositories.PasswordResetTokenRepository;
import com.dice205.hris.ws.io.repositories.ProfileRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.security.PasswordConstraintValidator;
import com.dice205.hris.ws.security.SecurityConstants;
import com.dice205.hris.ws.security.UserPrincipal;
import com.dice205.hris.ws.io.repositories.RoleRepository;
import com.dice205.hris.ws.service.UserService;
import com.dice205.hris.ws.shared.AmazonSES;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.EmploymentStatus;
import com.dice205.hris.ws.shared.dto.UserDto;
import com.dice205.hris.ws.shared.dto.WorkingStatus;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	ProfileRepository profileRepository;

	@Autowired
	JwtBlacklistRepository jwtBlacklistRepository;

	@Autowired
	Utils utils;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	PasswordResetTokenRepository passwordResetTokenRepository;

	@Override
	public UserDto createUser(UserDto user) {

		if (userRepository.findByEmail(user.getEmail()) != null)
			throw new RuntimeException("Record already exist");

		UserEntity userEntity = new UserEntity();
		BeanUtils.copyProperties(user, userEntity);

		String publicUserId = utils.generateUserId(30);

		Collection<RoleEntity> roleEntities = new ArrayList<>();

		RoleEntity roleEntity = roleRepository.findByName("ROLE_USER");

		if (roleEntity == null)
			throw new RuntimeException("Role not found");

		roleEntities.add(roleEntity);

		userEntity.setWorkingStatus(user.getWorkingStatus().name());
		userEntity.setEmploymentStatus(user.getEmploymentStatus().name());
		userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userEntity.setUserId(publicUserId);
		userEntity.setCreatedBy(utils.getAuth().getUserId());
		userEntity.setCreatedAt(utils.generateTimestamp());
		userEntity.setRoles(roleEntities);
		userEntity.setEmailVerificationToken(utils.generateEmailVerificationToken(publicUserId));
		userEntity.setEmailVerificationStatus(false);

		UserEntity storedUserDetails = userRepository.save(userEntity);

		String resetToken = this.onCreateUserResetPassword(storedUserDetails);

		UserDto returnValue = new UserDto();
		BeanUtils.copyProperties(storedUserDetails, returnValue);
		
		returnValue.setRoles(this.getRoleNames(userEntity));
		returnValue.setWorkingStatus(WorkingStatus.valueOf(storedUserDetails.getWorkingStatus()));
		returnValue.setEmploymentStatus(EmploymentStatus.valueOf(storedUserDetails.getEmploymentStatus()));

		new AmazonSES().verifyEmail(returnValue, resetToken);

		return returnValue;
	}

	@Override
	public UserDto getUser(String email) {
		UserDto returnValue = new UserDto();

		UserEntity userEntity = userRepository.findByEmail(email);

		if (userEntity == null)
			throw new UsernameNotFoundException(email);

		BeanUtils.copyProperties(userEntity, returnValue);

		returnValue.setRoles(this.getRoleNames(userEntity));
		returnValue.setWorkingStatus(WorkingStatus.valueOf(userEntity.getWorkingStatus()));
		returnValue.setEmploymentStatus(EmploymentStatus.valueOf(userEntity.getEmploymentStatus()));
		
		return returnValue;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByEmailAndDeletedAtIsNull(username);

		if (userEntity == null)
			throw new UsernameNotFoundException(username);

		return new UserPrincipal(userEntity);
	}

	@Override
	public UserDto getUserByUserId(String id) {
		UserDto returnValue = new UserDto();
		UserEntity userEntity = userRepository.findByUserId(id);

		if (userEntity == null)
			throw new UsernameNotFoundException(id);

		BeanUtils.copyProperties(userEntity, returnValue);

		returnValue.setRoles(this.getRoleNames(userEntity));
		returnValue.setWorkingStatus(WorkingStatus.valueOf(userEntity.getWorkingStatus()));
		returnValue.setEmploymentStatus(EmploymentStatus.valueOf(userEntity.getEmploymentStatus()));
		
		return returnValue;
	}

	@Override
	public UserDto updateUser(String id, UserDto userDetails) {
		UserDto returnValue = new UserDto();
		UserEntity userEntity = userRepository.findByUserId(id);

		if (userEntity == null)
			throw new UsernameNotFoundException(id);

		if (!userEntity.getEmail().equalsIgnoreCase(userDetails.getEmail())) {
			UserEntity findEmailIfExist = userRepository.findByEmail(userDetails.getEmail());

			if (findEmailIfExist == null) {
				userEntity.setEmail(userDetails.getEmail());
			} else {
				throw new UserServiceException(ErrorMessages.EMAIL_ALREADY_USED.getErrorMessage());
			}
		}

		Collection<RoleEntity> roleEntities = new ArrayList<>();

		for (String role : userDetails.getRoles()) {
			RoleEntity roleEntity = roleRepository.findByName(role);

			if (roleEntity == null)
				throw new RuntimeException("Role not found");

			roleEntities.add(roleEntity);
		}

		userEntity.setWorkingStatus(userDetails.getWorkingStatus().name());
		userEntity.setEmploymentStatus(userDetails.getEmploymentStatus().name());
		userEntity.setRoles(roleEntities);
		userEntity.setShift(userDetails.getShift());
		userEntity.setSupervisorId(userDetails.getSupervisorId());
		userEntity.setUpdatedBy(utils.getAuth().getUserId());
		userEntity.setUpdatedAt(utils.generateTimestamp());

		UserEntity updatedUserDetails = userRepository.save(userEntity);

		BeanUtils.copyProperties(updatedUserDetails, returnValue);
		
		returnValue.setWorkingStatus(WorkingStatus.valueOf(updatedUserDetails.getWorkingStatus()));
		returnValue.setEmploymentStatus(EmploymentStatus.valueOf(updatedUserDetails.getEmploymentStatus()));
		returnValue.setRoles(this.getRoleNames(userEntity));

		return returnValue;
	}

	@Override
	public Boolean softDeleteUser(String userId) {
		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);
		Boolean bool = false;

		if (userEntity != null) {

			userEntity.setDeletedAt(utils.generateTimestamp());
			userEntity.setEmploymentStatus("RESIGNED");
			userRepository.save(userEntity);

			bool = true;
		}

		return bool;
	}

	@Override
	public List<UserDto> getAllUsers(int page, int limit) {
		List<UserDto> returnValue = new ArrayList<>();
		Pageable pageableRequest = PageRequest.of(page, limit);

//		List<UserEntity> userEntities = userRepository.findByDeletedAtIsNull();
		Page<UserEntity> usersPage = userRepository.findAllByDeletedAtIsNull(pageableRequest);
		List<UserEntity> userEntities = usersPage.getContent();

		for (int i = 0; i < userEntities.size(); i++) {
			UserDto userDto = new UserDto();
			
			Collection<String> roles = new ArrayList<>();

			for (RoleEntity roleEntity : userEntities.get(i).getRoles()) {
				roles.add(roleEntity.getName());
			}

			BeanUtils.copyProperties(userEntities.get(i), userDto);
			
			userDto.setWorkingStatus(WorkingStatus.valueOf(userEntities.get(i).getWorkingStatus()));
			userDto.setEmploymentStatus(EmploymentStatus.valueOf(userEntities.get(i).getEmploymentStatus()));
			userDto.setRoles(roles);
			
			returnValue.add(userDto);
		}

		return returnValue;
	}

	@Override
	public List<UserDto> getAllUsersByRole(String roleName, int page, int limit) {
		List<UserDto> returnValue = new ArrayList<>();
		Pageable pageableRequest = PageRequest.of(page, limit);

		Page<UserEntity> userEntityIter = userRepository.findAllByDeletedAtIsNull(pageableRequest);
		
		for (UserEntity userEnity: userEntityIter) {
			UserDto userDto = new UserDto();
			
			Collection<String> roles = this.getRoleNames(userEnity);
			
			if (roles.contains(roleName)) {
				BeanUtils.copyProperties(userEnity, userDto);
				
				userDto.setRoles(roles);
				userDto.setWorkingStatus(WorkingStatus.valueOf(userEnity.getWorkingStatus()));
				userDto.setEmploymentStatus(EmploymentStatus.valueOf(userEnity.getEmploymentStatus()));
				
				returnValue.add(userDto);
			}
		}

		return returnValue;
	}

	@Override
	public String verifyEmailToken(String token) {
		String returnValue = null;

		UserEntity userEntity = userRepository.findUserByEmailVerificationToken(token);

		if (userEntity != null) {
			boolean hasTokenExpired = Utils.hasTokenExpired(token);
			if (!hasTokenExpired) {
				userEntity.setEmailVerificationToken(null);
				userEntity.setEmailVerificationStatus(Boolean.TRUE);
				userRepository.save(userEntity);
				returnValue = userEntity.getUserId();
			}
		}

		return returnValue;
	}

	private String onCreateUserResetPassword(UserEntity userEntity) {
		String token = utils.generatePasswordResetToken(userEntity.getUserId());

		PasswordResetTokenEntity passwordResetTokenEntity = new PasswordResetTokenEntity();
		passwordResetTokenEntity.setToken(token);
		passwordResetTokenEntity.setUserDetails(userEntity);
		PasswordResetTokenEntity storedToken = passwordResetTokenRepository.save(passwordResetTokenEntity);

		return storedToken.getToken();
	}

	@Override
	public boolean requestPasswordReset(String email) {

		boolean returnValue = false;

		UserEntity userEntity = userRepository.findByEmail(email);

		if (userEntity == null) {
			return returnValue;
		}

		String token = utils.generatePasswordResetToken(userEntity.getUserId());

		PasswordResetTokenEntity passwordResetTokenEntity = new PasswordResetTokenEntity();
		passwordResetTokenEntity.setToken(token);
		passwordResetTokenEntity.setUserDetails(userEntity);
		passwordResetTokenRepository.save(passwordResetTokenEntity);

		returnValue = new AmazonSES().sendPasswordResetRequest(userEntity.getEmail(), token);

		return returnValue;
	}

	@Override
	public boolean resetPassword(String token, String password) {
		boolean returnValue = false;

		PasswordConstraintValidator validator = new PasswordConstraintValidator();
		List<String> validation = validator.validate(password);

		if (validation.size() > 0)
			throw new RuntimeException(validation.get(validation.size() - 1));

		if (Utils.hasTokenExpired(token)) {
			return returnValue;
		}

		PasswordResetTokenEntity passwordResetTokenEntity = passwordResetTokenRepository.findByToken(token);

		if (passwordResetTokenEntity == null) {
			return returnValue;
		}

		// Prepare new password
		String encodedPassword = bCryptPasswordEncoder.encode(password);

		// Update User password in database
		UserEntity userEntity = passwordResetTokenEntity.getUserDetails();
		userEntity.setEncryptedPassword(encodedPassword);
		UserEntity savedUserEntity = userRepository.save(userEntity);

		// Verify if password was saved successfully
		if (savedUserEntity != null && savedUserEntity.getEncryptedPassword().equalsIgnoreCase(encodedPassword)) {
			returnValue = true;
		}

		// Remove Password Reset token from database
		passwordResetTokenRepository.delete(passwordResetTokenEntity);

		return returnValue;
	}

	@Override
	public List<UserDto> getArchive(int page, int limit) {
		List<UserDto> returnValue = new ArrayList<>();
		Pageable pageableRequest = PageRequest.of(page, limit);

		Page<UserEntity> usersPage = userRepository.findAllByDeletedAtIsNotNull(pageableRequest);
		List<UserEntity> userEntities = usersPage.getContent();

		for (int i = 0; i < userEntities.size(); i++) {
			UserDto userDto = new UserDto();

			BeanUtils.copyProperties(userEntities.get(i), userDto);

			userDto.setRoles(this.getRoleNames(userEntities.get(i)));
			userDto.setEmploymentStatus(EmploymentStatus.valueOf(userEntities.get(i).getEmploymentStatus()));
			userDto.setWorkingStatus(WorkingStatus.valueOf(userEntities.get(i).getWorkingStatus()));
			
			returnValue.add(userDto);
		}

		return returnValue;
	}

	@Override
	public boolean logout(String jwt) {
		boolean returnValue = false;

		jwt = jwt.replace(SecurityConstants.TOKEN_PREFIX, "");

		Optional<JwtBlacklistEntity> jwtBlacklistEntity = jwtBlacklistRepository.findByJwt(jwt);

		if (!jwtBlacklistEntity.isPresent()) {
			JwtBlacklistEntity newJwtBlacklistEntity = new JwtBlacklistEntity();

			newJwtBlacklistEntity.setJwt(jwt);
			newJwtBlacklistEntity.setDateBlacklisted(utils.generateTimestamp());

			JwtBlacklistEntity storedJwtBlacklistEntity = jwtBlacklistRepository.save(newJwtBlacklistEntity);

			if (storedJwtBlacklistEntity != null) {
				returnValue = true;
			}
		}

		return returnValue;
	}
	
	private Collection<String> getRoleNames(UserEntity userEntity) {
		Collection<String> roles = new ArrayList<>();

		for (RoleEntity roleEntity : userEntity.getRoles()) {
			roles.add(roleEntity.getName());
		}
		
		return roles;
	}
	
	@Override
	public boolean changePassword(String password, String newPassword) {
		boolean returnValue = false;

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		String email = authentication.getName();

		UserEntity userEntity = userRepository.findByEmail(email);

		if (userEntity == null) {
			return returnValue;
		}

		String encodedNewPassword = bCryptPasswordEncoder.encode(newPassword);

		boolean isMatch = bCryptPasswordEncoder.matches(password, userEntity.getEncryptedPassword());

		if (!isMatch)
			throw new RuntimeException("Wrong password");

		userEntity.setEncryptedPassword(encodedNewPassword);

		userRepository.save(userEntity);

		return true;
	}

	@Override
	public List<UserDto> getAllUserByUserId(String[] userIds) {
		List<UserDto> returnValue = new ArrayList<>();
				
		for (UserEntity userEntity : userRepository.findAllByUserIdIn(userIds)) {
			UserDto userDto = new UserDto();
			
			BeanUtils.copyProperties(userEntity, userDto);
			
			userDto.setWorkingStatus(WorkingStatus.valueOf(userEntity.getWorkingStatus()));
			userDto.setEmploymentStatus(EmploymentStatus.valueOf(userEntity.getEmploymentStatus()));
			userDto.setRoles(this.getRoleNames(userEntity));
			
			returnValue.add(userDto);
		}
		
		return returnValue;
	}
}
