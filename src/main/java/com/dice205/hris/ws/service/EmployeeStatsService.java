package com.dice205.hris.ws.service;

import java.sql.Date;

import com.dice205.hris.ws.shared.dto.EmployeeStatsDto;

public interface EmployeeStatsService {
	EmployeeStatsDto getEmployeeStats(String userId, Date date);
}
