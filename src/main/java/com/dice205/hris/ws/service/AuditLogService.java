package com.dice205.hris.ws.service;

import com.dice205.hris.ws.shared.dto.AuditLogDto;

public interface AuditLogService {
	boolean createAuditLog(AuditLogDto auditDetails);
}
