package com.dice205.hris.ws.service.impl;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dice205.hris.ws.io.entity.SubscriptionKeysEntity;
import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.entity.SubscriptionEntity;
import com.dice205.hris.ws.io.repositories.SubscriptionKeysRepository;
import com.dice205.hris.ws.io.repositories.SubscriptionRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.security.SecurityConstants;
import com.dice205.hris.ws.service.PushService;
import com.dice205.hris.ws.shared.dto.KeysDto;
import com.dice205.hris.ws.shared.dto.NotificationPayloadDto;
import com.dice205.hris.ws.shared.dto.SubscriptionDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.Utils;

@Service
public class PushServiceImpl implements PushService {

	@Autowired
	SubscriptionRepository subscriptionRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	SubscriptionKeysRepository keysRepository;

	@Autowired
	com.dice205.hris.ws.shared.Utils utils;

	@Override
	public SubscriptionDto subscribe(SubscriptionDto subscriptionDetails) {
		SubscriptionDto returnValue = new SubscriptionDto();

		SubscriptionEntity subscriptionEntity = new SubscriptionEntity();

		BeanUtils.copyProperties(subscriptionDetails, subscriptionEntity);

		subscriptionEntity.setSubscriptionId(utils.generateSubscriptionId(30));
		subscriptionEntity.setKeysId(this.createSubscriptionKey(subscriptionDetails));
		subscriptionEntity.setUser(userRepository.findByUserId(subscriptionDetails.getUserId()));

		SubscriptionEntity createdSubscriptionEntity = subscriptionRepository.save(subscriptionEntity);

		if (createdSubscriptionEntity == null)
			throw new RuntimeException("Could not be subscribed");

		BeanUtils.copyProperties(createdSubscriptionEntity, returnValue);

		returnValue.setKeys(new KeysDto(createdSubscriptionEntity.getKeysId()));
		returnValue.setUserId(createdSubscriptionEntity.getUser().getUserId());

		return returnValue;
	}

	private SubscriptionKeysEntity createSubscriptionKey(SubscriptionDto subscriptionDetails) {

		SubscriptionKeysEntity keysEntity = new SubscriptionKeysEntity();

		BeanUtils.copyProperties(subscriptionDetails.getKeys(), keysEntity);

		SubscriptionKeysEntity createdKeyEntity = keysRepository.save(keysEntity);

		if (createdKeyEntity == null)
			throw new RuntimeException("Could not be subscribed");

		return createdKeyEntity;
	}

	@Override
	public List<SubscriptionDto> getAllSubscriptions() {
		List<SubscriptionDto> returnValue = new ArrayList<>();

		for (SubscriptionEntity subscriptionEntity : subscriptionRepository.findAll()) {
			SubscriptionDto subscriptionDto = new SubscriptionDto();
			KeysDto keysDto = new KeysDto();

			BeanUtils.copyProperties(subscriptionEntity, subscriptionDto);
			BeanUtils.copyProperties(subscriptionEntity.getKeysId(), keysDto);

			subscriptionDto.setKeys(keysDto);
			subscriptionDto.setUserId(subscriptionEntity.getUser().getUserId());

			returnValue.add(subscriptionDto);
		}

		return returnValue;
	}

	@Override
	public SubscriptionDto getBySubscriptionId(String subscriptionId) {
		SubscriptionDto returnValue = new SubscriptionDto();

		SubscriptionEntity subscriptionEntity = subscriptionRepository.findBySubscriptionId(subscriptionId);

		if (subscriptionEntity == null)
			throw new RuntimeException("Could not find subscription");

		BeanUtils.copyProperties(subscriptionEntity, returnValue);

		KeysDto keysDto = new KeysDto(subscriptionEntity.getKeysId());

		returnValue.setKeys(keysDto);
		returnValue.setUserId(subscriptionEntity.getUser().getUserId());

		return returnValue;
	}

	private boolean sendPush(String subscriptionId, NotificationPayloadDto notificationPayload)
			throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException, JsonProcessingException {

		boolean returnValue = false;

		SubscriptionDto subscriptionDto = this.getBySubscriptionId(subscriptionId);

		KeyPair keyPair = new KeyPair(Utils.loadPublicKey(SecurityConstants.PUSH_PUBLIC_KEY),
				Utils.loadPrivateKey(SecurityConstants.PUSH_PRIVATE_KEY));
		nl.martijndwars.webpush.PushService pushService = new nl.martijndwars.webpush.PushService(keyPair,
				"mailto:francistesting123456@gmail.com");

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

		String payload = ow.writeValueAsString(notificationPayload);

		Notification notification = new Notification(subscriptionDto.getEndpoint(),
				subscriptionDto.getKeys().getP256dh(), subscriptionDto.getKeys().getAuth(), payload);

		try {
			pushService.send(notification);
			returnValue = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return returnValue;
	}

	@Override
	public boolean unsubscribe(String endpoint, boolean isUnsubscribed) {
		boolean returnValue = false;

		if (!isUnsubscribed) {
			return returnValue;
		}

		SubscriptionEntity subscriptionEntity = subscriptionRepository.findByEndpoint(endpoint);

		if (subscriptionEntity == null) {
			return returnValue;
		}

		subscriptionRepository.delete(subscriptionEntity);

		returnValue = true;

		return returnValue;
	}

	@Override
	public boolean sendPushMessages(String[] userIds, NotificationPayloadDto notificationPayload) {
		boolean returnValue = false;
		List<SubscriptionDto> subscriptionDtoList = this.getByUsers(userIds);
		
		for (SubscriptionDto subscriptionDto : subscriptionDtoList) {
			try {
				this.sendPush(subscriptionDto.getSubscriptionId(), notificationPayload);
				returnValue = true;
			} catch (NoSuchProviderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return returnValue;
	}

	@Override
	public List<SubscriptionDto> getByUsers(String[] userIds) {
		List<SubscriptionDto> returnValue = new ArrayList<>();
		
		List<UserEntity> userEntityList = userRepository.findAllByUserIdIn(userIds);
		
		List<SubscriptionEntity> subscriptionEntityList = subscriptionRepository
				.findAllByUserIn(userEntityList);

		for (SubscriptionEntity subscriptionEntity : subscriptionEntityList) {
			SubscriptionDto subscriptionDto = new SubscriptionDto();
			KeysDto keysDto = new KeysDto(subscriptionEntity.getKeysId());

			BeanUtils.copyProperties(subscriptionEntity, subscriptionDto);

			subscriptionDto.setKeys(keysDto);
			subscriptionDto.setUserId(subscriptionEntity.getUser().getUserId());

			returnValue.add(subscriptionDto);
		}

		return returnValue;
	}
	
	@Override
	public NotificationPayloadDto newNotificationPayload(String title, String content) {
		return new NotificationPayloadDto(title, content);
	}
}
