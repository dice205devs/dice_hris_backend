package com.dice205.hris.ws.service;

import com.dice205.hris.ws.shared.dto.ProfileDto;

public interface ProfileService {
	ProfileDto createProfile(ProfileDto profile);
	ProfileDto getProfile(String associateId);
	ProfileDto updateProfile(ProfileDto profile, String userId);
	Boolean softDeleteProfile(String userId);
}