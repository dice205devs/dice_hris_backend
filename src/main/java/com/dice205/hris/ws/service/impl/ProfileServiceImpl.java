package com.dice205.hris.ws.service.impl;

import com.dice205.hris.ws.io.entity.ProfileEntity;
import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.repositories.ProfileRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.service.ProfileService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.ProfileDto;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileServiceImpl implements ProfileService {

    @Autowired
    ProfileRepository profileRepository;

    @Autowired 
    UserRepository userRepository;

    @Autowired
    Utils utils;

    @Override
    public ProfileDto createProfile(ProfileDto profile) {
    	ProfileEntity profileEntity = new ProfileEntity();
        BeanUtils.copyProperties(profile, profileEntity);

        profileEntity.setCreatedAt(utils.generateTimestamp());
        profileEntity.setCreatedBy(utils.getAuth().getUserId());
        profileEntity.setEmployeeId(profile.getEmployeeId());

        ProfileEntity updatedProfileEntity = profileRepository.save(profileEntity);

        UserEntity userEntity = userRepository.findByUserId(profile.getEmployeeId());
        userEntity.setProfile(updatedProfileEntity);
        userRepository.save(userEntity);

        ProfileDto returnValue = new ProfileDto();
        BeanUtils.copyProperties(updatedProfileEntity, returnValue);
        
        return returnValue;
    }

    @Override
    public ProfileDto getProfile(String associateId) {
        ProfileEntity profileEntity = profileRepository.findByEmployeeId(associateId);
        
        if (profileEntity == null) throw new RuntimeException("Profile does not exist or not yet verified");
        
        ProfileDto returnValue = new ProfileDto();
        BeanUtils.copyProperties(profileEntity, returnValue);
        
        return returnValue;
    }

    @Override
    public ProfileDto updateProfile(ProfileDto profile, String associateId) {
        ProfileEntity profileEntity = profileRepository.findByEmployeeId(associateId);
        ProfileDto returnValue = new ProfileDto();

        if (profileEntity == null) throw new RuntimeException("Profile does not exist");

        profileEntity.setUpdatedAt(utils.generateTimestamp());
        profileEntity.setUpdatedBy(utils.getAuth().getUserId());
        profileEntity.setPersonalInformation(profile.getPersonalInformation());
        profileEntity.setContactInformation(profile.getContactInformation());
        profileEntity.setEducationalBackground(profile.getEducationalBackground());
        profileEntity.setEmploymentBackground(profile.getEmploymentBackground());
        profileEntity.setWorkDetails(profile.getWorkDetails());
        profileEntity.setGovernmentInformation(profile.getGovernmentInformation());

        ProfileEntity updatedProfileEntity = profileRepository.save(profileEntity);

        BeanUtils.copyProperties(updatedProfileEntity, returnValue);
        
        return returnValue;
    }

    @Override
    public Boolean softDeleteProfile(String userId) {
        ProfileEntity profileEntity = profileRepository.findByEmployeeId(userId);
        Boolean bool = false;

        if (profileEntity != null) {

			profileEntity.setDeletedAt(utils.generateTimestamp());
			profileRepository.save(profileEntity);

			bool = true;
		}

		return bool;
    }

}