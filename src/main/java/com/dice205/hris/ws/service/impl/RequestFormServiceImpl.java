package com.dice205.hris.ws.service.impl;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dice205.hris.ws.io.entity.RequestFormEntity;
import com.dice205.hris.ws.io.entity.RequestTypeEntity;
import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.repositories.RequestFormRepository;
import com.dice205.hris.ws.io.repositories.RequestTypeRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.service.EmployeeRecordService;
import com.dice205.hris.ws.service.PayrollRecordService;
import com.dice205.hris.ws.service.PushService;
import com.dice205.hris.ws.service.RequestFormService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.EmployeeRecordDto;
import com.dice205.hris.ws.shared.dto.NotificationPayloadDto;
import com.dice205.hris.ws.shared.dto.PayrollRecordDto;
import com.dice205.hris.ws.shared.dto.RequestFormDto;
import com.dice205.hris.ws.shared.dto.RequestTypeDto;
import com.dice205.hris.ws.shared.dto.RequestTypes;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;

@Service
public class RequestFormServiceImpl implements RequestFormService {

	@Autowired
	RequestFormRepository requestFormRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RequestTypeRepository requestTypeRepository;

	@Autowired
	EmployeeRecordService employeeRecordService;

	@Autowired
	PayrollRecordService payrollRecordService;

	@Autowired
	Utils utils;

	@Autowired
	PushService pushService;

	@Override
	public List<RequestFormDto> getRequestForms(String userId, int page, int limit) {
		List<RequestFormDto> returnValue = new ArrayList<>();
		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);
		Pageable pageableRequest = PageRequest.of(page, limit);

		if (userEntity == null)
			throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());

		Page<RequestFormEntity> requestFormEntityList = requestFormRepository.findAllByApplicant(userEntity,
				pageableRequest);

		for (RequestFormEntity requestFormEntity : requestFormEntityList) {
			RequestFormDto requestFormDto = new RequestFormDto();

			BeanUtils.copyProperties(requestFormEntity, requestFormDto);

			requestFormDto.setRequestType(RequestTypes.valueOf(requestFormEntity.getRequestType().getName()));

			returnValue.add(requestFormDto);
		}

		return returnValue;
	}

	@Override
	public RequestFormDto createRequestForm(String userId, RequestFormDto requestFormDto) {
		RequestFormDto returnValue = new RequestFormDto();
		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);

		if (userEntity == null)
			throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());
		RequestFormEntity requestFormEntity = new RequestFormEntity();

		double overtimeDuration = 0.0;

		if (requestFormDto.getOvertimeDate() != null) {
			overtimeDuration = utils.getTimeDiff(requestFormDto.getOvertimeEnd(), requestFormDto.getOvertimeStart())
					.doubleValue();
		}

		BeanUtils.copyProperties(requestFormDto, requestFormEntity);

		RequestTypeEntity requestTypeEntity = requestTypeRepository.findByName(requestFormDto.getRequestType().name());

		if (requestTypeEntity == null)
			throw new RuntimeException("Request type does not exist");

		requestFormEntity.setRequestType(requestTypeEntity);
		requestFormEntity.setApplicant(userEntity);
		requestFormEntity.setCreatedAt(utils.generateTimestamp());
		requestFormEntity.setCreatedBy(utils.getAuth().getUserId());
		requestFormEntity.setOvertimeDuration(overtimeDuration);

		if (!this.validateRequest(requestFormEntity))
			throw new RuntimeException("Please file " + this.getExceptionMessage(requestTypeEntity));

		RequestFormEntity storedRequestFormEntity = requestFormRepository.save(requestFormEntity);

		if (storedRequestFormEntity == null)
			throw new RuntimeException("Request Form, " + ErrorMessages.COULD_NOT_CREATE_RECORD.getErrorMessage());

		BeanUtils.copyProperties(storedRequestFormEntity, returnValue);

		returnValue.setRequestType(RequestTypes.valueOf(storedRequestFormEntity.getRequestType().getName()));

		boolean push = this.sendPush(new String[] { returnValue.getApplicant().getSupervisorId() }, "New request",
				this.getFullname(returnValue.getApplicant()) + " has filed for " + changeCase(returnValue.getRequestType().name()));

		System.out.println("REQUEST FROM PUSH: " + push);

		return returnValue;
	}

	@Override
	public RequestFormDto updateRequestForm(long id, RequestFormDto requestFormDto) {
		RequestFormDto returnValue = new RequestFormDto();

		Optional<RequestFormEntity> requestFormEntityOpt = requestFormRepository.findById(id);

		if (requestFormEntityOpt.get() == null)
			throw new RuntimeException("Request Form, " + ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		RequestFormEntity requestFormEntity = requestFormEntityOpt.get();

		RequestTypeEntity requestTypeEntity = requestTypeRepository.findByName(requestFormDto.getRequestType().name());

		requestFormEntity.setRequestType(requestTypeEntity);
		requestFormEntity.setRequestStatus(requestFormDto.getRequestStatus());
		requestFormEntity.setUpdatedBy(utils.getAuth().getUserId());
		requestFormEntity.setUpdatedAt(utils.generateTimestamp());

		RequestFormEntity updatedRequestFormEntity = requestFormRepository.save(requestFormEntity);

		if (updatedRequestFormEntity == null)
			throw new RuntimeException("Request Form, " + ErrorMessages.COULD_NOT_UPDATE_RECORD.getErrorMessage());

		BeanUtils.copyProperties(updatedRequestFormEntity, returnValue);

		returnValue.setRequestType(RequestTypes.valueOf(updatedRequestFormEntity.getRequestType().getName()));

		if (updatedRequestFormEntity.getRequestType().getName().contains("Overtime")) {
			EmployeeRecordDto employeeRecordDtoOvertime = this
					.updateEmployeeRecordOvertime(updatedRequestFormEntity.getApplicant().getUserId(), returnValue);

			if (employeeRecordDtoOvertime != null) {
				this.updatePayrollRecordOvertime(employeeRecordDtoOvertime);
			}
		} else {
			EmployeeRecordDto employeeRecordDtoLeave = this
					.createEmployeeRecordLeave(updatedRequestFormEntity.getApplicant().getUserId(), requestFormDto);
			if (employeeRecordDtoLeave != null) {
				this.updatePayrollRecordHoursRendered(employeeRecordDtoLeave);
			}
		}

		boolean push = this.sendPush(new String[] { returnValue.getApplicant().getUserId() }, "Request Status",
				"Your request for " + returnValue.getRequestType() + " has been "
						+ this.getStatus(returnValue.getRequestStatus()));

		System.out.print("REQUEST FROM PUSH: " + push);

		return returnValue;
	}

	@Override
	public List<RequestFormDto> getAllBySupervisor(String supervisorId, int page, int limit) {
		List<RequestFormDto> returnValue = new ArrayList<>();
		Pageable pageableRequest = PageRequest.of(page, limit);

		Page<RequestFormEntity> requestFormEntityList = requestFormRepository.findAll(pageableRequest);

		if (requestFormEntityList == null)
			throw new RuntimeException("Request Form, " + ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		for (RequestFormEntity requestFormEntity : requestFormEntityList) {
			RequestFormDto requestFormDto = new RequestFormDto();

			if (requestFormEntity.getApplicant().getSupervisorId() == supervisorId) {
				BeanUtils.copyProperties(requestFormEntity, requestFormDto);

				requestFormDto.setRequestType(RequestTypes.valueOf(requestFormEntity.getRequestType().getName()));

				returnValue.add(requestFormDto);
			}
		}

		return returnValue;
	}

	@Override
	public List<RequestFormDto> getAllRequestForms(int page, int limit) {
		List<RequestFormDto> returnValue = new ArrayList<>();
		Pageable pageableRequest = PageRequest.of(page, limit);

		Page<RequestFormEntity> requestFormEntityList = requestFormRepository.findAll(pageableRequest);

		if (requestFormEntityList == null)
			throw new RuntimeException("Request Form, " + ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		for (RequestFormEntity requestFormEntity : requestFormEntityList) {
			RequestFormDto requestFormDto = new RequestFormDto();

			BeanUtils.copyProperties(requestFormEntity, requestFormDto);

			requestFormDto.setRequestType(RequestTypes.valueOf(requestFormEntity.getRequestType().getName()));

			returnValue.add(requestFormDto);
		}

		return returnValue;
	}

	private boolean validateRequest(RequestFormEntity requestFormEntity) {
		boolean returnValue = false;

		RequestTypeEntity requestTypeEntity = requestFormEntity.getRequestType();

		RequestTypes requestTypeName = RequestTypes.valueOf(requestTypeEntity.getName());

		Date dateFiled = requestFormEntity.getDateFiled();
		Date leaveDate = requestFormEntity.getLeaveDate();

		if (leaveDate == null && requestTypeName == RequestTypes.OVERTIME) {
			return true;
		}

		LocalDate leaveDateLd = leaveDate.toLocalDate();

		long fileByDate = requestTypeEntity.getFileByDate();
		long leaveDateMilli = leaveDateLd.atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();

		if (requestTypeName == RequestTypes.VACATION_LEAVE || requestTypeName == RequestTypes.BIRTHDAY_LEAVE) {
			long fileBeforeMilli = leaveDateMilli - fileByDate;
			LocalDate fileBeforeLd = Instant.ofEpochMilli(fileBeforeMilli).atZone(ZoneId.systemDefault()).toLocalDate();
			Date fileBeforeDate = Date.valueOf(fileBeforeLd);

			boolean before = dateFiled.before(fileBeforeDate);
			boolean on = dateFiled.equals(fileBeforeDate);

			returnValue = on || before;
		}

		if (requestTypeName == RequestTypes.SICK_LEAVE || requestTypeName == RequestTypes.EMERGENCY_LEAVE) {
			long fileBeforeMilli = leaveDateMilli + fileByDate;
			LocalDate fileBeforeLd = Instant.ofEpochMilli(fileBeforeMilli).atZone(ZoneId.systemDefault()).toLocalDate();
			Date fileBeforeDate = Date.valueOf(fileBeforeLd);

			boolean before = dateFiled.before(fileBeforeDate);
			boolean on = dateFiled.equals(fileBeforeDate);

			returnValue = on || before;

			if (!returnValue) {
				this.sendPush(new String[] { requestFormEntity.getApplicant().getUserId() }, "Leave",
						"Sick Leave and Emergency leave lapsed 48 hrs will be Leave Without Pay");
			}
		}

		if (requestTypeName == RequestTypes.BEREAVEMENT_LEAVE) {
			boolean before = dateFiled.before(leaveDate);
			boolean on = dateFiled.equals(leaveDate);

			returnValue = on || before;
		}

		return returnValue;
	}

	private String getExceptionMessage(RequestTypeEntity requestTypeEntity) {
		String message = " days before date of leave";
		String sickAndEmergency = "";

		if (RequestTypes.valueOf(requestTypeEntity.getName()) == RequestTypes.SICK_LEAVE
				|| RequestTypes.valueOf(requestTypeEntity.getName()) == RequestTypes.EMERGENCY_LEAVE) {
			sickAndEmergency = "within ";
			message = " days after date of leave";
		}

		return sickAndEmergency + String.valueOf((int) (requestTypeEntity.getFileByDate() / (1000 * 60 * 60 * 24)))
				+ message;
	}

	@Override
	public List<RequestTypeDto> getRequestTypes() {
		List<RequestTypeDto> returnValue = new ArrayList<>();

		Iterable<RequestTypeEntity> requestTypeEntityIter = requestTypeRepository.findAll();

		for (RequestTypeEntity requestTypeEntity : requestTypeEntityIter) {
			RequestTypeDto requestTypeDto = new RequestTypeDto();

			BeanUtils.copyProperties(requestTypeEntity, requestTypeDto);

			returnValue.add(requestTypeDto);
		}

		return returnValue;
	}

	private EmployeeRecordDto updateEmployeeRecordOvertime(String userId, RequestFormDto requestFormDto) {
		EmployeeRecordDto returnValue = new EmployeeRecordDto();

		if (requestFormDto.getRequestStatus()) {
			EmployeeRecordDto newEmployeeRecordDto = new EmployeeRecordDto();

			for (EmployeeRecordDto employeeRecordDto : employeeRecordService.getEmployeeRecords(userId,
					requestFormDto.getOvertimeDate(), requestFormDto.getOvertimeDate(), 0, 1)) {
				BeanUtils.copyProperties(employeeRecordDto, newEmployeeRecordDto);
			}

			newEmployeeRecordDto.setOvertime(requestFormDto.getOvertimeDuration());

			BeanUtils.copyProperties(
					employeeRecordService.updateEmloyeeRecord(newEmployeeRecordDto.getId(), newEmployeeRecordDto),
					returnValue);
		}

		return returnValue;
	}

	private void updatePayrollRecordOvertime(EmployeeRecordDto employeeRecordDto) {
		LocalDate localDate = employeeRecordDto.getEmployeeRecordDate().toLocalDate();

		String userId = employeeRecordDto.getUser().getUserId();
		int year = localDate.getYear();
		int month = localDate.getMonthValue();
		int cutOffPeriod = employeeRecordDto.getCutOffPeriod();

		List<PayrollRecordDto> payrollRecordDtoList = payrollRecordService.getAllPayrollRecords(userId, year, month,
				cutOffPeriod, 0, 1);

		if (payrollRecordDtoList.size() > 0) {
			PayrollRecordDto payrollRecordDto = payrollRecordDtoList.get(0);

			payrollRecordDto.setCutOffOvertimeRendered(
					employeeRecordService.getOvertimePerCutOff(userId, year, month, cutOffPeriod));

			PayrollRecordDto updatedPayrollRecordDto = payrollRecordService
					.updatePayrollRecord(payrollRecordDto.getId(), payrollRecordDto);

			if (updatedPayrollRecordDto != null) {
				System.out.println("REQUEST FORM: PAYROLL UPDATED");
			}
		}

	}

	private EmployeeRecordDto createEmployeeRecordLeave(String userId, RequestFormDto requestFormDto) {
		EmployeeRecordDto returnValue = new EmployeeRecordDto();

		if (requestFormDto.getRequestStatus()) {
			EmployeeRecordDto employeeRecordDto = new EmployeeRecordDto();

			employeeRecordDto.setLeaveType(requestFormDto.getRequestType().name());
			employeeRecordDto.setEmployeeRecordDate(requestFormDto.getLeaveDate());
			employeeRecordDto.setHoursRendered(8);

			EmployeeRecordDto createdEmployeeRecordDto = employeeRecordService.createEmployeeRecord(userId,
					employeeRecordDto);

			BeanUtils.copyProperties(createdEmployeeRecordDto, returnValue);

			if (createdEmployeeRecordDto != null) {
				System.out.println("REQUEST FORM: EMPLOYEE RECORD UPDATED");
			}
		}

		return returnValue;
	}
	
	private String changeCase(String leave) {
		return leave.replace("_", " ");
	}

	private void updatePayrollRecordHoursRendered(EmployeeRecordDto employeeRecordDto) {
		LocalDate localDate = employeeRecordDto.getEmployeeRecordDate().toLocalDate();

		String userId = employeeRecordDto.getUser().getUserId();
		int year = localDate.getYear();
		int month = localDate.getMonthValue();
		int cutOffPeriod = employeeRecordDto.getCutOffPeriod();

		List<PayrollRecordDto> payrollRecordDtoList = payrollRecordService.getAllPayrollRecords(userId, year, month,
				cutOffPeriod, 0, 1);

		if (!payrollRecordDtoList.isEmpty()) {
			PayrollRecordDto payrollRecordDto = payrollRecordDtoList.get(0);

			payrollRecordDto
					.setCutOffHoursRendered(employeeRecordService.getHoursPerCutOff(userId, year, month, cutOffPeriod));

			System.out.println("CUTOFF REQUEST HOURS => "
					+ employeeRecordService.getHoursPerCutOff(userId, year, month, cutOffPeriod));

			PayrollRecordDto updatedPayrollRecordDto = payrollRecordService
					.updatePayrollRecord(payrollRecordDto.getId(), payrollRecordDto);

			if (updatedPayrollRecordDto != null) {
				System.out.println("REQUEST FORM: PAYROLL UPDATED");
			}
		}

		else {
			PayrollRecordDto payrollRecordDto = new PayrollRecordDto();

			payrollRecordDto.setCutOffPeriod(cutOffPeriod);
			payrollRecordDto.setPayrollRecordDate(employeeRecordDto.getEmployeeRecordDate());
			payrollRecordDto
					.setCutOffHoursRendered(employeeRecordService.getHoursPerCutOff(userId, year, month, cutOffPeriod));

			this.createPayrollRecord(userId, payrollRecordDto);
		}

	}

	private PayrollRecordDto createPayrollRecord(String userId, PayrollRecordDto payrollRecordDto) {
		return payrollRecordService.createPayrollRecord(userId, payrollRecordDto);
	}

	private NotificationPayloadDto createNotificationPayload(String title, String content) {
		return new NotificationPayloadDto(title, content);
	}

	private String getFullname(UserEntity userEntity) {
		return userEntity.getProfile().getPersonalInformation().getString("firstname") + " "
				+ userEntity.getProfile().getPersonalInformation().getString("lastname");
	}

	private boolean sendPush(String[] userIds, String title, String content) {
		return pushService.sendPushMessages(userIds, createNotificationPayload(title, content));
	}

	private String getStatus(boolean status) {
		if (status) {
			return "approved";
		}

		return "disapproved";
	}
}
