package com.dice205.hris.ws.service.impl;

import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dice205.hris.ws.exceptions.UserServiceException;
import com.dice205.hris.ws.io.entity.EmployeeRecordEntity;
import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.repositories.EmployeeRecordRepository;
import com.dice205.hris.ws.io.repositories.ProfileRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.service.EmployeeRecordService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.EmployeeRecordDto;
import com.dice205.hris.ws.shared.dto.HolidayTypes;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;

@Service
public class EmployeeRecordServiceImpl implements EmployeeRecordService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	EmployeeRecordRepository employeeRecordRepository;
	
	@Autowired
	ProfileRepository profileRepository;

	@Autowired
	Utils utils;
	
	@Override
	public Collection<EmployeeRecordDto> getEmployeeRecords(String userId, Date from, Date to, int page, int limit) {
		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);
		Collection<EmployeeRecordDto> returnValue = new ArrayList<>();
		
		if (userEntity == null) throw new UserServiceException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage()); 
		Pageable pageableRequest = PageRequest.of(page, limit);
		
		Page<EmployeeRecordEntity> employeeRecordList = employeeRecordRepository.findAllByUserAndEmployeeRecordDateLessThanEqualAndEmployeeRecordDateGreaterThanEqual(userEntity, to, from, pageableRequest);
		
		for (EmployeeRecordEntity employeeRecordEntity: employeeRecordList) {
			EmployeeRecordDto employeeRecordDto = new EmployeeRecordDto();
			
			BeanUtils.copyProperties(employeeRecordEntity, employeeRecordDto);
			
			try {
				employeeRecordDto.setHolidayType(HolidayTypes.valueOf(employeeRecordEntity.getHolidayType()));
			} catch (NullPointerException e) {
				
			}
			
			returnValue.add(employeeRecordDto);
		}

		return returnValue;
	}

	@Override
	public EmployeeRecordDto createEmployeeRecord(String userId, EmployeeRecordDto employeeRecordDto) {
		EmployeeRecordDto returnValue = new EmployeeRecordDto();

		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);
		
		if (userEntity == null) throw new UserServiceException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage()); 
		EmployeeRecordEntity employeeRecordEntity = new EmployeeRecordEntity();
		
		BeanUtils.copyProperties(employeeRecordDto, employeeRecordEntity);

		String firstname = userEntity.getProfile().getPersonalInformation().getString("firstname");
		String lastname = userEntity.getProfile().getPersonalInformation().getString("lastname");
		String creator = "";
		try {
			creator = utils.getAuth().getUserId();
		} catch (NullPointerException e) {
			creator = employeeRecordDto.getCreatedBy();
		}
		
		employeeRecordEntity.setEmployeeName(firstname + " " + lastname);
		employeeRecordEntity.setPosition(userEntity.getProfile().getWorkDetails().getString("positionTitle"));
		employeeRecordEntity.setCreatedBy(creator);
		employeeRecordEntity.setCreatedAt(utils.generateTimestamp());
		employeeRecordEntity.setUser(userEntity);
		employeeRecordEntity.setHolidayType(employeeRecordDto.getHolidayType().name());

		EmployeeRecordEntity storedEmployeeRecordEntity = employeeRecordRepository.save(employeeRecordEntity);

		if (storedEmployeeRecordEntity.equals(null)) throw new RuntimeException("Record failed");

		BeanUtils.copyProperties(storedEmployeeRecordEntity, returnValue);

		returnValue.setHolidayType(HolidayTypes.valueOf(storedEmployeeRecordEntity.getHolidayType()));
		
		return returnValue;
	}

	@Override
	public EmployeeRecordDto updateEmloyeeRecord(long id, EmployeeRecordDto employeeRecordDto) {
		EmployeeRecordDto returnValue = new EmployeeRecordDto(); 
		Optional<EmployeeRecordEntity> employeeRecordEntityOpt = employeeRecordRepository.findById(id);
		
		if (!employeeRecordEntityOpt.isPresent()) throw new RuntimeException("Employee Record, " + ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		
		EmployeeRecordEntity employeeRecordEntity = employeeRecordEntityOpt.get();
		
		employeeRecordEntity.setPosition(employeeRecordDto.getPosition());
		employeeRecordEntity.setBreakOBDuration(employeeRecordDto.getBreakOBDuration());
		employeeRecordEntity.setBreaksDuration(employeeRecordDto.getBreaksDuration());
		employeeRecordEntity.setBreaksOffset(employeeRecordDto.getBreaksOffset());
		employeeRecordEntity.setHoursRendered(employeeRecordDto.getHoursRendered());
		employeeRecordEntity.setOvertime(employeeRecordDto.getOvertime());
		employeeRecordEntity.setShift(employeeRecordDto.getShift());
		employeeRecordEntity.setUpdatedBy(utils.getAuth().getUserId());
		employeeRecordEntity.setUpdatedAt(utils.generateTimestamp());
		
		EmployeeRecordEntity storedEmployeeRecordEntity = employeeRecordRepository.save(employeeRecordEntity);
		
		if (storedEmployeeRecordEntity == null) throw new RuntimeException("Employee Record, " + ErrorMessages.COULD_NOT_UPDATE_RECORD.getErrorMessage());
		
		BeanUtils.copyProperties(storedEmployeeRecordEntity, returnValue);
		
		try {
			returnValue.setHolidayType(HolidayTypes.valueOf(storedEmployeeRecordEntity.getHolidayType()));
		} catch(NullPointerException e) {
			
		}
		
		return returnValue;
	}

	@Override
	public boolean softDeleteEmployeeRecord(long id) {
		boolean returnValue = false;
		
		Optional<EmployeeRecordEntity> employeeRecordEntityOpt = employeeRecordRepository.findById(id);
		
		if(employeeRecordEntityOpt.isPresent()) {
			EmployeeRecordEntity employeeRecordEntity = employeeRecordEntityOpt.get();
			
			employeeRecordEntity.setDeletedAt(utils.generateTimestamp());
			
			employeeRecordRepository.save(employeeRecordEntity);
			
			returnValue = true;
		}
		
		return returnValue;
	}

	@Override
	public double getHoursPerCutOff(String userId, int year, int month, int cutOffPeriod) {
		double returnValue = 0;
		
		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);

		if (userEntity == null) throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());

		String cutOffPeriodString = String.valueOf(cutOffPeriod);
		
		List<EmployeeRecordEntity> employeeRecordList = employeeRecordRepository.getByUserAndCutOffAndDate(userEntity, cutOffPeriodString, this.dateToString(year, month));
				
		for (EmployeeRecordEntity employeeRecordEntity : employeeRecordList) {
			returnValue += employeeRecordEntity.getHoursRendered();
		}
		
		System.out.println("HOURS PER CUTOFF "+cutOffPeriodString+" => " + returnValue);
				
		BigDecimal bigDecimal = new BigDecimal(returnValue);

		return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	@Override
	public double getBreaksDurationPerCutOff(String userId, int year, int month, int cutOffPeriod) {
		double returnValue = 0;
		
		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);

		if (userEntity == null) throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());

		String cutOffPeriodString = String.valueOf(cutOffPeriod);
		
		List<EmployeeRecordEntity> employeeRecordList = employeeRecordRepository.getByUserAndCutOffAndDate(userEntity, cutOffPeriodString, this.dateToString(year, month));
				
		for (EmployeeRecordEntity employeeRecordEntity : employeeRecordList) {
			returnValue += employeeRecordEntity.getBreaksDuration();
		}
		
		System.out.println("BREAKS DURATION PER CUTOFF => " + returnValue);
				
		BigDecimal bigDecimal = new BigDecimal(returnValue);

		return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	@Override
	public double getOfficialBusinessPerCutOff(String userId, int year, int month, int cutOffPeriod) {
		double returnValue = 0;
		
		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);

		if (userEntity == null) throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());

		String cutOffPeriodString = String.valueOf(cutOffPeriod);
		
		List<EmployeeRecordEntity> employeeRecordList = employeeRecordRepository.getByUserAndCutOffAndDate(userEntity, cutOffPeriodString, this.dateToString(year, month));
				
		for (EmployeeRecordEntity employeeRecordEntity : employeeRecordList) {
			returnValue += employeeRecordEntity.getBreakOBDuration();
		}
		
		System.out.println("OFFICIAL BUSINESS DURATION PER CUTOFF => " + returnValue);
				
		BigDecimal bigDecimal = new BigDecimal(returnValue);

		return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	@Override
	public double getOverBreakPerCutOff(String userId, int year, int month, int cutOffPeriod) {
		double returnValue = 0;
		
		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);

		if (userEntity == null) throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());

		String cutOffPeriodString = String.valueOf(cutOffPeriod);
		
		List<EmployeeRecordEntity> employeeRecordList = employeeRecordRepository.getByUserAndCutOffAndDate(userEntity, cutOffPeriodString, this.dateToString(year, month));
				
		for (EmployeeRecordEntity employeeRecordEntity : employeeRecordList) {
			returnValue += employeeRecordEntity.getBreaksOffset();
		}
		
		System.out.println("OVER BREAK PER CUTOFF => " + returnValue);
				
		BigDecimal bigDecimal = new BigDecimal(returnValue);

		return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	@Override
	public double getOvertimePerCutOff(String userId, int year, int month, int cutOffPeriod) {
		double returnValue = 0;
		
		UserEntity userEntity = userRepository.findByUserIdAndDeletedAtIsNull(userId);

		if (userEntity == null) throw new RuntimeException(ErrorMessages.USER_DOES_NOT_EXIST.getErrorMessage());

		String cutOffPeriodString = String.valueOf(cutOffPeriod);
		
		List<EmployeeRecordEntity> employeeRecordList = employeeRecordRepository.getByUserAndCutOffAndDate(userEntity, cutOffPeriodString, this.dateToString(year, month));
				
		for (EmployeeRecordEntity employeeRecordEntity : employeeRecordList) {
			returnValue += employeeRecordEntity.getOvertime();
		}
		
		System.out.println("OVERTIME PER CUTOFF => " + returnValue);
				
		BigDecimal bigDecimal = new BigDecimal(returnValue);

		return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	private String dateToString(int year, int month) {
		String monthString = String.valueOf(month);
		
		if (monthString.length() == 1) {
			monthString = "0" + monthString;
		}
		
		String dateString = String.valueOf(year) + "_" + monthString + "%";
		
		return dateString;
	}

}
