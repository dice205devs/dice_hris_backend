package com.dice205.hris.ws.ui.model.response;

import com.dice205.hris.ws.shared.dto.KeysDto;

public class KeysRest {
	private String auth;
	private String p256dh;

	public KeysRest() {}
	
	public KeysRest(KeysDto keys) {
		this.auth = keys.getAuth();
		this.p256dh = keys.getP256dh();
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getP256dh() {
		return p256dh;
	}

	public void setP256dh(String p256dh) {
		this.p256dh = p256dh;
	}
}
