package com.dice205.hris.ws.ui.model.request;

import com.dice205.hris.ws.shared.dto.KeysDto;

public class SubscriptionRequestModel {
	private String subscriptionId;
	private String endpoint;
	private String userId;
	private KeysDto keys;

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public KeysDto getKeys() {
		return keys;
	}

	public void setKeys(KeysDto keys) {
		this.keys = keys;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
