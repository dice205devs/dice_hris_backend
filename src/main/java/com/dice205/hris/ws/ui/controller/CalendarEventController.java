package com.dice205.hris.ws.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dice205.hris.ws.service.CalendarEventService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.CalendarEventDto;
import com.dice205.hris.ws.ui.model.query.CalendarEventQueryModel;
import com.dice205.hris.ws.ui.model.request.CalendarEventRequestModel;
import com.dice205.hris.ws.ui.model.response.CalendarEventRest;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;
import com.dice205.hris.ws.ui.model.response.OperationStatusModel;
import com.dice205.hris.ws.ui.model.response.RequestOperationStatus;

@RestController
@RequestMapping("calendar-event")
public class CalendarEventController {
	@Autowired
	CalendarEventService calendarEventService;
	
	@Autowired
	Utils utils;
	
	@Secured("ROLE_ADMIN")
	@PostMapping(produces = {MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE})
	public CalendarEventRest createCalendarEvent(@RequestBody CalendarEventRequestModel calendarEventDetails) {
		CalendarEventRest returnValue = new CalendarEventRest();
		
		if (calendarEventDetails.getEventName().isEmpty()) throw new NullPointerException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		
		CalendarEventDto calendarEventDto = new CalendarEventDto();
		
		BeanUtils.copyProperties(calendarEventDetails, calendarEventDto);

		CalendarEventDto storedCalendarEventDto = calendarEventService.createCalendarEvent(calendarEventDto);
		
		BeanUtils.copyProperties(storedCalendarEventDto, returnValue);
		
		return returnValue;
	}
	
	@PutMapping(path = "/{id}",produces = {MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE})
	public CalendarEventRest updateCalendarEvent(@PathVariable(value = "id") long id, @RequestBody CalendarEventRequestModel calendarEventDetails) {
		CalendarEventRest returnValue = new CalendarEventRest();
		
		if (calendarEventDetails.getEventName().isEmpty()) throw new NullPointerException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		
		CalendarEventDto calendarEventDto = new CalendarEventDto();
		
		BeanUtils.copyProperties(calendarEventDetails, calendarEventDto);

		CalendarEventDto updatedCalendarEventDto = calendarEventService.updateCalendarEvent(id, calendarEventDto);
		
		BeanUtils.copyProperties(updatedCalendarEventDto, returnValue);
		
		return returnValue;
	}
	
	@PutMapping(path = "/remove/{id}",produces = {MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE})
	public OperationStatusModel deleteCalendarEvent(@PathVariable(value = "id") long id) {
		OperationStatusModel returnValue = new OperationStatusModel();

		boolean deleteCalendarEvent = calendarEventService.deleteCalendarEvent(id);

		returnValue.setOperationName(RequestOperationName.DELETE.name());

		if (deleteCalendarEvent) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		} else {
			returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
		}

		return returnValue;
	}
	
	
	@PostMapping(path = "/query", produces = {MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE})
	public List<CalendarEventRest> getByEventDate(@RequestBody CalendarEventQueryModel calendarEventQuery) {
		List<CalendarEventRest> returnValue = new ArrayList<>();
		
		for (CalendarEventDto calendarEventDto: calendarEventService.getByEventDate(calendarEventQuery.getYear(), calendarEventQuery.getMonth(), calendarEventQuery.getDay())) {
			CalendarEventRest calendarEventRest = new CalendarEventRest();
			
			BeanUtils.copyProperties(calendarEventDto, calendarEventRest);
			
			returnValue.add(calendarEventRest);
		}
		
		return returnValue;
	}
	
	@PostMapping(path = "/meeting-schedule",produces = {MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE})
	public CalendarEventRest createMeetingSchedule(@RequestBody CalendarEventRequestModel calendarEventDetails) {
		CalendarEventRest returnValue = new CalendarEventRest();
		
		if (calendarEventDetails.getParticipantIds().size() < 1) throw new NullPointerException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		
		CalendarEventDto calendarEventDto = new CalendarEventDto();
		
		BeanUtils.copyProperties(calendarEventDetails, calendarEventDto);

		CalendarEventDto storedCalendarEventDto = calendarEventService.createMeetingSchedule(calendarEventDto);
		
		BeanUtils.copyProperties(storedCalendarEventDto, returnValue);
		
		return returnValue;
	}
}
