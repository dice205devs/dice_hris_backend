package com.dice205.hris.ws.ui.model.request;

import com.dice205.hris.ws.shared.dto.NotificationPayloadDto;
import com.dice205.hris.ws.ui.model.query.UsersQueryModel;

public class SendPushRequestModel extends UsersQueryModel {
	private NotificationPayloadDto notificationPayload;

	public NotificationPayloadDto getNotificationPayload() {
		return notificationPayload;
	}

	public void setNotificationPayload(NotificationPayloadDto notificationPayload) {
		this.notificationPayload = notificationPayload;
	}

}
