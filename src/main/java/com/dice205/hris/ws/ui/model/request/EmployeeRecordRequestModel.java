package com.dice205.hris.ws.ui.model.request;

import java.sql.Date;

import com.dice205.hris.ws.shared.dto.HolidayTypes;

public class EmployeeRecordRequestModel {
	private String employeeName;
	private String position;
	private String shift;
	private boolean isLate;
	private int requiredHours;
	private double breaksDuration;
	private double breaksOffset;
	private double breakOBDuration;
	private double overtime;
	private double hoursRendered;
	private Date employeeRecordDate;
	private int cutOffPeriod;
	private String leaveType;
	private HolidayTypes holidayType;

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getShift() {
		return shift;
	}

	public void setShift(String shift) {
		this.shift = shift;
	}

	public int getRequiredHours() {
		return requiredHours;
	}

	public void setRequiredHours(int requiredHours) {
		this.requiredHours = requiredHours;
	}

	public double getBreaksDuration() {
		return breaksDuration;
	}

	public void setBreaksDuration(double breaksDuration) {
		this.breaksDuration = breaksDuration;
	}

	public double getBreaksOffset() {
		return breaksOffset;
	}

	public void setBreaksOffset(double breaksOffset) {
		this.breaksOffset = breaksOffset;
	}

	public double getBreakOBDuration() {
		return breakOBDuration;
	}

	public void setBreakOBDuration(double breakOBDuration) {
		this.breakOBDuration = breakOBDuration;
	}

	public double getOvertime() {
		return overtime;
	}

	public void setOvertime(double overtime) {
		this.overtime = overtime;
	}

	public double getHoursRendered() {
		return hoursRendered;
	}

	public void setHoursRendered(double hoursRendered) {
		this.hoursRendered = hoursRendered;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public Date getEmployeeRecordDate() {
		return employeeRecordDate;
	}

	public void setEmployeeRecordDate(Date employeeRecordDate) {
		this.employeeRecordDate = employeeRecordDate;
	}

	public int getCutOffPeriod() {
		return cutOffPeriod;
	}

	public void setCutOffPeriod(int cutOffPeriod) {
		this.cutOffPeriod = cutOffPeriod;
	}

	public boolean isLate() {
		return isLate;
	}

	public void setLate(boolean isLate) {
		this.isLate = isLate;
	}

	public HolidayTypes getHolidayType() {
		return holidayType;
	}

	public void setHolidayType(HolidayTypes holidayType) {
		this.holidayType = holidayType;
	}
}
