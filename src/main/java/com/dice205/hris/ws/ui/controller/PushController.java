package com.dice205.hris.ws.ui.controller;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dice205.hris.ws.shared.dto.SubscriptionDto;
import com.dice205.hris.ws.ui.model.query.UsersQueryModel;
import com.dice205.hris.ws.ui.model.request.SendPushRequestModel;
import com.dice205.hris.ws.ui.model.request.SubscriptionRequestModel;
import com.dice205.hris.ws.ui.model.request.UnsubscribeRequestModel;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;
import com.dice205.hris.ws.ui.model.response.KeysRest;
import com.dice205.hris.ws.ui.model.response.OperationStatusModel;
import com.dice205.hris.ws.ui.model.response.RequestOperationStatus;
import com.dice205.hris.ws.ui.model.response.SubscriptionRest;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("web-push")
public class PushController {
	@Autowired
	com.dice205.hris.ws.service.PushService pushService;

	@PostMapping(path = "/subscribe", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public SubscriptionRest subscribe(@RequestBody SubscriptionRequestModel subscriptionDetails) {
		SubscriptionRest returnValue = new SubscriptionRest();

		SubscriptionDto subscriptionDto = new SubscriptionDto();

		BeanUtils.copyProperties(subscriptionDetails, subscriptionDto);

		BeanUtils.copyProperties(subscriptionDetails.getKeys(), subscriptionDto.getKeys());

		SubscriptionDto createdSubscriptionDto = pushService.subscribe(subscriptionDto);

		BeanUtils.copyProperties(createdSubscriptionDto, returnValue);
		KeysRest keysRest = new KeysRest(createdSubscriptionDto.getKeys());

		returnValue.setKeys(keysRest);

		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@GetMapping(path = "/all", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<SubscriptionRest> getAllSubscriptions() {
		List<SubscriptionRest> returnValue = new ArrayList<>();

		for (SubscriptionDto subscriptionDto : pushService.getAllSubscriptions()) {
			SubscriptionRest subscriptionRest = new SubscriptionRest();

			BeanUtils.copyProperties(subscriptionDto, subscriptionRest);

			KeysRest keysRest = new KeysRest(subscriptionDto.getKeys());

			subscriptionRest.setKeys(keysRest);

			returnValue.add(subscriptionRest);
		}

		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@GetMapping(path = "/{subscriptionId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public SubscriptionRest getBySubscriptionId(@PathVariable("subscriptionId") String subscriptionId) {
		SubscriptionRest returnValue = new SubscriptionRest();

		SubscriptionDto subscriptionDto = pushService.getBySubscriptionId(subscriptionId);

		BeanUtils.copyProperties(subscriptionDto, returnValue);

		returnValue.setKeys(new KeysRest(subscriptionDto.getKeys()));

		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@PostMapping(path = "/send", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel sendPushTest(@RequestBody SendPushRequestModel sendPushDetails)
			throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException, JsonProcessingException {
		OperationStatusModel returnValue = new OperationStatusModel();

		boolean sent = pushService.sendPushMessages(sendPushDetails.getUserIds(), sendPushDetails.getNotificationPayload());

		returnValue.setOperationName(RequestOperationName.SEND_PUSH.name());

		if (sent) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		} else {
			returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
		}

		return returnValue;
	}

	@PostMapping(path = "/unsubscribe", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel unsubscribe(@RequestBody UnsubscribeRequestModel unsubscribeRequestDetails) {
		OperationStatusModel returnValue = new OperationStatusModel();

		if (unsubscribeRequestDetails.getEndpoint().isEmpty()) throw new NullPointerException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		
		boolean isUnsubscribed = pushService.unsubscribe(unsubscribeRequestDetails.getEndpoint(),
				unsubscribeRequestDetails.isUnsubscribed());

		returnValue.setOperationName(RequestOperationName.UNSUBSCRIBE.name());

		if (isUnsubscribed) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		} else {
			returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
		}

		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@PostMapping(path = "/users-subscription", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public List<SubscriptionRest> getSubscriptionByUsers(@RequestBody UsersQueryModel users) {
		List<SubscriptionRest> returnValue = new ArrayList<>();

		if (users.getUserIds().length < 1) throw new NullPointerException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		
		for (SubscriptionDto subscriptionDto : pushService.getByUsers(users.getUserIds())) {
			SubscriptionRest subscriptionRest = new SubscriptionRest();

			BeanUtils.copyProperties(subscriptionDto, subscriptionRest);

			KeysRest keysRest = new KeysRest(subscriptionDto.getKeys());

			subscriptionRest.setKeys(keysRest);

			returnValue.add(subscriptionRest);
		}

		return returnValue;
	}
}
