package com.dice205.hris.ws.ui.model.json;

import java.sql.Date;

import com.dice205.hris.ws.shared.Utils;

public class WorkDetailsModel {
	private Date dateHired = Utils.generateDate();
	private String recruitmentChannel = "";
	private double yearsOfExperienceUponHiring = 0;
	private Date regularizationDate = Utils.generateDate();
	private String tenureInTheCompany = "";
	private String associateStatus = "";
	private String employmentType = "";
	private String primaryWorkLocation = "";
	private String positionTitle = "";
	private String department = "";
	private String careerLevel = "";
	private String careerBand = "";
	private String careerZone = "";
	private String tenureInTheZone = "";
	private int startingPay = 0;
	private int currentPay = 0;
	private String payScaleType = "";

	public Date getDateHired() {
		return dateHired;
	}

	public void setDateHired(Date dateHired) {
		this.dateHired = dateHired;
	}

	public String getRecruitmentChannel() {
		return recruitmentChannel;
	}

	public void setRecruitmentChannel(String recruitmentChannel) {
		this.recruitmentChannel = recruitmentChannel;
	}

	public double getYearsOfExperienceUponHiring() {
		return yearsOfExperienceUponHiring;
	}

	public void setYearsOfExperienceUponHiring(double yearsOfExperienceUponHiring) {
		this.yearsOfExperienceUponHiring = yearsOfExperienceUponHiring;
	}

	public Date getRegularizationDate() {
		return regularizationDate;
	}

	public void setRegularizationDate(Date regularizationDate) {
		this.regularizationDate = regularizationDate;
	}

	public String getTenureInTheCompany() {
		return tenureInTheCompany;
	}

	public void setTenureInTheCompany(String tenureInTheCompany) {
		this.tenureInTheCompany = tenureInTheCompany;
	}

	public String getAssociateStatus() {
		return associateStatus;
	}

	public void setAssociateStatus(String associateStatus) {
		this.associateStatus = associateStatus;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public String getPrimaryWorkLocation() {
		return primaryWorkLocation;
	}

	public void setPrimaryWorkLocation(String primaryWorkLocation) {
		this.primaryWorkLocation = primaryWorkLocation;
	}

	public String getPositionTitle() {
		return positionTitle;
	}

	public void setPositionTitle(String positionTitle) {
		this.positionTitle = positionTitle;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCareerLevel() {
		return careerLevel;
	}

	public void setCareerLevel(String careerLevel) {
		this.careerLevel = careerLevel;
	}

	public String getCareerBand() {
		return careerBand;
	}

	public void setCareerBand(String careerBand) {
		this.careerBand = careerBand;
	}

	public String getCareerZone() {
		return careerZone;
	}

	public void setCareerZone(String careerZone) {
		this.careerZone = careerZone;
	}

	public String getTenureInTheZone() {
		return tenureInTheZone;
	}

	public void setTenureInTheZone(String tenureInTheZone) {
		this.tenureInTheZone = tenureInTheZone;
	}

	public int getStartingPay() {
		return startingPay;
	}

	public void setStartingPay(int startingPay) {
		this.startingPay = startingPay;
	}

	public int getCurrentPay() {
		return currentPay;
	}

	public void setCurrentPay(int currentPay) {
		this.currentPay = currentPay;
	}

	public String getPayScaleType() {
		return payScaleType;
	}

	public void setPayScaleType(String payScaleType) {
		this.payScaleType = payScaleType;
	}
}
