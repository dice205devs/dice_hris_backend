package com.dice205.hris.ws.ui.model.query;

import java.sql.Date;

public class EmployeeStatsQueryModel {
	private Date date;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
