package com.dice205.hris.ws.ui.model.request;

import java.sql.Date;
import java.sql.Time;

import com.dice205.hris.ws.shared.dto.RequestTypes;

public class RequestFormRequestModel {
	private RequestTypes requestType;
	private Boolean requestStatus;
	private String reason;
	private Date dateFiled;
	private Date overtimeDate;
	private Time overtimeStart;
	private Time overtimeEnd;
	private Date leaveDate;

	public Boolean getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(Boolean requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getDateFiled() {
		return dateFiled;
	}

	public void setDateFiled(Date dateFiled) {
		this.dateFiled = dateFiled;
	}

	public Date getOvertimeDate() {
		return overtimeDate;
	}

	public void setOvertimeDate(Date overtimeDate) {
		this.overtimeDate = overtimeDate;
	}

	public Time getOvertimeStart() {
		return overtimeStart;
	}

	public void setOvertimeStart(Time overtimeStart) {
		this.overtimeStart = overtimeStart;
	}

	public Time getOvertimeEnd() {
		return overtimeEnd;
	}

	public void setOvertimeEnd(Time overtimeEnd) {
		this.overtimeEnd = overtimeEnd;
	}

	public Date getLeaveDate() {
		return leaveDate;
	}

	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}

	public RequestTypes getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestTypes requestType) {
		this.requestType = requestType;
	}
}
