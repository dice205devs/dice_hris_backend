package com.dice205.hris.ws.ui.model.request;

import java.sql.Date;
import java.sql.Time;

import com.dice205.hris.ws.ui.model.json.OfficialBusinessModel;

public class TimesheetRequestModel {
	private Date timesheetDate;
	private Time timeStart;
	private Time timeEnd;
	private OfficialBusinessModel officialBusiness;
	private String breakType;

	public Date getTimesheetDate() {
		return timesheetDate;
	}

	public void setTimesheetDate(Date timesheetDate) {
		this.timesheetDate = timesheetDate;
	}

	public Time getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(Time timeStart) {
		this.timeStart = timeStart;
	}

	public Time getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(Time timeEnd) {
		this.timeEnd = timeEnd;
	}

	public String getBreakType() {
		return breakType;
	}

	public void setBreakType(String breakType) {
		this.breakType = breakType;
	}

	public OfficialBusinessModel getOfficialBusiness() {
		return officialBusiness;
	}

	public void setOfficialBusiness(OfficialBusinessModel officialBusiness) {
		this.officialBusiness = officialBusiness;
	}

}
