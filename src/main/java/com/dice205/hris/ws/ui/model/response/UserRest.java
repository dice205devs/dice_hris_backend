package com.dice205.hris.ws.ui.model.response;

import java.sql.Time;
import java.util.Collection;

import com.dice205.hris.ws.shared.dto.EmploymentStatus;
import com.dice205.hris.ws.shared.dto.WorkingStatus;

public class UserRest {
	private String userId;
	private String email;
	private String supervisorId;
	private EmploymentStatus employmentStatus;
	private WorkingStatus workingStatus;
	private int earlyTimeoutCount;
	private Collection<String> roles;
	
	private Time shift;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Time getShift() {
		return shift;
	}

	public void setShift(Time shift) {
		this.shift = shift;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	public WorkingStatus getWorkingStatus() {
		return workingStatus;
	}

	public void setWorkingStatus(WorkingStatus workingStatus) {
		this.workingStatus = workingStatus;
	}

	public Collection<String> getRoles() {
		return roles;
	}

	public void setRoles(Collection<String> roles) {
		this.roles = roles;
	}

	public int getEarlyTimeoutCount() {
		return earlyTimeoutCount;
	}

	public void setEarlyTimeoutCount(int earlyTimeoutCount) {
		this.earlyTimeoutCount = earlyTimeoutCount;
	}

	public EmploymentStatus getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(EmploymentStatus employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

}
