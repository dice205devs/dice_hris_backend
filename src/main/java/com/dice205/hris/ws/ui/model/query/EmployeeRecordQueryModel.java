package com.dice205.hris.ws.ui.model.query;

import java.sql.Date;

public class EmployeeRecordQueryModel {
	private Date from;
	private Date to;
	
	public Date getFrom() {
		return from;
	}
	public void setFrom(Date from) {
		this.from = from;
	}
	public Date getTo() {
		return to;
	}
	public void setTo(Date to) {
		this.to = to;
	}
}
