package com.dice205.hris.ws.ui.model.response;

import java.sql.Date;

public class PayrollRecordRest {
	private long id;
	private String employeeName;
	private String userId;
	private Date payrollRecordDate;
	private double cutOffRequiredHours;
	private double cutOffHoursRendered;
	private double cutOffBreaksRendered;
	private double cutOffOBRendered;
	private double cutOffOvertimeRendered;
	private double cutOffBreaksOffset;
	private int cutOffPeriod;

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public double getCutOffHoursRendered() {
		return cutOffHoursRendered;
	}

	public void setCutOffHoursRendered(double cutOffHoursRendered) {
		this.cutOffHoursRendered = cutOffHoursRendered;
	}

	public int getCutOffPeriod() {
		return cutOffPeriod;
	}

	public void setCutOffPeriod(int cutOffPeriod) {
		this.cutOffPeriod = cutOffPeriod;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getPayrollRecordDate() {
		return payrollRecordDate;
	}

	public void setPayrollRecordDate(Date payrollRecordDate) {
		this.payrollRecordDate = payrollRecordDate;
	}

	public double getCutOffRequiredHours() {
		return cutOffRequiredHours;
	}

	public void setCutOffRequiredHours(double cutOffRequiredHours) {
		this.cutOffRequiredHours = cutOffRequiredHours;
	}

	public double getCutOffBreaksRendered() {
		return cutOffBreaksRendered;
	}

	public void setCutOffBreaksRendered(double cutOffBreaksRendered) {
		this.cutOffBreaksRendered = cutOffBreaksRendered;
	}

	public double getCutOffOBRendered() {
		return cutOffOBRendered;
	}

	public void setCutOffOBRendered(double cutOffOBRendered) {
		this.cutOffOBRendered = cutOffOBRendered;
	}

	public double getCutOffOvertimeRendered() {
		return cutOffOvertimeRendered;
	}

	public void setCutOffOvertimeRendered(double cutOffOvertimeRendered) {
		this.cutOffOvertimeRendered = cutOffOvertimeRendered;
	}

	public double getCutOffBreaksOffset() {
		return cutOffBreaksOffset;
	}

	public void setCutOffBreaksOffset(double cutOffBreaksOffset) {
		this.cutOffBreaksOffset = cutOffBreaksOffset;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
