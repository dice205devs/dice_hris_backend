package com.dice205.hris.ws.ui.model.response;

public enum RequestOperationStatus {
	SUCCESS, ERROR
}
