package com.dice205.hris.ws.ui.model.json;

public class OfficialBusinessModel {
	private String reason;
	private String task;

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}
}
