package com.dice205.hris.ws.ui.model.response;

import java.sql.Date;

import com.dice205.hris.ws.shared.dto.RequestTypes;

public class RequestFormRest {
	private long id;
	private RequestTypes requestType;
	private String requestStatus;
	private String reason;
	private Date dateFiled;
	private double overtimeDuration;
	private Date overtimeDate;
	private Date leaveDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getDateFiled() {
		return dateFiled;
	}

	public void setDateFiled(Date dateFiled) {
		this.dateFiled = dateFiled;
	}

	public double getOvertimeDuration() {
		return overtimeDuration;
	}

	public void setOvertimeDuration(double overtimeDuration) {
		this.overtimeDuration = overtimeDuration;
	}

	public Date getOvertimeDate() {
		return overtimeDate;
	}

	public void setOvertimeDate(Date overtimeDate) {
		this.overtimeDate = overtimeDate;
	}

	public Date getLeaveDate() {
		return leaveDate;
	}

	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}

	public RequestTypes getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestTypes requestType) {
		this.requestType = requestType;
	}
}
