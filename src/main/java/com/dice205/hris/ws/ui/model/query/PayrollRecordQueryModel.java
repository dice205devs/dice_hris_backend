package com.dice205.hris.ws.ui.model.query;

public class PayrollRecordQueryModel {
	private int year;
	private int month;
	private	int cutOffPeriod;
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getCutOffPeriod() {
		return cutOffPeriod;
	}
	public void setCutOffPeriod(int cutOffPeriod) {
		this.cutOffPeriod = cutOffPeriod;
	}
}
