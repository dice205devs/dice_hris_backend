package com.dice205.hris.ws.ui.model.request;

import com.dice205.hris.ws.ui.model.json.ContactInformationModel;
import com.dice205.hris.ws.ui.model.json.EducationalBackgroundModel;
import com.dice205.hris.ws.ui.model.json.EmploymentBackgroundModel;
import com.dice205.hris.ws.ui.model.json.GovernmentInformationModel;
import com.dice205.hris.ws.ui.model.json.PersonalInformationModel;
import com.dice205.hris.ws.ui.model.json.WorkDetailsModel;

public class ProfileReqModel {
	private PersonalInformationModel personalInformation;
	private EducationalBackgroundModel educationalBackground;
	private ContactInformationModel contactInformation;
	private GovernmentInformationModel governmentInformation;
	private EmploymentBackgroundModel employmentBackground;
	private WorkDetailsModel workDetails;

	public EducationalBackgroundModel getEducationalBackground() {
		return educationalBackground;
	}

	public void setEducationalBackground(EducationalBackgroundModel educationalBackground) {
		this.educationalBackground = educationalBackground;
	}

	public ContactInformationModel getContactInformation() {
		return contactInformation;
	}

	public void setContactInformation(ContactInformationModel contactInformation) {
		this.contactInformation = contactInformation;
	}

	public GovernmentInformationModel getGovernmentInformation() {
		return governmentInformation;
	}

	public void setGovernmentInformation(GovernmentInformationModel governmentInformation) {
		this.governmentInformation = governmentInformation;
	}

	public EmploymentBackgroundModel getEmploymentBackground() {
		return employmentBackground;
	}

	public void setEmploymentBackground(EmploymentBackgroundModel employmentBackground) {
		this.employmentBackground = employmentBackground;
	}

	public WorkDetailsModel getWorkDetails() {
		return workDetails;
	}

	public void setWorkDetails(WorkDetailsModel workDetails) {
		this.workDetails = workDetails;
	}

	public PersonalInformationModel getPersonalInformation() {
		return personalInformation;
	}

	public void setPersonalInformation(PersonalInformationModel personalInformation) {
		this.personalInformation = personalInformation;
	}
}