package com.dice205.hris.ws.ui.model.request;

public class RoleReqModel {
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
