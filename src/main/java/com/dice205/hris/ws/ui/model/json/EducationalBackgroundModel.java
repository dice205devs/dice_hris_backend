package com.dice205.hris.ws.ui.model.json;

public class EducationalBackgroundModel {
	private String school = "";
	private String course = "";
	private String location = "";
	private String yearsAttended = "";

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getYearsAttended() {
		return yearsAttended;
	}

	public void setYearsAttended(String yearsAttended) {
		this.yearsAttended = yearsAttended;
	}

}
