package com.dice205.hris.ws.ui.model.request;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import com.dice205.hris.ws.shared.dto.HolidayTypes;

public class CalendarEventRequestModel {
	private String eventName;
	private Date eventDate;
	private HolidayTypes holidayType;
	private Time eventTime;
	private List<String> participantIds;

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	public List<String> getParticipantIds() {
		return participantIds;
	}

	public void setParticipantIds(List<String> participantIds) {
		this.participantIds = participantIds;
	}

	public HolidayTypes getHolidayType() {
		return holidayType;
	}

	public void setHolidayType(HolidayTypes holidayType) {
		this.holidayType = holidayType;
	}
}
