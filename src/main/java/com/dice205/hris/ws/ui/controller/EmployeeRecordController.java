package com.dice205.hris.ws.ui.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dice205.hris.ws.service.EmployeeRecordService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.EmployeeRecordDto;
import com.dice205.hris.ws.ui.model.query.EmployeeRecordQueryModel;
import com.dice205.hris.ws.ui.model.request.EmployeeRecordRequestModel;
import com.dice205.hris.ws.ui.model.response.EmployeeRecordRest;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;
import com.dice205.hris.ws.ui.model.response.OperationStatusModel;
import com.dice205.hris.ws.ui.model.response.RequestOperationStatus;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

@RestController
@RequestMapping("employee-record")
public class EmployeeRecordController {
	@Autowired
	EmployeeRecordService employeeRecordService;

	@Autowired
	Utils utils;

	@PostMapping(path="/export-csv/{userId}",  consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public void exportCSV(HttpServletResponse response, @PathVariable(value = "userId") String userId,
			@RequestBody EmployeeRecordQueryModel employeeQuery) throws Exception {
		List<EmployeeRecordRest> employeeRecords = new ArrayList<>();
		Collection<EmployeeRecordRest> collection = this.getEmployeeRecords(userId, employeeQuery, 0, 999);

		for (EmployeeRecordRest employeeRecordRest : collection) {
			employeeRecords.add(employeeRecordRest);
		}

		// set file name and content type
		String filename = "hris_employee_record.csv";

		response.setContentType("text/csv");
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"");

		StatefulBeanToCsv<EmployeeRecordRest> writer = new StatefulBeanToCsvBuilder<EmployeeRecordRest>(
				response.getWriter()).withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).build();

		writer.write(employeeRecords);

	}

	@PostAuthorize("hasRole('ADMIN') or returnObject[0].userId == principal.userId")
	@PostMapping(path = "/query/{userId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE },  consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public Collection<EmployeeRecordRest> getEmployeeRecords(@PathVariable(value = "userId") String userId,
			@RequestBody EmployeeRecordQueryModel employeeQuery,
			@RequestParam("page") int page,
			@RequestParam("limit") int limit) {
		Collection<EmployeeRecordRest> returnValue = new ArrayList<>();

		for (EmployeeRecordDto employeeRecordDto : employeeRecordService.getEmployeeRecords(userId, employeeQuery.getFrom(), employeeQuery.getTo(), page, limit)) {
			EmployeeRecordRest employeeRecordRest = new EmployeeRecordRest();

			BeanUtils.copyProperties(employeeRecordDto, employeeRecordRest);

			employeeRecordRest.setUserId(employeeRecordDto.getUser().getUserId());
			
			returnValue.add(employeeRecordRest);
		}

		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@PostMapping(path = "/{userId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public EmployeeRecordRest createEmployeeRecord(@PathVariable(value = "userId") String userId, @RequestBody EmployeeRecordRequestModel employeeRecordDetails) {
		EmployeeRecordRest returnValue = new EmployeeRecordRest();

		if (employeeRecordDetails.getEmployeeName().isEmpty()) throw new RuntimeException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		EmployeeRecordDto employeeRecordDto = new EmployeeRecordDto();
		
		BeanUtils.copyProperties(employeeRecordDetails, employeeRecordDto);
		
		EmployeeRecordDto createdEmployeeRecordDto = employeeRecordService.createEmployeeRecord(userId, employeeRecordDto);

		BeanUtils.copyProperties(createdEmployeeRecordDto, returnValue);
		returnValue.setUserId(createdEmployeeRecordDto.getUser().getUserId());

		return returnValue;
	}
	
	@Secured("ROLE_ADMIN")
	@PutMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public EmployeeRecordRest updateEmployeeRecord(@PathVariable(value = "id") long id, @RequestBody EmployeeRecordRequestModel employeeRecordDetails) {
		EmployeeRecordRest returnValue = new EmployeeRecordRest(); 
		
		EmployeeRecordDto employeeRecordDto = new EmployeeRecordDto();
		
		BeanUtils.copyProperties(employeeRecordDetails, employeeRecordDto);
		
		EmployeeRecordDto updatedEmployeeRecordDto = employeeRecordService.updateEmloyeeRecord(id, employeeRecordDto);
		
		BeanUtils.copyProperties(updatedEmployeeRecordDto, returnValue);
		returnValue.setUserId(updatedEmployeeRecordDto.getUser().getUserId());
		
		return returnValue;
	}
	
	@Secured("ROLE_ADMIN")
	@PutMapping(path = "/remove/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deleteEmployeeRecord(@PathVariable("id") long id) {
		OperationStatusModel returnValue = new OperationStatusModel();

		boolean deleteEmployeeRecord = employeeRecordService.softDeleteEmployeeRecord(id);

		returnValue.setOperationName(RequestOperationName.DELETE.name());

		if (deleteEmployeeRecord) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		} else {
			returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
		}

		return returnValue;
	}
}
