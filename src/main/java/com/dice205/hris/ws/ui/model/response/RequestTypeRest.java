package com.dice205.hris.ws.ui.model.response;

public class RequestTypeRest {
	private long id;

	private String name;
	
	private long fileByDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getFileByDate() {
		return fileByDate;
	}

	public void setFileByDate(long fileByDate) {
		this.fileByDate = fileByDate;
	}
}
