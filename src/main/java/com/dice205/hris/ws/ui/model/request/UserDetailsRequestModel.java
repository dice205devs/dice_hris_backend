package com.dice205.hris.ws.ui.model.request;

import java.sql.Time;
import java.util.Collection;

import com.dice205.hris.ws.shared.dto.EmploymentStatus;
import com.dice205.hris.ws.shared.dto.WorkingStatus;

public class UserDetailsRequestModel {
	private String supervisorId;
	private String email;
	private EmploymentStatus employmentStatus;
	private WorkingStatus workingStatus;	
	private Collection<String> roles;
	private String password;
	private Time shift;
	private ProfileReqModel profile;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	public Time getShift() {
		return shift;
	}

	public void setShift(Time shift) {
		this.shift = shift;
	}

	public ProfileReqModel getProfile() {
		return profile;
	}

	public void setProfile(ProfileReqModel profile) {
		this.profile = profile;
	}

	public Collection<String> getRoles() {
		return roles;
	}

	public void setRoles(Collection<String> roles) {
		this.roles = roles;
	}

	public WorkingStatus getWorkingStatus() {
		return workingStatus;
	}

	public void setWorkingStatus(WorkingStatus workingStatus) {
		this.workingStatus = workingStatus;
	}

	public EmploymentStatus getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(EmploymentStatus employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
}
