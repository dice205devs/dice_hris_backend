package com.dice205.hris.ws.ui.model.response;

public class EmailConfirmationRest {
	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
