package com.dice205.hris.ws.ui.model.query;

public class UsersQueryModel {
	private String[] userIds;

	public String[] getUserIds() {
		return userIds;
	}

	public void setUserIds(String[] userIds) {
		this.userIds = userIds;
	}
}
