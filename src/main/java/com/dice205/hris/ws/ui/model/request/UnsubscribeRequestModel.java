package com.dice205.hris.ws.ui.model.request;

public class UnsubscribeRequestModel {
	private String endpoint;
	private boolean isUnsubscribed;

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public boolean isUnsubscribed() {
		return isUnsubscribed;
	}

	public void setUnsubscribed(boolean isUnsubscribed) {
		this.isUnsubscribed = isUnsubscribed;
	}
}
