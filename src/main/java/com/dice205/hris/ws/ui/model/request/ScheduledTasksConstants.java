package com.dice205.hris.ws.ui.model.request;

public class ScheduledTasksConstants {
	public static final String OT = "OT"; 
	public static final String HOLIDAY = "HOLIDAY"; 
	public static final String MEETING = "MEETING";
}
