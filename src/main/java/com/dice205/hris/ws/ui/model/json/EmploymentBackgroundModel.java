package com.dice205.hris.ws.ui.model.json;

import java.sql.Date;

import com.dice205.hris.ws.shared.Utils;

public class EmploymentBackgroundModel {
	private String nameOfCompany = "";
	private String positionHeld = "";
	private Date dateEmployed = Utils.generateDate();
	private int lenghtOfStay = 0;
	private int startingPayRate = 0;
	private int endingPayRate = 0;

	public String getNameOfCompany() {
		return nameOfCompany;
	}

	public void setNameOfCompany(String nameOfCompany) {
		this.nameOfCompany = nameOfCompany;
	}

	public String getPositionHeld() {
		return positionHeld;
	}

	public void setPositionHeld(String positionHeld) {
		this.positionHeld = positionHeld;
	}

	public Date getDateEmployed() {
		return dateEmployed;
	}

	public void setDateEmployed(Date dateEmployed) {
		this.dateEmployed = dateEmployed;
	}

	public int getLenghtOfStay() {
		return lenghtOfStay;
	}

	public void setLenghtOfStay(int lenghtOfStay) {
		this.lenghtOfStay = lenghtOfStay;
	}

	public int getStartingPayRate() {
		return startingPayRate;
	}

	public void setStartingPayRate(int startingPayRate) {
		this.startingPayRate = startingPayRate;
	}

	public int getEndingPayRate() {
		return endingPayRate;
	}

	public void setEndingPayRate(int endingPayRate) {
		this.endingPayRate = endingPayRate;
	}
}
