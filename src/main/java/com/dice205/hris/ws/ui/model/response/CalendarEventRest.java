package com.dice205.hris.ws.ui.model.response;

import java.sql.Date;
import java.sql.Time;

import com.dice205.hris.ws.shared.dto.HolidayTypes;

public class CalendarEventRest {
	private long id;
	private String eventName;
	private Date eventDate;
	private HolidayTypes holidayType;
	private Time eventTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	public HolidayTypes getHolidayType() {
		return holidayType;
	}

	public void setHolidayType(HolidayTypes holidayType) {
		this.holidayType = holidayType;
	}

}
