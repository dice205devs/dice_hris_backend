package com.dice205.hris.ws.ui.model.json;

public class JwtPayloadModel {
	private String sub;
	private long exp;

	public long getExp() {
		return exp;
	}

	public void setExp(long exp) {
		this.exp = exp;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}
}
