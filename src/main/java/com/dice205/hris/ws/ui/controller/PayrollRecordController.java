package com.dice205.hris.ws.ui.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dice205.hris.ws.service.PayrollRecordService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.PayrollRecordDto;
import com.dice205.hris.ws.ui.model.query.PayrollRecordQueryModel;
import com.dice205.hris.ws.ui.model.request.PayrollRecordRequestModel;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;
import com.dice205.hris.ws.ui.model.response.OperationStatusModel;
import com.dice205.hris.ws.ui.model.response.PayrollRecordRest;
import com.dice205.hris.ws.ui.model.response.RequestOperationStatus;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

@RestController
@RequestMapping("payroll-record")
public class PayrollRecordController {
	@Autowired
	PayrollRecordService payrollRecordService;

	@Autowired
	Utils utils;

	@PostMapping(path = "/{userId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public PayrollRecordRest createPayrollRecord(@PathVariable(value = "userId") String userId,
			@RequestBody PayrollRecordRequestModel payrollRecordDetails) {
		PayrollRecordRest returnValue = new PayrollRecordRest();

		if (payrollRecordDetails.getEmployeeName().isEmpty())
			throw new RuntimeException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		PayrollRecordDto payrollRecordDto = new PayrollRecordDto();

		BeanUtils.copyProperties(payrollRecordDetails, payrollRecordDto);

		PayrollRecordDto createdPayrollRecordDto = payrollRecordService.createPayrollRecord(userId, payrollRecordDto);

		BeanUtils.copyProperties(createdPayrollRecordDto, returnValue);
		returnValue.setUserId(createdPayrollRecordDto.getUser().getUserId());

		return returnValue;
	}

	@PostAuthorize("hasRole('ADMIN') or returnObject[0].userId == principal.userId")
	@PostMapping(path = "/query/{userId}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public List<PayrollRecordRest> getAllPayrollRecordsByUser(@PathVariable(value = "userId") String userId,
			@RequestBody PayrollRecordQueryModel payrollQuery,
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "10") int limit) {
		List<PayrollRecordRest> returnValue = new ArrayList<>();

		List<PayrollRecordDto> payrollRecordDtoList = payrollRecordService.getAllPayrollRecords(userId, payrollQuery.getYear(), payrollQuery.getMonth(),
				payrollQuery.getCutOffPeriod(), page, limit);

		for (PayrollRecordDto payrollRecordDto : payrollRecordDtoList) {
			PayrollRecordRest payrollRecordRest = new PayrollRecordRest();

			BeanUtils.copyProperties(payrollRecordDto, payrollRecordRest);
			payrollRecordRest.setUserId(payrollRecordDto.getUser().getUserId());
			returnValue.add(payrollRecordRest);
		}

		return returnValue;
	}
	
	@Secured("ROLE_ADMIN")
	@PutMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public PayrollRecordRest updatePayRollRecord(@PathVariable(value = "id") long id,
			@RequestBody PayrollRecordRequestModel payrollRecordDetails) {
		PayrollRecordRest returnValue = new PayrollRecordRest();

		if (payrollRecordDetails.getEmployeeName().isEmpty())
			throw new RuntimeException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		PayrollRecordDto payrollRecordDto = new PayrollRecordDto();

		BeanUtils.copyProperties(payrollRecordDetails, payrollRecordDto);

		PayrollRecordDto updatedPayrollRecordDto = payrollRecordService.updatePayrollRecord(id, payrollRecordDto);

		BeanUtils.copyProperties(updatedPayrollRecordDto, returnValue);
		returnValue.setUserId(updatedPayrollRecordDto.getUser().getUserId());

		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@PutMapping(path = "/remove/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deletePayrollRecord(@PathVariable(value = "id") long id) {
		OperationStatusModel returnValue = new OperationStatusModel();

		boolean deletedRecord = payrollRecordService.deletePayrollRecord(id);

		returnValue.setOperationName(RequestOperationName.DELETE_PAYROLL_RECORD.name());

		if (deletedRecord) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		} else {
			returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
		}

		return returnValue;
	}

	@PostMapping(path = "/export-csv/{userId}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public void exportPayrollCSV(HttpServletResponse response, @PathVariable(value = "userId") String userId,
			@RequestBody PayrollRecordQueryModel payrollQuery) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
		List<PayrollRecordRest> payrollRecords = this.getAllPayrollRecordsByUser(userId, payrollQuery, 0, 999);

		// set file name and content type
		String filename = "hris_payroll_record.csv";

		response.setContentType("text/csv");
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"");

		StatefulBeanToCsv<PayrollRecordRest> writer = new StatefulBeanToCsvBuilder<PayrollRecordRest>(
				response.getWriter()).withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).build();

		writer.write(payrollRecords);
	}
}
