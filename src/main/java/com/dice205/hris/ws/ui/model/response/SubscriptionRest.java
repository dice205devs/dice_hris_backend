package com.dice205.hris.ws.ui.model.response;

public class SubscriptionRest {
	private String subscriptionId;
	private String endpoint;
	private KeysRest keys;
	private String userId;
	
	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public KeysRest getKeys() {
		return keys;
	}

	public void setKeys(KeysRest keys) {
		this.keys = keys;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
