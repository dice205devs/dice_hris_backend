package com.dice205.hris.ws.ui.model.json;

public class GovernmentInformationModel {
	private String taxStatus = "";
	private String rdo = "";
	private String tin = "";
	private String sss = "";
	private String hdmf = "";
	private String philhealth = "";

	public String getTaxStatus() {
		return taxStatus;
	}

	public void setTaxStatus(String taxStatus) {
		this.taxStatus = taxStatus;
	}

	public String getRdo() {
		return rdo;
	}

	public void setRdo(String rdo) {
		this.rdo = rdo;
	}

	public String getTin() {
		return tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public String getSss() {
		return sss;
	}

	public void setSss(String sss) {
		this.sss = sss;
	}

	public String getHdmf() {
		return hdmf;
	}

	public void setHdmf(String hdmf) {
		this.hdmf = hdmf;
	}

	public String getPhilhealth() {
		return philhealth;
	}

	public void setPhilhealth(String philhealth) {
		this.philhealth = philhealth;
	}
}
