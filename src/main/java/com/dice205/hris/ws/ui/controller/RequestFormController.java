package com.dice205.hris.ws.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dice205.hris.ws.service.RequestFormService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.RequestFormDto;
import com.dice205.hris.ws.shared.dto.RequestTypeDto;
import com.dice205.hris.ws.ui.model.request.RequestFormRequestModel;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;
import com.dice205.hris.ws.ui.model.response.RequestFormRest;
import com.dice205.hris.ws.ui.model.response.RequestTypeRest;

@RestController
@RequestMapping("request-form")
public class RequestFormController {
	@Autowired
	RequestFormService requestFormService;

	@Autowired
	Utils utils;

	@PreAuthorize("hasRole('ADMIN') or #userId == principal.userId")
	@GetMapping(path = "/{userId}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<RequestFormRest> getRequestForms(@PathVariable String userId,
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "10") int limit) {
		List<RequestFormRest> returnValue = new ArrayList<>();

		List<RequestFormDto> requestFormDtoList = requestFormService.getRequestForms(userId, page, limit);

		for (RequestFormDto requestFormDto : requestFormDtoList) {
			RequestFormRest requestFormRest = new RequestFormRest();

			BeanUtils.copyProperties(requestFormDto, requestFormRest);

			if (requestFormDto.getRequestStatus() != null) {

				if (requestFormDto.getRequestStatus()) {
					requestFormRest.setRequestStatus("APPROVED");
				}

				if (!requestFormDto.getRequestStatus()) {
					requestFormRest.setRequestStatus("DISAPPROVED");
				}
			}

			returnValue.add(requestFormRest);
		}

		return returnValue;
	}

	@PostMapping(path = "/{userId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public RequestFormRest createRequestForm(@PathVariable String userId,
			@RequestBody RequestFormRequestModel requestFormDetails) {
		RequestFormRest returnValue = new RequestFormRest();

		if (requestFormDetails.getRequestType()== null)
			throw new RuntimeException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		RequestFormDto requestFormDto = new RequestFormDto();

		BeanUtils.copyProperties(requestFormDetails, requestFormDto);

		RequestFormDto storedRequestFormDto = requestFormService.createRequestForm(userId, requestFormDto);

		BeanUtils.copyProperties(storedRequestFormDto, returnValue);

		return returnValue;
	}

	@PreAuthorize("hasRole('ADMIN') or hasRole('APPROVER')")
	@PutMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public RequestFormRest updateRequestForm(@PathVariable long id,
			@RequestBody RequestFormRequestModel requestFormDetails) {
		RequestFormRest returnValue = new RequestFormRest();

		if (requestFormDetails.getRequestStatus() == null)
			throw new RuntimeException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		RequestFormDto requestFormDto = new RequestFormDto();

		BeanUtils.copyProperties(requestFormDetails, requestFormDto);

		RequestFormDto updatedRequestFormDto = requestFormService.updateRequestForm(id, requestFormDto);

		BeanUtils.copyProperties(updatedRequestFormDto, returnValue);

		if (updatedRequestFormDto.getRequestStatus() != null) {
			
			if (updatedRequestFormDto.getRequestStatus()) {
				returnValue.setRequestStatus("APPROVED");
			}
			
			if (!updatedRequestFormDto.getRequestStatus()) {
				returnValue.setRequestStatus("DISAPPROVED");
			}
		}

		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@GetMapping(path = "/all", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<RequestFormRest> getAllRequestForms(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "10") int limit) {
		List<RequestFormRest> returnValue = new ArrayList<>();

		List<RequestFormDto> requestFormDtoList = requestFormService.getAllRequestForms(page, limit);

		for (RequestFormDto requestFormDto : requestFormDtoList) {
			RequestFormRest requestFormRest = new RequestFormRest();

			BeanUtils.copyProperties(requestFormDto, requestFormRest);

			if (requestFormDto.getRequestStatus() != null) {

				if (requestFormDto.getRequestStatus()) {
					requestFormRest.setRequestStatus("APPROVED");
				}

				if (!requestFormDto.getRequestStatus()) {
					requestFormRest.setRequestStatus("DISAPPROVED");
				}
			}

			returnValue.add(requestFormRest);
		}

		return returnValue;
	}

	@PreAuthorize("hasRole('ADMIN') or #supervisorId == principal.userId or hasRole('APPROVER')")
	@GetMapping(path = "/supervisor/{supervisorId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public List<RequestFormRest> getAllRequestFormsBySupervisor(@PathVariable String supervisorId,
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "10") int limit) {
		List<RequestFormRest> returnValue = new ArrayList<>();

		List<RequestFormDto> requestFormDtoList = requestFormService.getAllBySupervisor(supervisorId, page, limit);

		for (RequestFormDto requestFormDto : requestFormDtoList) {
			RequestFormRest requestFormRest = new RequestFormRest();

			BeanUtils.copyProperties(requestFormDto, requestFormRest);

			if (requestFormDto.getRequestStatus() != null) {

				if (requestFormDto.getRequestStatus()) {
					requestFormRest.setRequestStatus("APPROVED");
				}

				if (!requestFormDto.getRequestStatus()) {
					requestFormRest.setRequestStatus("DISAPPROVED");
				}
			}

			returnValue.add(requestFormRest);
		}

		return returnValue;
	}
	
	@GetMapping(path = "/request-types", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public List<RequestTypeRest> getRequestTypes() {
		List<RequestTypeRest> returnValue = new ArrayList<>();
		
		List<RequestTypeDto> requestTypeDtoList = requestFormService.getRequestTypes();
		
		for (RequestTypeDto requestTypeDto: requestTypeDtoList) {
			RequestTypeRest requestTypeRest = new RequestTypeRest();
			
			BeanUtils.copyProperties(requestTypeDto, requestTypeRest);
			
			returnValue.add(requestTypeRest);
		}
		
		return returnValue;
		
	}
}
