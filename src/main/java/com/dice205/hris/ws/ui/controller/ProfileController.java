package com.dice205.hris.ws.ui.controller;

import com.dice205.hris.ws.service.impl.ProfileServiceImpl;
import com.dice205.hris.ws.shared.CustomJSONObject;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.ProfileDto;

import com.dice205.hris.ws.ui.model.request.ProfileReqModel;
import com.dice205.hris.ws.ui.model.response.ProfileRest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("profile")
public class ProfileController {
	@Autowired
	ProfileServiceImpl profileService;

	@Autowired
	Utils utils;

	@PostAuthorize("hasRole('ADMIN') or returnObject.employeeId == principal.userId")
	@GetMapping(path = "/{userId}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ProfileRest getProfile(@PathVariable String userId)
			throws JsonMappingException, JsonProcessingException, ClassNotFoundException {
		ProfileRest returnValue = new ProfileRest();

		ProfileDto profileDto = profileService.getProfile(userId);

		BeanUtils.copyProperties(profileDto, returnValue);

		returnValue.setPersonalInformation(
				utils.jsonToJavaObject(profileDto.getPersonalInformation(), "PersonalInformationModel"));
		returnValue.setEducationalBackground(
				utils.jsonToJavaObject(profileDto.getEducationalBackground(), "EducationalBackgroundModel"));
		returnValue.setContactInformation(
				utils.jsonToJavaObject(profileDto.getContactInformation(), "ContactInformationModel"));
		returnValue.setGovernmentInformation(
				utils.jsonToJavaObject(profileDto.getGovernmentInformation(), "GovernmentInformationModel"));
		returnValue.setEmploymentBackground(
				utils.jsonToJavaObject(profileDto.getEmploymentBackground(), "EmploymentBackgroundModel"));
		returnValue.setWorkDetails(utils.jsonToJavaObject(profileDto.getWorkDetails(), "WorkDetailsModel"));

		return returnValue;
	}

	@PreAuthorize("hasRole('ADMIN') or #userId == principal.userId")
	@PutMapping(path = "/{userId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public ProfileRest updateProfile(@RequestBody ProfileReqModel profile, @PathVariable String userId)
			throws JsonMappingException, ClassNotFoundException, JsonProcessingException {
		ProfileRest returnValue = new ProfileRest();
		ProfileDto profileDto = new ProfileDto();

		BeanUtils.copyProperties(profile, profileDto);

		profileDto.setPersonalInformation(new CustomJSONObject(profile.getPersonalInformation()));
		profileDto.setEducationalBackground(new CustomJSONObject(profile.getEducationalBackground()));
		profileDto.setContactInformation(new CustomJSONObject(profile.getContactInformation()));
		profileDto.setGovernmentInformation(new CustomJSONObject(profile.getGovernmentInformation()));
		profileDto.setEmploymentBackground(new CustomJSONObject(profile.getEmploymentBackground()));
		profileDto.setWorkDetails(new CustomJSONObject(profile.getWorkDetails()));

		ProfileDto updatedProfileDto = profileService.updateProfile(profileDto, userId);

		BeanUtils.copyProperties(updatedProfileDto, returnValue);

		returnValue.setPersonalInformation(
				utils.jsonToJavaObject(updatedProfileDto.getPersonalInformation(), "PersonalInformationModel"));
		returnValue.setEducationalBackground(
				utils.jsonToJavaObject(updatedProfileDto.getEducationalBackground(), "EducationalBackgroundModel"));
		returnValue.setContactInformation(
				utils.jsonToJavaObject(updatedProfileDto.getContactInformation(), "ContactInformationModel"));
		returnValue.setGovernmentInformation(
				utils.jsonToJavaObject(updatedProfileDto.getGovernmentInformation(), "GovernmentInformationModel"));
		returnValue.setEmploymentBackground(
				utils.jsonToJavaObject(updatedProfileDto.getEmploymentBackground(), "EmploymentBackgroundModel"));
		returnValue.setWorkDetails(utils.jsonToJavaObject(updatedProfileDto.getWorkDetails(), "WorkDetailsModel"));

		return returnValue;
	}
}