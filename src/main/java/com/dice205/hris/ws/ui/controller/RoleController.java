package com.dice205.hris.ws.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dice205.hris.ws.service.RoleService;
import com.dice205.hris.ws.shared.dto.RoleDto;
import com.dice205.hris.ws.ui.model.response.RoleRest;

@RestController
@RequestMapping("role-list")

public class RoleController {

	@Autowired
	RoleService userRoleService;

	@GetMapping(path = "/{roleId}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public RoleRest getUserRole(@PathVariable long roleId) {
		RoleRest returnValue = new RoleRest();

		RoleDto userDto = userRoleService.getUserRole(roleId);
		BeanUtils.copyProperties(userDto, returnValue);

		return returnValue;
	}

	@GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<RoleRest> getAllUserRoles() {
		List<RoleRest> returnValue = new ArrayList<>();
		List<RoleDto> userRoleDtos = userRoleService.getAllUserRoles();

		for (int i = 0; i < userRoleDtos.size(); i++) {
			RoleRest userRoleRest = new RoleRest();

			BeanUtils.copyProperties(userRoleDtos.get(i), userRoleRest);

			returnValue.add(userRoleRest);
		}

		return returnValue;
	}
}
