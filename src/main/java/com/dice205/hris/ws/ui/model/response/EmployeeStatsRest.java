package com.dice205.hris.ws.ui.model.response;

public class EmployeeStatsRest {
	private int workingDays;
	private int daysWorked;
	private int absences;
	private int lates;
	private double punctualityRate;
	private int leavesAvailable;
	private int leavesUsed;
	private int sickLeavesAvailable;
	private int sickLeavesUsed;

	public int getWorkingDays() {
		return workingDays;
	}

	public void setWorkingDays(int workingDays) {
		this.workingDays = workingDays;
	}

	public int getDaysWorked() {
		return daysWorked;
	}

	public void setDaysWorked(int daysWorked) {
		this.daysWorked = daysWorked;
	}

	public int getAbsences() {
		return absences;
	}

	public void setAbsences(int absences) {
		this.absences = absences;
	}

	public int getLates() {
		return lates;
	}

	public void setLates(int lates) {
		this.lates = lates;
	}

	public double getPunctualityRate() {
		return punctualityRate;
	}

	public void setPunctualityRate(double punctualityRate) {
		this.punctualityRate = punctualityRate;
	}

	public int getLeavesAvailable() {
		return leavesAvailable;
	}

	public void setLeavesAvailable(int leavesAvailable) {
		this.leavesAvailable = leavesAvailable;
	}

	public int getLeavesUsed() {
		return leavesUsed;
	}

	public void setLeavesUsed(int leavesUsed) {
		this.leavesUsed = leavesUsed;
	}

	public int getSickLeavesAvailable() {
		return sickLeavesAvailable;
	}

	public void setSickLeavesAvailable(int sickLeavesAvailable) {
		this.sickLeavesAvailable = sickLeavesAvailable;
	}

	public int getSickLeavesUsed() {
		return sickLeavesUsed;
	}

	public void setSickLeavesUsed(int sickLeavesUsed) {
		this.sickLeavesUsed = sickLeavesUsed;
	}

}
