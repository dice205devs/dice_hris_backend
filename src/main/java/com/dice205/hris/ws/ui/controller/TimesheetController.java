package com.dice205.hris.ws.ui.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dice205.hris.ws.service.TimesheetService;
import com.dice205.hris.ws.shared.CustomJSONObject;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.TimesheetDto;
import com.dice205.hris.ws.ui.model.query.TimesheetQueryModel;
import com.dice205.hris.ws.ui.model.request.TimesheetRequestModel;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;
import com.dice205.hris.ws.ui.model.response.TimesheetRest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
@RequestMapping("timesheet")
public class TimesheetController {
	@Autowired
	TimesheetService timesheetService;

	@Autowired
	Utils utils;

	@PostMapping(path = "/time-in/{userId}")
	public TimesheetRest timeIn(@PathVariable(value = "userId") String userId,
			@RequestBody TimesheetRequestModel timesheetDetails) throws JsonMappingException, ClassNotFoundException, JsonProcessingException {
		TimesheetRest returnValue = new TimesheetRest();

		if (timesheetDetails.getTimesheetDate().toString().isEmpty())
			throw new RuntimeException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

		TimesheetDto timesheetDto = new TimesheetDto();

		BeanUtils.copyProperties(timesheetDetails, timesheetDto);
		
		timesheetDto.setOfficialBusiness(new CustomJSONObject(timesheetDetails.getOfficialBusiness()));

		TimesheetDto storedTimesheetDto = timesheetService.timeIn(userId, timesheetDto);

		if (storedTimesheetDto == null)
			throw new RuntimeException(ErrorMessages.COULD_NOT_CREATE_RECORD.getErrorMessage());

		BeanUtils.copyProperties(storedTimesheetDto, returnValue);
		
		returnValue.setOfficialBusiness(utils.jsonToJavaObject(storedTimesheetDto.getOfficialBusiness(), "OfficialBusinessModel"));

		return returnValue;
	}

	@PostMapping(path = "/time-out/{userId}")
	public TimesheetRest timeOut(@PathVariable(value = "userId") String userId,
			@RequestBody TimesheetRequestModel timesheetDetails) throws ParseException, JsonMappingException, ClassNotFoundException, JsonProcessingException {
		TimesheetRest returnValue = new TimesheetRest();

		if (timesheetDetails.getTimesheetDate().toString().isEmpty())
			throw new RuntimeException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		TimesheetDto timesheetDto = new TimesheetDto();

		BeanUtils.copyProperties(timesheetDetails, timesheetDto);
		
		timesheetDto.setOfficialBusiness(new CustomJSONObject(timesheetDetails.getOfficialBusiness()));

		TimesheetDto storedTimesheetDto = timesheetService.timeOut(userId, timesheetDto);

		if (storedTimesheetDto == null)
			throw new RuntimeException(ErrorMessages.COULD_NOT_CREATE_RECORD.getErrorMessage());
		
		BeanUtils.copyProperties(storedTimesheetDto, returnValue);
		
		returnValue.setOfficialBusiness(utils.jsonToJavaObject(storedTimesheetDto.getOfficialBusiness(), "OfficialBusinessModel"));
		
		return returnValue;
	}

	@PostMapping(path = "/query/{userId}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<TimesheetRest> getTimesheetRecords(@PathVariable(value = "userId") String userId,
			@RequestBody TimesheetQueryModel timesheetQuery,
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "10") int limit) throws BeansException, ParseException, JsonMappingException, ClassNotFoundException, JsonProcessingException {
		List<TimesheetRest> returnValue = new ArrayList<>();

		for (TimesheetDto timesheetDto : timesheetService.getTimesheetRecord(userId, timesheetQuery.getDate(), page, limit)) {
			TimesheetRest timesheetRest = new TimesheetRest();

			BeanUtils.copyProperties(timesheetDto, timesheetRest);

			timesheetRest.setOfficialBusiness(utils.jsonToJavaObject(timesheetDto.getOfficialBusiness(), "OfficialBusinessModel"));
			
			returnValue.add(timesheetRest);
		}

		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@PostMapping(path = "/{userId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public TimesheetRest createTimesheetRecord(@PathVariable(value = "userId") String userId,
			@RequestBody TimesheetRequestModel timesheetDetails) throws JsonMappingException, ClassNotFoundException, JsonProcessingException {
		TimesheetRest returnValue = new TimesheetRest();
		TimesheetDto timesheetDto = new TimesheetDto();

		BeanUtils.copyProperties(timesheetDetails, timesheetDto);

		timesheetDto.setOfficialBusiness(new CustomJSONObject(timesheetDetails.getOfficialBusiness()));
		
		TimesheetDto storedTimesheetDto = timesheetService.createTimesheetRecord(userId, timesheetDto);

		BeanUtils.copyProperties(storedTimesheetDto, returnValue);
		
		returnValue.setOfficialBusiness(utils.jsonToJavaObject(storedTimesheetDto.getOfficialBusiness(), "OfficialBusinessModel"));

		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@PutMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public TimesheetRest updateTimesheetRecord(@PathVariable(value = "id") long id,
			@RequestBody TimesheetRequestModel timesheetDetails) throws JsonMappingException, ClassNotFoundException, JsonProcessingException {
		TimesheetRest returnValue = new TimesheetRest();
		TimesheetDto timesheetDto = new TimesheetDto();

		BeanUtils.copyProperties(timesheetDetails, timesheetDto);
		
		timesheetDto.setOfficialBusiness(new CustomJSONObject(timesheetDetails.getOfficialBusiness()));

		TimesheetDto storedTimesheetDto = timesheetService.updateTimesheetRecord(id, timesheetDto);

		BeanUtils.copyProperties(storedTimesheetDto, returnValue);

		returnValue.setOfficialBusiness(utils.jsonToJavaObject(storedTimesheetDto.getOfficialBusiness(), "OfficialBusinessModel"));
		
		return returnValue;
	}
}
