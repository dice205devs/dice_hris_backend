package com.dice205.hris.ws.ui.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dice205.hris.ws.exceptions.UserServiceException;
import com.dice205.hris.ws.security.SecurityConstants;
import com.dice205.hris.ws.service.EmployeeStatsService;
import com.dice205.hris.ws.service.ProfileService;
import com.dice205.hris.ws.service.RoleService;
import com.dice205.hris.ws.service.UserService;
import com.dice205.hris.ws.shared.CustomJSONObject;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.EmployeeStatsDto;
import com.dice205.hris.ws.shared.dto.ProfileDto;
import com.dice205.hris.ws.shared.dto.UserDto;
import com.dice205.hris.ws.ui.model.query.EmployeeStatsQueryModel;
import com.dice205.hris.ws.ui.model.request.ChangePasswordRequestModel;
import com.dice205.hris.ws.ui.model.request.PasswordResetModel;
import com.dice205.hris.ws.ui.model.request.PasswordResetRequestModel;
import com.dice205.hris.ws.ui.model.request.ProfileReqModel;
import com.dice205.hris.ws.ui.model.request.UserDetailsRequestModel;
import com.dice205.hris.ws.ui.model.response.CurrentUserRest;
import com.dice205.hris.ws.ui.model.response.EmailConfirmationRest;
import com.dice205.hris.ws.ui.model.response.EmployeeStatsRest;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;
import com.dice205.hris.ws.ui.model.response.OperationStatusModel;
import com.dice205.hris.ws.ui.model.response.RequestOperationStatus;
import com.dice205.hris.ws.ui.model.response.UserRest;

@RestController
@RequestMapping("users")
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	RoleService userRoleService;

	@Autowired
	ProfileService profileService;
	
	@Autowired
	EmployeeStatsService employeeStatsService;
	
	@Autowired
	Utils utils;

	@PreAuthorize("hasRole('ADMIN') or #userId == principal.userId")
	@GetMapping(path = "/{userId}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public UserRest getUser(@PathVariable String userId) {
		UserRest returnValue = new UserRest();

		UserDto userDto = userService.getUserByUserId(userId);

		BeanUtils.copyProperties(userDto, returnValue);
		
		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public UserRest createUser(@RequestBody UserDetailsRequestModel userDetails) throws Exception {
		UserRest returnValue = new UserRest(); // Create new instance;

		Boolean isFieldEmpty = userDetails.getEmail().isEmpty() || userDetails.getPassword().isEmpty()
				|| userDetails.getSupervisorId().isEmpty() || userDetails.getShift().toString().isEmpty();

		if (isFieldEmpty)
			throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userDetails, userDto);

		UserDto createdUser = userService.createUser(userDto);
		
        // =============== PROFILE CREATION ===============
		
		ProfileDto profileDto = new ProfileDto();
		BeanUtils.copyProperties(userDetails.getProfile(), profileDto);
		
		ProfileReqModel profile = userDetails.getProfile();
		
		CustomJSONObject personalInfoJson = new CustomJSONObject(profile.getPersonalInformation());
		CustomJSONObject educBgJson = new CustomJSONObject(profile.getEducationalBackground());
		CustomJSONObject contactInfoJson = new CustomJSONObject(profile.getContactInformation());
		CustomJSONObject govInfoJson = new CustomJSONObject(profile.getGovernmentInformation());
		CustomJSONObject employBgJson = new CustomJSONObject(profile.getEmploymentBackground());
		CustomJSONObject workDetailsJson = new CustomJSONObject(profile.getWorkDetails());

		profileDto.setPersonalInformation(personalInfoJson);
		profileDto.setEducationalBackground(educBgJson);
		profileDto.setContactInformation(contactInfoJson);
		profileDto.setGovernmentInformation(govInfoJson);
		profileDto.setEmploymentBackground(employBgJson);
		profileDto.setWorkDetails(workDetailsJson);
		profileDto.setEmployeeId(createdUser.getUserId());

		profileService.createProfile(profileDto);
		
		// =================================================
		
		BeanUtils.copyProperties(createdUser, returnValue);

		return returnValue;

	}

	@PreAuthorize("hasRole('ADMIN') or #userId == principal.userId")
	@PutMapping(path = "/{userId}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public UserRest updateUser(@PathVariable(value = "userId") String userId,
			@RequestBody UserDetailsRequestModel userDetails) {
		UserRest returnValue = new UserRest(); // Create new instance;

		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userDetails, userDto);

		UserDto updatedUser = userService.updateUser(userId, userDto);

		BeanUtils.copyProperties(updatedUser, returnValue);

		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@PutMapping(path = "/remove/{userId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deleteUser(@PathVariable(value = "userId") String userId) {
		OperationStatusModel returnValue = new OperationStatusModel();

		boolean deleteUser = userService.softDeleteUser(userId);
		profileService.softDeleteProfile(userId);

		returnValue.setOperationName(RequestOperationName.DELETE.name());

		if (deleteUser) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		} else {
			returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
		}

		return returnValue;
	}

	@Secured("ROLE_ADMIN")
	@GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<UserRest> getAllUsers(@RequestParam(value="page", defaultValue="0") int page, @RequestParam(value="limit", defaultValue="20") int limit) {
		List<UserRest> returnValue = new ArrayList<>();
		List<UserDto> userDtos = userService.getAllUsers(page, limit);

		for (int i = 0; i < userDtos.size(); i++) {
			UserRest userRest = new UserRest();

			BeanUtils.copyProperties(userDtos.get(i), userRest);

			returnValue.add(userRest);
		}
		
		return returnValue;
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping(path = "/role", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<UserRest> getAllUsersByRole(@RequestParam(value="page", defaultValue="0") int page, @RequestParam(value="limit", defaultValue="20") int limit, @RequestParam(value = "roleName", required = true) String roleName) {
		List<UserRest> returnValue = new ArrayList<>();
		List<UserDto> userDtos = userService.getAllUsersByRole(roleName, page, limit);

		for (int i = 0; i < userDtos.size(); i++) {
			UserRest userRest = new UserRest();

			BeanUtils.copyProperties(userDtos.get(i), userRest);

			returnValue.add(userRest);
		}
		
		return returnValue;
	}

	@GetMapping(path = "/email-verification", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public EmailConfirmationRest verifyEmailToken(@RequestParam(value = "token") String token) {

		EmailConfirmationRest returnValue = new EmailConfirmationRest();

		String result = userService.verifyEmailToken(token);

		if (result == null) throw new UserServiceException(ErrorMessages.TOKEN_EXPIRED.getErrorMessage());
		
		returnValue.setUserId(result);

		return returnValue;
	}

	@PostMapping(path = "/password-reset-request", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public OperationStatusModel requestReset(@RequestBody PasswordResetRequestModel passwordResetRequestModel) {
		OperationStatusModel returnValue = new OperationStatusModel();

		boolean operationResult = userService.requestPasswordReset(passwordResetRequestModel.getEmail());

		returnValue.setOperationName(RequestOperationName.REQUEST_PASSWORD_RESET.name());
		returnValue.setOperationResult(RequestOperationStatus.ERROR.name());

		if (operationResult) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		}

		return returnValue;
	}

	@PostMapping(path = "/password-reset", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public OperationStatusModel resetPassword(@RequestBody PasswordResetModel passwordResetModel) {
		OperationStatusModel returnValue = new OperationStatusModel();

		boolean operationResult = userService.resetPassword(passwordResetModel.getToken(),
				passwordResetModel.getPassword());

		returnValue.setOperationName(RequestOperationName.PASSWORD_RESET.name());
		returnValue.setOperationResult(RequestOperationStatus.ERROR.name());

		if (operationResult) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		}

		return returnValue;
	}

	@GetMapping(path = "/get-current", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public CurrentUserRest getCurrent() {
		return utils.getAuth();
	}

	@Secured("ROLE_ADMIN")
	@GetMapping(path = "/archive", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<UserRest> getArchives(@RequestParam(value="page", defaultValue="0") int page, @RequestParam(value="limit", defaultValue="20") int limit) {
		List<UserRest> returnValue = new ArrayList<>();
		List<UserDto> userDtos = userService.getArchive(page, limit);

		for (int i = 0; i < userDtos.size(); i++) {
			UserRest userRest = new UserRest();

			BeanUtils.copyProperties(userDtos.get(i), userRest);

			returnValue.add(userRest);
		}

		return returnValue;
	}
	
	@PostMapping(path="/logout")
	public OperationStatusModel logout(HttpServletRequest request) {
		OperationStatusModel returnValue = new OperationStatusModel();
		boolean operationResult = userService.logout(request.getHeader(SecurityConstants.HEADER_STRING));
				
		returnValue.setOperationName(RequestOperationName.LOGOUT.name());
		returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
		
		if (operationResult) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		}
		
		return returnValue;
	}
	
	@PostMapping(path = "/change-password", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public OperationStatusModel changePassword(@RequestBody ChangePasswordRequestModel password) {
		OperationStatusModel returnValue = new OperationStatusModel();

		if (password.getPassword().isEmpty() || password.getNewPassword().isEmpty())
			throw new NullPointerException("The object is null");

		boolean operationResult = userService.changePassword(password.getPassword(), password.getNewPassword());

		returnValue.setOperationName(RequestOperationName.CHANGE_PASSWORD.name());
		returnValue.setOperationResult(RequestOperationStatus.ERROR.name());

		if (operationResult) {
			returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		}

		return returnValue;
	}
	
	@PostMapping(path = "/my-records/{userId}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE },  consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public EmployeeStatsRest getEmployeeStats(@PathVariable String userId, @RequestBody EmployeeStatsQueryModel employeeStatsQuery) {
		EmployeeStatsRest returnValue = new EmployeeStatsRest();
		
		EmployeeStatsDto employeeStatsDto = employeeStatsService.getEmployeeStats(userId, employeeStatsQuery.getDate());
		
		BeanUtils.copyProperties(employeeStatsDto, returnValue);
		
		return returnValue;
	}

}
