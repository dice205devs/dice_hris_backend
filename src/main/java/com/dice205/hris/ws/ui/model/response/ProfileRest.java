package com.dice205.hris.ws.ui.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ProfileRest {

	private String employeeId;
	private Object personalInformation;
    private Object educationalBackground;
    private Object contactInformation;
	private Object governmentInformation;
	private Object employmentBackground;
	private Object workDetails;
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public Object getPersonalInformation() {
		return personalInformation;
	}
	public void setPersonalInformation(Object personalInformation) {
		this.personalInformation = personalInformation;
	}
	public Object getEducationalBackground() {
		return educationalBackground;
	}
	public void setEducationalBackground(Object educationalBackground) {
		this.educationalBackground = educationalBackground;
	}
	public Object getContactInformation() {
		return contactInformation;
	}
	public void setContactInformation(Object contactInformation) {
		this.contactInformation = contactInformation;
	}
	public Object getGovernmentInformation() {
		return governmentInformation;
	}
	public void setGovernmentInformation(Object governmentInformation) {
		this.governmentInformation = governmentInformation;
	}
	public Object getEmploymentBackground() {
		return employmentBackground;
	}
	public void setEmploymentBackground(Object employmentBackground) {
		this.employmentBackground = employmentBackground;
	}
	public Object getWorkDetails() {
		return workDetails;
	}
	public void setWorkDetails(Object workDetails) {
		this.workDetails = workDetails;
	}
}