package com.dice205.hris.ws.security;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.RuleResult;
import org.passay.WhitespaceRule;

public class PasswordConstraintValidator {
	public List<String> validate(String password) {
        PasswordValidator validator = new PasswordValidator(Arrays.asList(
           new LengthRule(8, 30), 
           new WhitespaceRule(),
           new CharacterRule(EnglishCharacterData.UpperCase, 1),
           new CharacterRule(EnglishCharacterData.Digit, 1),
           new CharacterRule(EnglishCharacterData.Special, 1)
           ));
        
        List<String> returnValue = new ArrayList<>();
 
        RuleResult result = validator.validate(new PasswordData(password));
        if (result.isValid()) {
            return returnValue;
        }
        
        returnValue.addAll(validator.getMessages(result));
        
        return returnValue;
    }
}
