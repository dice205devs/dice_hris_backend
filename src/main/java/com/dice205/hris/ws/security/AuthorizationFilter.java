package com.dice205.hris.ws.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.dice205.hris.ws.io.entity.UserEntity;
import com.dice205.hris.ws.io.repositories.JwtBlacklistRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.ui.model.response.ErrorMessage;
import com.dice205.hris.ws.ui.model.response.ErrorMessages;

import io.jsonwebtoken.Jwts;

public class AuthorizationFilter extends BasicAuthenticationFilter {
	private final UserRepository userRepository;
	private final JwtBlacklistRepository jwtBlacklistRepository;

	public AuthorizationFilter(AuthenticationManager authManager, UserRepository userRepository, JwtBlacklistRepository jwtBlacklistRepository) {
		super(authManager);
		this.userRepository = userRepository;
		this.jwtBlacklistRepository = jwtBlacklistRepository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		String header = req.getHeader(SecurityConstants.HEADER_STRING);

		if (header == null || !header.startsWith(SecurityConstants.TOKEN_PREFIX)) {
			chain.doFilter(req, res);
			return;
		}

		String token = header.replace(SecurityConstants.TOKEN_PREFIX, "");
		boolean isExpired = Utils.hasTokenExpired(token) || jwtBlacklistRepository.findByJwt(token).isPresent();

		if (isExpired) {
			PrintWriter out = res.getWriter();

			JSONObject jsonObject = new JSONObject(
					new ErrorMessage(new Date(), ErrorMessages.TOKEN_EXPIRED.getErrorMessage()));
			res.setContentType("application/json");
			res.setCharacterEncoding("UTF-8");
			res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			out.print(jsonObject);
			out.flush();
			
			return;
		}

		UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(SecurityConstants.HEADER_STRING);

		if (token != null) {
			token = token.replace(SecurityConstants.TOKEN_PREFIX, "");

			String user = Jwts.parser().setSigningKey(SecurityConstants.getTokenSecret()).parseClaimsJws(token)
					.getBody().getSubject();

			if (user != null) {
				UserEntity userEntity = userRepository.findByEmail(user);

				if (userEntity == null)
					return null;

				UserPrincipal userPricipal = new UserPrincipal(userEntity);
				return new UsernamePasswordAuthenticationToken(userPricipal, null, userPricipal.getAuthorities());
			}

			return null;
		}

		return null;
	}
}
