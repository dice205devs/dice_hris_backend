package com.dice205.hris.ws.security;

import java.security.Security;
import java.util.Arrays;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.dice205.hris.ws.io.repositories.JwtBlacklistRepository;
import com.dice205.hris.ws.io.repositories.UserRepository;
import com.dice205.hris.ws.service.AuditLogService;
import com.dice205.hris.ws.service.UserService;
import com.dice205.hris.ws.shared.Utils;

@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {
	private final UserService userDetailsService;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final UserRepository userRepository;
	private final JwtBlacklistRepository jwtBlacklistRepository;
	private final Utils utils;
	private final AuditLogService auditLogService;
	
	public WebSecurity(UserService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder,
			UserRepository userRepository, JwtBlacklistRepository jwtBlacklistRepository, AuditLogService auditLogService, Utils utils) {
		this.userDetailsService = userDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.userRepository = userRepository;
		this.jwtBlacklistRepository = jwtBlacklistRepository;
		this.utils = utils;
		this.auditLogService = auditLogService;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		
		http.cors().and().csrf().disable().headers().frameOptions().deny().and().authorizeRequests()
//				.antMatchers(HttpMethod.POST, SecurityConstants.SIGN_UP_URL)
//				.hasRole("ADMIN")
				.antMatchers(HttpMethod.GET, SecurityConstants.EMAIL_VERIFICATION_URL).permitAll()
				.antMatchers(HttpMethod.POST, SecurityConstants.PASSWORD_RESET_URL).permitAll()
				.antMatchers(HttpMethod.POST, SecurityConstants.STORE_SUBSCRIPTION_URL).permitAll()
				.antMatchers(HttpMethod.POST, SecurityConstants.REMOVE_SUBSCRIPTION_URL).permitAll()
				.antMatchers(HttpMethod.POST, SecurityConstants.PASSWORD_RESET_REQUEST_URL).permitAll()
				.antMatchers(HttpMethod.GET, SecurityConstants.SWAGGER_UI_URL).permitAll()
				.antMatchers(HttpMethod.GET, "/v2/api-docs/**").permitAll()
				.antMatchers(HttpMethod.GET, "/swagger-resources/**").permitAll()
				.antMatchers(HttpMethod.GET, "/webjars/**").permitAll().anyRequest().authenticated().and()
				.addFilter(getAuthenticationFilter())
				.addFilterAfter(new CustomFilter(auditLogService, utils), AuthorizationFilter.class)
				.addFilter(new AuthorizationFilter(authenticationManager(), userRepository, jwtBlacklistRepository)).sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}

	public AuthenticationFilter getAuthenticationFilter() throws Exception {
		final AuthenticationFilter filter = new AuthenticationFilter(authenticationManager());
		filter.setFilterProcessesUrl("/users/login");
		return filter;
	}
	
	@Bean
	protected CorsConfigurationSource corsConfigurationSource() {
		final CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(false);
		config.setAllowedOrigins(Arrays.asList("*"));
		config.setAllowedHeaders(Arrays.asList("Authorization", "UserID", "Cache-Control", "Content-Type"));
		config.setAllowedMethods(Arrays.asList("*"));
		config.setExposedHeaders(Arrays.asList("Authorization", "UserID"));
		
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

		source.registerCorsConfiguration("/**", config);

		return source;
	}

}
