package com.dice205.hris.ws.security;

import com.dice205.hris.ws.SpringApplicationContext;

public class SecurityConstants {
	public static final long EXPIRATION_TIME = 86400000; // 1day
	public static final long PASSWORD_RESET_EXPIRATION_TIME = 86400000; // 1day
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String SIGN_UP_URL = "/users/{userId:\\d+}/register";
	public static final String EMAIL_VERIFICATION_URL = "/users/email-verification";
	public static final String STORE_SUBSCRIPTION_URL = "/web-push/subscribe";
	public static final String PASSWORD_RESET_URL = "/users/password-reset";
	public static final String PASSWORD_RESET_REQUEST_URL = "/users/password-reset-request";
//	public static final String  SERVER_URL = "http://localhost:8090";
	public static final String  SERVER_URL = "http://18.232.243.105:8090";
	public static final String SWAGGER_UI_URL = "/swagger-ui.html";
	public static final String PUSH_PUBLIC_KEY = "BBmyW7lABku7i9R71ZgjbO4UIUGNg688rRH4xC4rgGfMC1tPVMTPBi4XfGealUSI2SHUapsfIxw5uiNZGuaOxPw";
	public static final String PUSH_PRIVATE_KEY = "5_88-Iwj4kHjSQHTQeNJuS8PSGucJXnx6utKj5-cX40";
	public static final String REMOVE_SUBSCRIPTION_URL = "/web-push/unsubscribe";
	
	public static String getTokenSecret() {
		AppProperties appProperties = (AppProperties) SpringApplicationContext.getBean("appProperties");
		return appProperties.getTokenSecret();
	}
}
