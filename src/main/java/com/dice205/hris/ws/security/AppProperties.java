package com.dice205.hris.ws.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.dice205.hris.ws.shared.dto.HolidayTypes;

@Component
public class AppProperties {
	
	@Autowired
	private Environment env;
	
	private HolidayTypes holidayType;
	
	public String getTokenSecret() {
		return env.getProperty("tokenSecret");
	}

	public HolidayTypes getHolidayType() {
		return holidayType;
	}

	public void setHolidayType(HolidayTypes holidayType) {
		this.holidayType = holidayType;
	}
}
