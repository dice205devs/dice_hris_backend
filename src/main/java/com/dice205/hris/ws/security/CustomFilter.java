package com.dice205.hris.ws.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.transaction.annotation.Transactional;

import com.dice205.hris.ws.service.AuditLogService;
import com.dice205.hris.ws.shared.Utils;
import com.dice205.hris.ws.shared.dto.AuditLogDto;

public class CustomFilter implements Filter {
	private final Utils utils;
	private final AuditLogService auditLogService;

	public CustomFilter(AuditLogService auditLogService, Utils utils) {
		this.utils = utils;
		this.auditLogService = auditLogService;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (request instanceof HttpServletRequest) {
			request = new WrappedHttpServletRequest((HttpServletRequest) request);
		}

		AuditLogDto auditDetails = new AuditLogDto();

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		if (isSwagger(req.getRequestURI()) || !utils.getAuth().getRoles().contains("ROLE_ADMIN")) {
			chain.doFilter(request, response);
			return;
		}

		String body = IOUtils.toString(req.getInputStream(), "UTF-8");

		auditDetails.setApi(req.getRequestURI());
		auditDetails.setMethod(req.getMethod());
		auditDetails.setRequestBody(body);

		System.out.println("Request URI is: " + req.getRequestURI());
		System.out.println("Request Method is: " + req.getMethod());
		System.out.println("Request Body is: " + body);

		chain.doFilter(request, response);

		auditDetails.setStatus(res.getStatus());

		System.out.println("Response Status Code is: " + res.getStatus());

		this.auditLog(auditDetails);
	}

	@Transactional
	private void auditLog(AuditLogDto auditDetails) {
		boolean result = auditLogService.createAuditLog(auditDetails);

		if (result) {
			System.out.println("AUDIT: SUCCESS");
		} else {
			System.out.println("AUDIT: FAIL");
		}
	}

	private boolean isSwagger(String requestUri) {
		return requestUri.contains("/webjars") || requestUri.contains("/swagger-resources")
				|| requestUri.contains("/v2/api-docs") || requestUri.contains("/swagger-ui.html");
	}

}
